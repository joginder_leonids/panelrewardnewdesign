import appConfig from './apiConfig';

export const fetchProfileApi =(userId, access_token)=>{

const headers = Object.assign({'content-type':'application/json',
                               'accept':'*/*'
                            // 'Authorization' : 'Bearer '+ access_token
                           });
const request= new Request( appConfig.siteURL+'getProfileById/'+userId, {
  method : "GET",
  headers : headers,
});

return fetch(request).then(response => {
    return  response.json().then(profileDataResponse => {
      return profileDataResponse;
    });
  }).catch(error => {
   return error;
 });
}


export const updateProfileApi =(userId, access_token, data)=>{
  let objProfile =  JSON.stringify(data);

const headers = Object.assign({'content-type':'application/json',
                               'accept':'*/*'
                            // 'Authorization' : 'Bearer '+ access_token
                           });
const request= new Request( appConfig.siteURL+'updateProfile/'+userId, {
  method : "POST",
  headers : headers,
  body : objProfile
});

return fetch(request).then(response => {
    return  response.json().then(updateProfileResponse => {
    
      if(updateProfileResponse.success){
        sessionStorage.setItem('user_name', updateProfileResponse.profile[0].First_name+' '+ updateProfileResponse.profile[0].Last_name);        
      }
      return updateProfileResponse;
    });
  }).catch(error => {
   return error;
 });
}

//addProfileImageApi
export const addProfileImageApi =(data, pid)=>{

  const headers = Object.assign({'content-type':"application/x-www-form-urlencoded",
                                 'accept':'*/*'});
  const request= new Request( appConfig.siteURL+'uploadRegisterdUserImage/'+ pid, {
  
    method : "POST",
    //headers : headers,
    body : data
  });
  
  return fetch(request).then(response => {
      return  response.json().then(categoryResponse => {
        if(categoryResponse.success){
        return categoryResponse;
      }else{
        return categoryResponse;
      }
      });
    }).catch(error => {
     return error;
   });
  }