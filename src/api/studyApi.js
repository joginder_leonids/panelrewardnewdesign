import appConfig from './apiConfig';

export const fetchStudyApi =(userId, access_token, pageNo)=>{

const headers = Object.assign({'content-type':'application/json',
                               'accept':'*/*'
                           });
const request= new Request( appConfig.siteURL+'getStudyByIdPageNo/'+userId+'/'+pageNo, {
  method : "GET",
  headers : headers,
});

return fetch(request).then(response => {
    return  response.json().then(studyDataResponse => {
      return studyDataResponse;
    });
  }).catch(error => {
   return error;
 });
}

//fetchStudyCountDataApi
export const fetchStudyCountDataApi =(userId)=>{

  const headers = Object.assign({'content-type':'application/json',
                                 'accept':'*/*'
                             });
  const request= new Request( appConfig.siteURL+'getStudyCountDetails/'+userId, {
    method : "GET",
    headers : headers,
  });
  
  return fetch(request).then(response => {
      return  response.json().then(studyDataResponse => {
        return studyDataResponse;
      });
    }).catch(error => {
     return error;
   });
  }


export const saveStudyParticipateApi =( data, userId)=>{
  let participate =  JSON.stringify(data);

const headers = Object.assign({'Content-type':'application/json',
                               'Accept':'*/*'});
const request= new Request( appConfig.siteURL+'study-participate/'+userId, {
  method : "POST",
  headers : headers,
  body : participate
});

return fetch(request).then(response => {
    return  response.json().then(studyDataResponse => {
      return studyDataResponse;
    });
  }).catch(error => {
   return error;
 });
}

export const studyParticipateStatusApi =( data)=>{
  let status =  JSON.stringify(data);

const headers = Object.assign({'Content-type':'application/json',
                               'Accept':'*/*'});
const request= new Request( appConfig.siteURL+'study-participate-status', {
  method : "POST",
  headers : headers,
  body : status
});

return fetch(request).then(response => {
    return  response.json().then(statusResponse => {
      return statusResponse;
    });
  }).catch(error => {
   return error;
 });
}


export const studyCountApi =( userid)=>{

const headers = Object.assign({'Content-type':'application/json',
                               'Accept':'*/*'});
const request= new Request( appConfig.siteURL+'study-participate-count/'+ userid, {
  method : "GET",
  headers : headers,

});

return fetch(request).then(response => {
    return  response.json().then(studyCountResponse => {
      return studyCountResponse;
    });
  }).catch(error => {
   return error;
 });
}
