const siteURL = 'http://localhost:8811/api/';
//const siteURL = 'http://208.109.53.27:8822/api/';
//const siteURL ="http://192.168.1.4:8811/api/";
// const siteURL ="http://192.168.1.4:8811/api/";
// const siteURL ="http://208.109.53.27:7010/api/";
//const siteURL = 'https://apimy.panelreward.com/api/';
//const url = 'http://192.168.1.4:4011/';
const url = 'http://localhost:4011/';
// const url = 'http://208.109.53.27:4011/';
//const url = 'https://my.panelreward.com/';
  const imageUrl = 'http://192.168.1.4:8811';
//  const imageUrl = 'http://208.109.53.27:8822';
 // const imageUrl = 'https://apimy.panelreward.com/';
  
  const completedUrl = 'https://pcmy.panelreward.com/';
  const quotaFailUrl = 'https://pqfmy.panelreward.com/';
  const failUrl = 'https://pfmy.panelreward.com/';

  //test production
  //const siteURL = 'https://testmyapi.panelreward.com/api/'
  //const url = 'https://testmy.panelreward.com/';
  //const imageUrl = 'https://testmyapi.panelreward.com';


  // const completedUrl = 'https://testpcmy.panelreward.com/';
  // const quotaFailUrl = 'https://testpqfmy.panelreward.com/';
  // const failUrl = 'https://testpfmy.panelreward.com/';

  //..........

  let appConfig = {
    siteURL : siteURL,
    url : url,
    completedUrl : completedUrl,
    quotaFailUrl : quotaFailUrl,
    failUrl : failUrl,
    imageUrl : imageUrl
    //headers: headers
  }
  export default appConfig
