import appConfig from './apiConfig';

export const submitLoginApi =(loginData)=>{
let objLogin =  JSON.stringify(loginData);

const headers = Object.assign({'content-type':"application/json",
                               'accept':'*/*'});
const request= new Request( appConfig.siteURL+'login', {

//const request= new Request( app.siteURL + '/api/login', {
  method : "POST",
  headers : headers,
  body : objLogin
});

return fetch(request).then(response => {
    return  response.json().then(loginResponse => {

      if(loginResponse.success){
        sessionStorage.setItem('token', loginResponse.data.userToken);
        sessionStorage.setItem('user_id', loginResponse.userID);
        sessionStorage.setItem('user_name', loginResponse.user_name);
        if(loginResponse.resultData[0].ImageStatus === true){
          sessionStorage.setItem('userImagePath', appConfig.imageUrl+loginResponse.resultData[0].ImageURL);
        }
      }
      return loginResponse;
    });
  }).catch(error => {
   return error;
 });
}

export const resetPasswordApi =(Data)=>{
let objLogin =  JSON.stringify(Data);

const headers = Object.assign({'content-type':"application/json",
                               'accept':'*/*'});
const request= new Request( appConfig.siteURL+'resetPassword', {

  method : "POST",
  headers : headers,
  body : objLogin
});

return fetch(request).then(response => {
    return  response.json().then(resetPasswordResponse => {
      return resetPasswordResponse;
    });
  }).catch(error => {
   return error;
 });
}

export const forgetPasswordMailApi =(Data)=>{
let objPass =  JSON.stringify(Data);

const headers = Object.assign({'content-type':"application/json",
                               'accept':'*/*'});
const request= new Request( appConfig.siteURL+'forgetPasswordMail', {

  method : "POST",
  headers : headers,
  body : objPass
});

return fetch(request).then(response => {
    return  response.json().then(forgetPasswordMailResponse => {
      return forgetPasswordMailResponse;
    });
  }).catch(error => {
   return error;
 });
}
