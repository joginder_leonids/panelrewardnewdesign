import React, { Component } from 'react';
import './style.css';
import email from './email.svg';
import address from './address.svg';
import {
  Button, Card, CardBody, CardGroup, CardSubtitle, FormGroup, Label, CardImg, CardTitle, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row,
  CardHeader, CardText, CardFooter
} from 'reactstrap';
import { Link } from 'react-router-dom';
class CollectTotalReward extends Component {
  constructor(props) {
    super(props)

    this.state = {
      errors: {},
      totalRewards: "",
      totalAmount: "",
      couponTotalAmount: "",
      couponTotalPoint: "",
      paymentMethod: "",
      paypalEmail: "",
      amazoneDelivery: "",
      amazonePayAddOne: "",
      amazonePayAddTwo: "",
      amazonePayCity: "",
      amazonePayState: "",
      amazonePayPinCode: "",
      show: false,
    }
    this.onChange = this.onChange.bind(this);
    this.rewardemailChange = this.rewardemailChange.bind(this);
    this.paypalTransaction = this.paypalTransaction.bind(this);
    this.giftCouponOnline = this.giftCouponOnline.bind(this);
    this.giftCouponOffline = this.giftCouponOffline.bind(this);
  }

  componentWillReceiveProps(nextprops) {
    if (nextprops.transactionSuccess) {

    }
  }



  componentWillMount(Data) {
    this.setState({
      totalRewards: this.props.StudyCountData.totalRewards,
      totalAmount: this.props.StudyCountData.totalRewards / 50,
    });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.name === "paymentMethod") {
      this.setState({ amazoneDelivery: "" });
      let oldTotalAmount = parseInt(this.state.totalAmount);
      let newTotalAmount = oldTotalAmount - (oldTotalAmount % 10);
      this.setState({ couponTotalAmount: newTotalAmount, couponTotalPoint: newTotalAmount * 50 });
    }
  }

  rewardemailChange(e) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    let tempemail = e.target.value;
    if (e.target.value === "") {
      document.getElementById("rewardvalidmailerror").style.display = "none";
      document.getElementById("rewardmailerror").style.display = "block";
    } else if (!pattern.test(tempemail)) {
      document.getElementById("rewardvalidmailerror").style.display = "block";
      document.getElementById("rewardmailerror").style.display = "none";
    } else {
      document.getElementById("rewardvalidmailerror").style.display = "none";
      document.getElementById("rewardmailerror").style.display = "none";
    }
    this.setState({ paypalEmail: e.target.value });
  }
  handleValidationPaypalTransaction() {
    let valid = true;
    if (this.state.paypalEmail === "") {
      valid = false;
      document.getElementById("rewardmailerror").style.display = "block";
      return valid;
    }
    return valid;
    // let fields = this.state.fields;
    // let errors = {};
    // let formIsValid = true;
    // //Email
    // if (this.state.paypalEmail) {
    //   formIsValid = false;
    //   errors["email"] = "Cannot be empty";
    // } else if (!this.state.paypalEmail.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
    //   formIsValid = false;
    //   errors["email"] = "Email is not valid";
    // }
    // this.setState({ errors: errors });
    // return formIsValid;
  }

  paypalTransaction(e) {    //paypalTransaction
    e.preventDefault();
    if (this.handleValidationPaypalTransaction()) {
      if (this.state.totalAmount == "0") {
        alert("Sorry you have 0 balance to redeem");
      } else {
        let paypalTransactionObj = {}
        paypalTransactionObj.user_id = sessionStorage.getItem("user_id");
        paypalTransactionObj.totalPoints = this.state.totalRewards;
        paypalTransactionObj.totalAmount = this.state.totalAmount;
        paypalTransactionObj.paymentType = "paypal"
        paypalTransactionObj.paypalEmailId = this.state.paypalEmail;
        this.props.transaction(paypalTransactionObj);
        this.setState({ errors: {} });
      }

    }
  }

  giftCouponOnline() {     //giftCouponOnline
    if (this.state.totalAmount == "0") {
      alert("Sorry you have 0 balance to redeem");
    } else {
      let giftCouponObj = {}
      giftCouponObj.user_id = sessionStorage.getItem("user_id");
      giftCouponObj.totalPoints = this.state.couponTotalPoint;
      giftCouponObj.totalAmount = this.state.couponTotalAmount;
      giftCouponObj.paymentType = "GiftCouponOnline"
      giftCouponObj.deliveryType = "online"
      this.props.transaction(giftCouponObj);
    }

  }

  handleValidationGiftCouponOffline() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;
    //amazonePayAddOne
    if (!this.state.amazonePayAddOne) {
      formIsValid = false;
      errors["addressOne"] = "Cannot be empty";
    }
    //amazonePayAddTwo
    if (!this.state.amazonePayAddTwo) {
      formIsValid = false;
      errors["addressTwo"] = "Cannot be empty";
    }
    //amazonePayState
    if (!this.state.amazonePayState) {
      formIsValid = false;
      errors["state"] = "Cannot be empty";
    }
    //amazonePayCity
    if (!this.state.amazonePayCity) {
      formIsValid = false;
      errors["city"] = "Cannot be empty";
    }
    //amazonePayPinCode
    if (!this.state.amazonePayPinCode) {
      formIsValid = false;
      errors["pinCode"] = "Cannot be empty";
    } else if (!this.state.amazonePayPinCode.match(/^[0-9]{1,6}$/)) {
      formIsValid = false;

      errors["pinCode"] = "Pin code should be 6 digits";
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  giftCouponOffline(e) {

    e.preventDefault();
    if (this.handleValidationGiftCouponOffline()) {
      let giftCouponObj = {}
      giftCouponObj.user_id = sessionStorage.getItem("user_id");
      giftCouponObj.totalPoints = this.state.totalRewards;
      giftCouponObj.totalAmount = this.state.totalAmount;
      giftCouponObj.paymentType = "GiftCouponOffline"
      giftCouponObj.deliveryType = "offline"
      giftCouponObj.addressLineOne = this.state.amazonePayAddOne;
      giftCouponObj.addressLineTwo = this.state.amazonePayAddTwo;
      giftCouponObj.state = this.state.amazonePayState;
      giftCouponObj.city = this.state.amazonePayCity;
      giftCouponObj.pinCode = this.state.amazonePayPinCode;

      this.props.transaction(giftCouponObj);
    }
  }

  render() {

    let showPaymentMethod = "";
    let showAmazoneGiftCardPayment = "";

    if (this.state.paymentMethod === "paypal") {
      showPaymentMethod = <div id="paypalemail">
        <div id="paypaltexandfield">
          <div id="paypalemailtxt">Paypal Email</div>
          <div id="paypalemailfield"><input type="text" placeholder="Enter Email" id="emailinput" onChange={this.rewardemailChange} />
            <div id="rewardmailerror">
              <div class="rewquote">
                <blockquote>
                  Required
                          </blockquote>
              </div>
            </div>
            <div id="rewardvalidmailerror">
              <div class="rewvalidquote">
                <blockquote>
                  Incorrect Mail
                          </blockquote>
              </div>
            </div>
          </div>
        </div>
        <div id="paypalsend"><button id="paypalreqbtn" onClick={this.paypalTransaction}>Send Request</button></div>
      </div>

      // <form >
      //   <div className="paypalMethod">
      //     <label className="mb-5" className="text-left leftmarginset" > Paypal Email id : </label><br /><br />
      //     <InputGroup className="mb-4" className="text-left inputemail" >
      //       <InputGroupAddon addonType="prepend">
      //         <InputGroupText>
      //           <img src={email} width="15" height="15" ></img>
      //         </InputGroupText>
      //       </InputGroupAddon>
      //       <Input type="text" placeholder=" Email" name="paypalEmail" value={this.state.paypalEmail} onChange={this.onChange} />

      //     </InputGroup>
      //     <span style={{ color: "red", fontSize: "14px" }} className="errormarginset">{this.state.errors["email"]}</span><br />
      //     <Button className="mt-3" onClick={this.paypalTransaction} style={{ backgroundColor: "#1985AC", marginLeft: '40%' }}>Send Request</Button>
      //   </div></form>
    }
    if (this.state.paymentMethod === "amazone") {
      showPaymentMethod = <div id="amazonform">
        <div id="giftamounts">
          <div id="giftamt">Gift Coupons Amount</div>
          <div id="giftamttxt">{this.state.couponTotalAmount + " USD"}</div>
      </div>
      <div id="giftcoupons">
          <div id="giftcoupon">Gift Coupons Points</div>
          <div id="giftamttxt">{this.state.couponTotalPoint}</div>
      </div>
      <div id="delivery">
          <div id="deli">Delivery Type</div>
          <div id="deliradio"><input type="radio" value="Online" name="amazoneDelivery" onChange={this.onChange} /><span>Online</span></div>
      </div>
       </div>
        //<div style={{ textAlign: 'left' }}>
        //   <label className="amazeLabel">Gift coupons amount :  &nbsp;</label><label>{this.state.couponTotalAmount + " USD"}</label><br />
        //   <label className="amazeLabel">Gift coupons points :  &nbsp;</label><label>{this.state.couponTotalPoint}</label><br /><br />

        //   <label className="amazeLabel">Delhivery type :</label>
        //   <FormGroup check className="text-left setradiomargin">
        //     <Label check>
        //       <Input type="radio" name="amazoneDelivery" value="Online" onChange={this.onChange} />
        //       Online
        //            </Label>
        //     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {/* <Label>
             <Input type="radio" name="amazoneDelivery" value="Offline" onChange={this.onChange} ></Input>
             Offline             // Offline work is on progress !!!!!!
                   </Label> */}
      //   </FormGroup>

      {/* <input type="radio" name="amazoneDelivery" value="Online" onChange={this.onChange} /> Online
                            <input type="radio" name="amazoneDelivery" value="Offline" onChange={this.onChange} /> Offline
                             <br /><br /> */}
      // </div>
    }

    if (this.state.amazoneDelivery === "Online") {

      showAmazoneGiftCardPayment =<div id="onlinebtn">
        <button id="onlinereqbtn" onClick={this.giftCouponOnline}>Send Request</button>
      </div>
      //  <div>

      //   <Button className="mt-3" style={{ backgroundColor: "#1985AC", marginLeft: '37%' }} onClick={this.giftCouponOnline}>Send Request</Button>

      // </div>

     
    }
    if (this.state.amazoneDelivery === "Offline") {
      showAmazoneGiftCardPayment = <form>

        <div className="amazeOffline"><br /><br />
          <label className="text-left leftmarginset">Delhivery Address :</label><br /><br />
          <InputGroup className="mb-2" className="text-left inputemail" >
            <InputGroupAddon addonType="prepend" >
              <InputGroupText>
                <img src={address} width="15" height="15" ></img>
              </InputGroupText>
            </InputGroupAddon>
            <Input type="text" placeholder="Address line one" name="amazonePayAddOne" value={this.state.amazonePayAddOne} onChange={this.onChange} />
          </InputGroup>
          <span style={{ color: "red", fontSize: "14px" }} className="errormarginset">{this.state.errors["addressOne"]}</span><br />
          <InputGroup className="mb-2" className="text-left inputemail">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <img src={address} width="15" height="15" ></img>
              </InputGroupText>
            </InputGroupAddon>
            <Input type="text" placeholder="Address line two" name="amazonePayAddTwo" value={this.state.amazonePayAddTwo} onChange={this.onChange} />
          </InputGroup>
          <span style={{ color: "red", fontSize: "14px" }} className="errormarginset">{this.state.errors["addressTwo"]}</span><br />
          <label className="text-left leftmarginset">City</label>
          <InputGroup className="mb-2" className="text-left inputemail" >
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <img src={address} width="15" height="15" ></img>
              </InputGroupText>
            </InputGroupAddon>
            <Input type="text" placeholder="City" name="amazonePayCity" value={this.state.amazonePayCity} onChange={this.onChange} />
          </InputGroup>
          <span style={{ color: "red", fontSize: "14px" }} className="errormarginset">{this.state.errors["city"]}</span><br />
          <label className="text-left leftmarginset">State</label>
          <InputGroup className="mb-2" className="text-left inputemail" >
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <img src={address} width="15" height="15" ></img>
              </InputGroupText>
            </InputGroupAddon>
            <Input type="text" placeholder="State" name="amazonePayState" value={this.state.amazonePayState} onChange={this.onChange} />
          </InputGroup>
          <span style={{ color: "red", fontSize: "14px" }} className="errormarginset">{this.state.errors["state"]}</span><br />
          <label className="text-left leftmarginset">PinCode</label>
          <InputGroup className="mb-2" className="text-left inputemail" >
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <img src={address} width="15" height="15" ></img>
              </InputGroupText>
            </InputGroupAddon>
            <Input type="text" placeholder="PinCode" name="amazonePayPinCode" value={this.state.amazonePayPinCode} onChange={this.onChange} />
          </InputGroup>
          <span style={{ color: "red", fontSize: "14px" }} className="errormarginset">{this.state.errors["pinCode"]}</span>
          <br /><br />
          <Button onClick={this.giftCouponOffline} style={{ backgroundColor: "#1985AC", marginLeft: '40%' }} > Submit</Button>
        </div>


        {/* <div className="amazeOffline"><br />
          <label > Delivery Address : </label><br /><br />
          <label >Address line one :</label><input type="text" className="" placeholder="Address line one" name="amazonePayAddOne" value={this.state.amazonePayAddOne} onChange={this.onChange} />
          &nbsp;<span style={{ color: "red", fontSize: "14px" }}>{this.state.errors["addressOne"]}</span><br /><br />
          <label> Address line two :</label><input type="text" className="" placeholder="Address line two" name="amazonePayAddTwo" value={this.state.amazonePayAddTwo} onChange={this.onChange} />
          &nbsp;<span style={{ color: "red", fontSize: "14px" }}>{this.state.errors["addressTwo"]}</span><br /><br />
          <label > City :</label><input type="text" className="" placeholder="City" name="amazonePayCity" value={this.state.amazonePayCity} onChange={this.onChange} />
          &nbsp;<span style={{ color: "red", fontSize: "14px" }}>{this.state.errors["city"]}</span><br /><br />
          <label> State :</label><input type="text" className="" placeholder="State" name="amazonePayState" value={this.state.amazonePayState} onChange={this.onChange} />
          &nbsp;<span style={{ color: "red", fontSize: "14px" }}>{this.state.errors["state"]}</span><br /><br />
          <label > PinCode :</label><input type="text" className="" placeholder="pinCode" name="amazonePayPinCode" value={this.state.amazonePayPinCode} onChange={this.onChange} />
          &nbsp;<span style={{ color: "red", fontSize: "14px" }}>{this.state.errors["pinCode"]}</span><br /><br />
          <button type="submit" >Send Request</button>
        </div> */}
      </form>
    }



    return (
      <div id="studycountmain">
        <div id="headerstudy">
          <h3>Reward</h3>
          <ol id="breadcrumb">
            <li>Home</li> &nbsp; / &nbsp;
          <li style={{ opacity: '0.8' }}>Reward</li>
          </ol>
        </div>

        <div id="collectreward">
          <h3>Points Redeemtion</h3>
          <div id="totalpoints">
            <div id="totalpointstxt">Total Points </div>
            <div id="totalpointsno">{this.state.totalRewards + " USD"}</div>
          </div>
          <div id="totalpayamt">
            <div id="totalpaystxt">Total Points </div>
            <div id="totalpayno">{this.state.totalAmount + " USD"}</div>
          </div>
          <div id="paytype">
            <div id="paptypetxt">Payment type </div>
            <div id="paytyperadio">
              <div id="papal"> <input type="radio" value="paypal" name="paymentMethod" onChange={this.onChange} /><span>Paypal</span></div>
              <div id="amazon"> <input type="radio" value="amazone" name="paymentMethod" onChange={this.onChange} /><span>Amazon</span></div>
            </div>
          </div>
          {showPaymentMethod}
          {showAmazoneGiftCardPayment}
        </div>
      </div>


      //     <div className="paypal" style={{marginTop: "40px"}}>
      //   <div className="cardback">
      //     <Card className="mycard">
      //       <CardHeader><h3 className="text-center">Points Redeemtion</h3></CardHeader>
      //       <CardBody>
      //       <Card  style={{paddingLeft:'2%',paddingTop:'2%',paddingBottom:'2%'}}>
      //         <CardText className="text-left leftmarginset">Total points :  {this.state.totalRewards}</CardText>
      //         <CardText className="text-left leftmarginset" >Total payable amount : {this.state.totalAmount + " USD"}</CardText>

      //         <CardText className="text-left" >
      //           <Label className="text-left  leftmarginset">Payment type :</Label>
      //           <FormGroup className="text-left setradiomargin">
      //             <Label check>
      //               <Input type="radio" name="paymentMethod" value="paypal" onChange={this.onChange} />
      //               Paypal
      //            </Label>
      //             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      //             <Label>
      //               <Input type="radio" name="paymentMethod" value="amazone" onChange={this.onChange}></Input>
      //               Amazon
      //             </Label>
      //           </FormGroup>
      //           {/* <hr /> */}
      //           {showPaymentMethod}
      //           {showAmazoneGiftCardPayment}
      //         </CardText>
      //         </Card>
      //       </CardBody>

      //       <CardFooter></CardFooter>
      //     </Card>
      //   </div>
      // </div>

    );
  }
}
export default CollectTotalReward;
