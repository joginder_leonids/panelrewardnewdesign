import React, { Component } from 'react';
import './style.css';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { Badge, Card, CardBody, CardHeader, Button, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import dotspinner from '../../icons/spinner-of-dots.png';
import cude from '../../icons/cube.png';
import thumb from '../../icons/thumb-up.png';
import approved from '../../icons/checkmark.png';
import coupon from '../../icons/gift-card.png';
import process from '../../icons/process.png';
import reject from '../../icons/cross.png';
import areachart from '../../icons/area-chart.png';
import eye from '../../icons/eye.png';
import pencil from '../../icons/pencil.png';
import emptytranhistory from '../../images/empty transaction.png';
class TransactionHistoryView extends Component {
  constructor(props) {
    super(props)

    this.state = {

    };
  }

  componentWillMount() {
    this.props.showTranactions();
  }

  getGiftcouponByUser(data) {
    this.props.getGiftcouponByUser(data);
  }



  render() {
    let transactionView = "";
    let transactionItem = "";
    let SNo = 0;
    let emptyTransimg="";
    if (this.props.UserTranactionData && this.props.UserTranactionData.length) {
      let studies = this.props.UserTranactionData;
      transactionView = Object.keys(studies).map((item, index) => {
        transactionItem = studies[item];

        let viewAction = "";
        let ViewPaymentStatus = "";
        if (transactionItem.paymentType === "GiftCouponOnline" && transactionItem.paymentStatus === "success") {
          // viewAction = <Button type="button" style={{ padding: "0.2rem 0.3rem", width: '72%' }} color="success">ViewCoupon</Button>
          viewAction = <button id="coupon" onClick={this.getGiftcouponByUser.bind(this, transactionItem)}><img src={coupon} height="15px" width="15px" /><span>View Coupon</span></button>
          ViewPaymentStatus = <button id="complete"><img src={thumb} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
        } else if (transactionItem.paymentStatus === "success") {
          viewAction = <button id="complete"><img src={thumb} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
          ViewPaymentStatus = <button id="complete"><img src={thumb} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
        } else if (transactionItem.paymentStatus === "pending") {
          viewAction = <div><button id="pending"><img src={dotspinner} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button></div>
          ViewPaymentStatus = <div><button id="pending"><img src={dotspinner} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button></div>
          //viewAction = <div><Button color="info" style={{ padding: "0.3rem 0.3rem", cursor: 'default', width: '72%' }}>{transactionItem.paymentStatus.toUpperCase()}</Button></div>
        } else if (transactionItem.paymentStatus === "approved") {
          //viewAction = <div><Button color="primary" style={{ padding: "0.2rem 0.3rem", cursor: 'default', width: '72%' }}>{transactionItem.paymentStatus.toUpperCase()}</Button></div>
          viewAction = <button id="approved"><img src={approved} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
          ViewPaymentStatus = <button id="approved"><img src={approved} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
        } else if (transactionItem.paymentStatus === "processing") {
          //viewAction = <div><Button style={{ padding: "0.3rem 0.3rem", backgroundColor: "#021a2f", cursor: 'default', width: '72%' }}>{transactionItem.paymentStatus.toUpperCase()}</Button></div>
          viewAction = <button id="processing"><img src={process} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
          ViewPaymentStatus = <button id="processing"><img src={process} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
        } else {
          //viewAction = <div><Button color='danger' style={{ padding: "0.2rem 0.2rem", cursor: 'default', width: '72%' }}>{transactionItem.paymentStatus.toUpperCase()}</Button></div>
          viewAction = <button id="reject"><img src={reject} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
          ViewPaymentStatus = <button id="reject"><img src={reject} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
        }


        SNo = SNo + 1;
        return <tr className="studyTr" key={index}>
          <td className="txnTD">{SNo}</td>
          <td className="txnTD">{transactionItem.totalAmount + " USD"}</td>
          <td className="txnTD">{transactionItem.totalPoints}</td>
          <td className="txnTD">{transactionItem.paymentType}</td>
          <td className="txnTD">{ViewPaymentStatus}</td>
          <td className="txnTD">{transactionItem.updatedAt.split("T")[0] + " " + transactionItem.updatedAt.split("T")[1].split(".")[0]}</td>
          <td className="txnTD">{viewAction}</td>
        </tr>
      });
    }
    else{
      emptyTransimg= <div id="emptytableimg"><img src={emptytranhistory} height="100%" width="100%" /></div>
    }

    return (
      <div id="studycountmain">
        <div id="headerstudy">
          <h3>Transaction History</h3>
          <ol id="breadcrumb">
            <li>Home</li> &nbsp;/&nbsp;
            <li style={{ opacity: '0.8' }}>Transaction History</li>
          </ol>
        </div>

        {/*         <div id="transcards">
          <div className="trans" id="totalachieveamt">
            <div id="transicon1"><img src={thumb} height="40px" width="40px" /></div>
            <div id="transtext1"><p>6,723</p>Total Achive Amount</div>
          </div>

          <div className="trans" id="totalpoint">
          <div id="transicon2"> <img src={cude} height="40px" width="40px" /></div>
            <div id="transtext2"><p>5,000</p>Total Points</div>
          </div>

          <div className="trans" id="requestpoints">
          <div id="transicon3"><img src={areachart} height="40px" width="40px" /></div>
            <div id="transtext3"><p>2,000</p>Request Points</div>
          </div>

          <div className="trans" id="balancepoint">
          <div id="transicon4"><img src={eye} height="40px" width="40px" /></div>
            <div id="transtext4"><p>2,000</p>Request Points</div>
          </div>

        </div> */}

        <div id="transhistory">
          <h3>Transaction History</h3>
          <table  id="transtable">
            <thead>
              <tr>
                  <th>S.NO</th>
                  <th>AMOUNT</th>
                  <th>POINTS</th>
                  <th>PAYMENT TYPE</th>
                  <th>PAYMENT</th>
                  <th>DATE & TIME</th>
                  <th>ACTION</th>
              </tr>
            </thead>
            <tbody>
            {transactionView}
            </tbody>
          </table>
          {emptyTransimg}
        </div>
       
      </div>


     






      // <div className="profile">
      // <div className = "studyback" style={{paddingTop:'1.5%'}}>
      //   <Card style={{marginLeft:"15%",marginRight:'15%'}}>
      //     <CardHeader className="text-center"><h3>Transaction History</h3></CardHeader>
      //     <CardBody>

      //       <Table responsive striped hover>
      //         <thead className="text-center">
      //           <tr>
      //             <th>Sr.No</th>
      //             <th>Amount</th>
      //             <th>Points</th>
      //             <th>Payment Type</th>
      //             <th>Payment</th>
      //             <th>Payment Status</th>
      //             <th>Action</th>
      //           </tr>
      //         </thead>
      //         <tbody>

      //           {studyList}

      //         </tbody>
      //       </Table>

      //     </CardBody>
      //   </Card>
      //   </div>
      // </div>


    );
  }
}
export default TransactionHistoryView;
