import React, { Component } from 'react';
import './coupon.css';
import {
  Button, Card, CardBody, CardGroup, CardSubtitle, FormGroup, Label, CardImg, CardTitle, Table, InputGroup, InputGroupAddon, InputGroupText, Row,
  CardHeader, CardText, CardFooter
} from 'reactstrap';
class GiftcouponView extends Component {
  constructor(props) {
    super(props)

    this.state = {

    };
    this.showTranactions = this.showTranactions.bind(this);
    this.showDashboard=this.showDashboard.bind(this);
  }

  showTranactions() {
    this.props.showTranactions();
  }
  showDashboard(){
    this.props.showDashboard();
  }

  render() {
    if (this.props.GiftcouponDataSuccess) {
      console.log(this.props.GiftcouponData.couponDetails);
    }

    let studyList = "";
    let transactionItem = "";
    let SNo = 0;
    if (this.props.GiftcouponData && this.props.GiftcouponData.couponDetails.length) {
      let studies = this.props.GiftcouponData.couponDetails;
      studyList = Object.keys(studies).map((item, index) => {
        transactionItem = studies[item];

        SNo = SNo + 1;
        return <tr className="studyTr" key={index}>
          <td className="txnTD">{SNo}</td>
          <td className="txnTD">{transactionItem[0].Coupon_code}</td>
          <td className="txnTD">{transactionItem[0].Coupon_amount}</td>
          <td className="txnTD">{transactionItem[0].Expire_date.split("T")[0]}</td>
        </tr>
      });
    }

    return (

      <div id="studycountmain">
        <div id="couponheader">
          <h3>Assign Coupon</h3>
        </div>
        <div id="couponpage">
        <div id="couponsbtn">
      <button id="gototran" onClick = {this.showTranactions}>Go To Transaction History</button>
      <button id="gotodash"  onClick = {this.showDashboard}>Go To Dashboard</button>
        </div>
          <table id="cupntbl">
            <thead>
              <tr>
                <th>S.NO</th>
                <th>COUPON CODE</th>
                <th>COUPON AMOUNT</th>
                <th>EXPIRE DATE</th>              
              </tr>
            </thead>
            <tbody>
                {studyList}
                 </tbody>
          </table>
        </div>
      </div>

      // <div className = "containerS">
      // <div className = "studyback" style={{paddingTop:'2%'}}>
      //   <Card style={{marginLeft:'20%',marginRight:'20%'}}>
      //     <CardHeader><h3 className="text-center">Assign Coupons</h3></CardHeader>
      //     <CardTitle style={{marginLeft:'2%',marginTop:'2%'}}><Button style={{backgroundColor:'#1985AC'}} onClick = {this.showTranactions} >Back</Button></CardTitle>
      //     <CardBody>
      //     <Table responsive striped hover>
      //             <thead className="text-center">
      //               <tr>
      //                 <th>Sr.No.</th>
      //                 <th>Coupon Code</th>
      //                 <th>Coupon Amount</th>
      //                 <th>Expire Date</th>
      //               </tr>
      //             </thead>
      //             <tbody>
      //               {studyList}
      //             </tbody>
      //           </Table>

      //     </CardBody>
      //   </Card>
      // </div>
      // </div>

    );
  }
}
export default GiftcouponView;
