import React, { Component } from 'react';
import './style.css';
import { Link } from 'react-router-dom';
import { Card, Button, CardTitle, CardText, CardHeader, CardBody } from 'reactstrap';
import giphy from './giphy.gif';
import facebook from '../Image/facebook-logo.svg';
import google from '../Image/google-logo.svg';
import twitter from '../Image/twitter.svg';
import linked from '../Image/linkedin.svg';
import panelogo from '../Image/panel reward.png';
import thnkuimg from '../Image/thankyou.png';
class ThankuView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      name: ''
    };

  }

  componentDidMount(){   
    let userEmail = sessionStorage.getItem('registerUserMail');
    let userName = sessionStorage.getItem('registerUserName');
      
    this.setState({
      email : userEmail,
      name  : userName
    });
  }



  render() {


    return (
  <div className="tnkuMain">
      <div id="thnkspanelogo"><img src={panelogo} height="50px" height="50px" /></div>
      <div id="thnkspanelicon">
      <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
        <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
        <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
        <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
      </div>
      <div id="thnximgcss">
      <img src={thnkuimg} height="auto" width="auto"/>
      <div>
        <p id="emailheading">Validation E-Mail Sent !</p>
        <p id="firstpara">An e-mail has been sent to <b> {this.state.email} </b>
          to confirm the ownership of the address by <b> {this.state.name} </b>
        </p>
        <p id="scndpara">It will arrive in the inbox shortly. 
              Please follow the instruction in the e-mail.
        </p>
      </div>
        <div><Link to="/Login"><button id="bcktohome">Back to home</button></Link></div>
      </div>
  </div>
      

    );
  }
}
export default ThankuView;
