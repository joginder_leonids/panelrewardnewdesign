import React, { Component } from 'react';
import './style.css';
import Pagination from "react-js-pagination";
import { Badge, Card, CardBody, CardHeader, Button, Col, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import bubble from '../../icons/bubble.png';
import msg from '../../icons/mail.png';
import linkbtn from '../../icons/link-symbol.png';
import emptystudy from '../../images/empty study.png';
class StudyList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activePage: 1,
      showButton: false,
      totalCompleteStudies: 0,
      newStudies: 0,
      totalStudies: 0,
    };
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  handlePageChange(pageNumber) {

    this.setState({ activePage: pageNumber });
    this.props.getStudiesByPageNo(pageNumber);
  }

  componentDidMount() {
    this.props.getStudiesByPageNo(1);
    this.setState({ totalStudies: this.props.totalStudies });
  }


  participate(data) {
    let userId = sessionStorage.getItem("user_id");
    //let url ="http://localhost:4000/CompaneyParticipated/s:"+ data._id + "/u:"+ userId ;

    //let url ="http://192.169.139.127:4000/CompaneyParticipated/s:"+ data._id + "/u:"+ userId ;
    //let url =  data.study_url +"/s:"+ data._id + "#u:"+ userId ;
    let url = "";
    if (data.study_url.match("{uid}")) {
      url = data.study_url.split("{uid}")[0] + "$SID$" + data._id + "@UID@" + userId;
    } else {
      url = data.study_url + "$SID$" + data._id + "@UID@" + userId;
    }

    window.open(url);
    let participateData = {};
    participateData.studyId = data._id;
    participateData.statusId = 0;
    this.props.saveStudyParticipate(participateData);
  }

  componentWillReceiveProps(nextprops) {
    this.setState({ totalStudies: nextprops.totalStudies });
    if (nextprops.StudyTotalCountData) {
      // alert();
      this.setState({
        totalCompleteStudies: parseInt(nextprops.StudyTotalCountData.completeStudyCount),
        newStudies: parseInt(nextprops.StudyTotalCountData.totalStudy) - parseInt(nextprops.StudyTotalCountData.completeStudyCount)
      });
    }
  }

  render() {

    if(this.props.getAgainStudiesData){
      this.props.getStudiesByPageNo(this.state.activePage);
    }

    let emptyStudyimg="";
    let studyList = "";
    let studyItem = "";
    let srNo = (this.state.activePage * 10 - 10);
    if (this.props.studies && this.props.studies.length) {
       let studies = this.props.studies;
      studyList = Object.keys(studies).map((item, index) => {
        studyItem = studies[item];
        let showParticipateButton = "";
        if (studyItem.is_live == true) {
          studyItem.isLive = "right";
        } else {
          studyItem.isLive = "wrong";
        }
        if (studyItem.completionStatus === "0") {
          studyItem.completionStatuss = <Badge color="info" style={{ width: '50%' }}>Start</Badge>;
          showParticipateButton = <button id="participatebtns" disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participant Participated</span></button>
          this.state.showButton = true;
        } else if (studyItem.completionStatus === "1") {
          studyItem.completionStatuss = <Badge color="success" style={{ width: '50%' }}>Completed</Badge>;
          showParticipateButton = <button id="participatebtns" disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participant Participated</span></button>
          this.state.showButton = true;
        } else if (studyItem.completionStatus === "2") {
          this.state.showButton = true;
          showParticipateButton = <button id="participatebtns" disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participant Participated</span></button>
          studyItem.completionStatuss = <Badge color="warning" style={{ width: '50%' }}>QuotaFail</Badge>;
        } else if (studyItem.completionStatus === "3") {
          this.state.showButton = true;
          showParticipateButton = <button id="participatebtns" disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participant Participated</span></button>
          studyItem.completionStatuss = <Badge color="danger" style={{ width: '50%' }}>Fail</Badge>;
        } else {
          studyItem.completionStatus = "";
          this.state.showButton = false;
          showParticipateButton = <button id="participatebtn" onClick={this.participate.bind(this, studyItem)} disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participate The Survey</span></button>
        }
        let studyd = studyItem.created_at;
        let correctdate = studyd.split('T');
        srNo = srNo + 1;
        return <tr className="" key={index}>
          <td className="">{srNo}</td>
          <td className="">{correctdate[0]}</td>
          <td><div class="tooltipFirstParty">{studyItem.study_name.slice(0, 40)}
            <span class="tooltiptextFirstParty">{studyItem.study_url}</span>
          </div></td>
          <td className="">{studyItem.cpi}</td>
          <td className="">{studyItem.completionStatuss}</td>
          <td className="">
            {/* <button color="primary" style={{ padding: "0.2rem 0.2rem", backgroundColor: "#1985AC" }} type="button" onClick={this.participate.bind(this, studyItem)} disabled={this.state.showButton} > Participate </button> */}
            {/* <button id="participatebtn"><img src={linkbtn} height="15px" width="15px" onClick={this.participate.bind(this, studyItem)} disabled={this.state.showButton} /><span>Participate the survey</span></button> */}
            {showParticipateButton}
          </td>
        </tr>
      });
    }
    else{
      emptyStudyimg= <div id="emptytableimg"><img src={emptystudy} height="100%" width="100%" /></div>
    }


    return (
      <div id="studycountmain">
        <div id="headerstudy">
          <h3>My Study</h3>
          <ol id="breadcrumb">
            <li>Home</li> &nbsp;/&nbsp;
              <li style={{ opacity: '0.8' }}>My Study</li>
          </ol>
        </div>
        <div id="stdcards">
          <div className="stdies" id="copmletestd">
            <div id="stdicon1">
              <div id="totalstdicon"><img src={bubble} height="35px" width="35px" /></div>
            </div>
            <div id="stdintext1"><p>{this.state.totalCompleteStudies}</p>Total Completed Studies</div>
          </div>
          <div className="stdies" id="newstd">
            <div id="stdicon2">
              <div id="newstdicon"><img src={msg} height="35px" width="35px" /></div>
            </div>
            <div id="stdintext2"><p>{this.state.newStudies}</p>New Studies</div>
          </div>
        </div>
        {/* mystudyscroll */}
        <div id="studypage">
          <h3>Study List</h3>
          <table id="mystudyscroll">

            <thead>
              <tr>
                <th>S.NO</th>
                <th>DATE</th>
                <th>STUDY NAME</th>
                <th>REWARD POINTS</th>
                <th>PARTICIPATION STATUS</th>
                <th>ACTION</th>
              </tr>
            </thead>
            <tbody>
            {studyList}
            </tbody>
          </table>
          {emptyStudyimg}
        </div>

        {/* <div id="std">
           <div id="stdtable">
               <h3>Study List</h3>
             <table id="customers">
              <tr>
                <th>S.No.</th>
                <th>DATE</th>
                <th>Study Name</th>
                <th>REWARD POINTS</th>
                <th>PARTICIPATION STATUS</th>
                <th>Action</th>
              </tr>
              {studyList}
            </table>
            </div>
        </div> */}
        <div className="paginationCountryMemberTable">
        <div id="pagebar">
          <Pagination
          
            activePage={this.state.activePage}
            itemsCountPerPage={10}
            totalItemsCount={this.state.totalStudies}    //total number of members
            pageRangeDisplayed={5}
            onChange={this.handlePageChange}
          />
          </div>
        </div>
      </div>
    );
  }
}
export default StudyList;
