import React, { Component } from 'react';
import './style.css';
import { fetchEduQuestions, saveQuestionAns } from '../../actions/profileDashboard';
import CheckboxOrRadioGroup from '../CheckBoxOrRadio/CheckboxOrRadioGroup';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { connect } from 'react-redux';
import { Card, Button, CardTitle, CardText, CardHeader, CardBody, FormGroup, Label, Input } from 'reactstrap';
import Select from 'react-select';
import { Link } from 'react-router-dom';
import demopic from './demopic.jpg';
import CircularProgressbar from 'react-circular-progressbar';
class Education extends Component {

  constructor(props) {


    super(props)
    this.state = {
      option: [],
      selectedOption: [],
      selectoption: "",
      categoryName: "",
      categoryPercentage: "",
      houseAns:"",
      carAns: "",
      mobAns: "",
      bikeAns: "",
      lapAns: "",
      creditCmpnyAns:"",
      currentCategoryID: '',
      currentOrderID :'',
      TotalCategory: '',
      TotalCategoryData:[],
      userPrimaryAgeCount : 0,
      age1to4Count : 0,
      age5to10Count : 0,
      age11to18Count : 0
    };

  }
  componentDidMount(){
    document.title = "Panelreward-"+ (window.location.pathname.split("/")[3].split("catName")[1]).replace("%20", " ").replace("%20", " ");
  }

  componentWillMount() {
    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let catNam = (window.location.pathname.split("/")[3].split("catName")[1]);
    var catN = catNam.replace("%20", " ");
    var catName = catN.replace("%20", " ");
    let retriveCategiry = localStorage.getItem("categoriesData");
    let categoryData = JSON.parse(retriveCategiry);
    let currentOrderNo = categoryData.find(item => item.cat_id == catID).order_no;
    this.setState({
      TotalCategory : categoryData.length,
      TotalCategoryData : categoryData,
      categoryName: catName,
      currentCategoryID : catID,
      currentOrderID: currentOrderNo,
     
    });
    let userId = sessionStorage.getItem("user_id");

    if (userId && catID) {
      this.props.dispatch(fetchEduQuestions(userId, catID));
    }
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.educationData) {
      let countUserAns= 0;
      for (var eduLoop = 0; eduLoop < nextProps.educationData.length; eduLoop++) {

        // if(nextProps.educationData[eduLoop].userOptions === "0" || nextProps.educationData[eduLoop].userOptions === undefined ){
        //   countUserAns= countUserAns + 1;
        // }
           
        //  if(Array.isArray(nextProps.educationData[eduLoop].userOptions)){
        //    if(nextProps.educationData[eduLoop].userOptions.length === 0){
        //     countUserAns= countUserAns + 1;
        //    }
        //  }
       

        //House 5bd195d09a7ee00f7064d156
         if(nextProps.educationData[eduLoop]._id === "5bd195d09a7ee00f7064d156"){
          this.setState({houseAns:nextProps.educationData[eduLoop].userOptions});
         }
        // car 
        if (nextProps.educationData[eduLoop]._id === "5bd19a759a7ee00f7064d179") {
          this.setState({ carAns: nextProps.educationData[eduLoop].userOptions });
        }
        //mobile  
        if (nextProps.educationData[eduLoop]._id === "5bd19f359a7ee00f7064d1bb") {
          this.setState({ mobAns: nextProps.educationData[eduLoop].userOptions });
        }
        //bike 
        if (nextProps.educationData[eduLoop]._id === "5bd19c289a7ee00f7064d190") {
          this.setState({ bikeAns: nextProps.educationData[eduLoop].userOptions });
        }
        //laptop
        if (nextProps.educationData[eduLoop]._id === "5bd19de99a7ee00f7064d1a5") {
          this.setState({ lapAns: nextProps.educationData[eduLoop].userOptions });
        }
        //credit company
        if(nextProps.educationData[eduLoop]._id === "5c5e79c5d394800a08ac3531"){
          //5c5e61268421c0144824aed8   test
          //5c5e79c5d394800a08ac3531   live
          this.setState({ creditCmpnyAns: nextProps.educationData[eduLoop].userOptions });
        }

        // Primary Age Count
        if(nextProps.educationData[eduLoop]._id === "5bd17cfc9a7ee00f7064d07f"){


          if(nextProps.educationData[eduLoop].userOptions){
            let myAgeCount = nextProps.educationData[eduLoop].userOptions ;
            if (myAgeCount === "0" || myAgeCount === "Select")
              {
                this.setState({userPrimaryAgeCount : 0});
              }
        else {
            let userOpt = nextProps.educationData[eduLoop].options;
            let userCountSearch =   userOpt.find( options => options.option_id === myAgeCount );
          
           if(userCountSearch.options === "None")
           {
            this.setState({userPrimaryAgeCount : 0});
           }
           else if(userCountSearch.options === "More than 4")
           {
            this.setState({userPrimaryAgeCount : 5})
           }
           else{
            this.setState({userPrimaryAgeCount : parseInt(userCountSearch.options)})
           }
          }
          }
          else{
            this.setState({userPrimaryAgeCount : 0, age1to4Count : 0, age5to10Count : 0, age11to18Count :0 });
          }
           
        }

        //age1to4Count : 0,
      // : 0,
      // : 0

      // Primary Age age1to4Count
      if(nextProps.educationData[eduLoop]._id === "5bd97e7c8082461644deaeac"){

        if(nextProps.educationData[eduLoop].userOptions){
        let myAgeCount = nextProps.educationData[eduLoop].userOptions ;
        if (myAgeCount === "0" || myAgeCount === "Select")
        {
          this.setState({age1to4Count : 0});
        }
        else{
        let userOpt = nextProps.educationData[eduLoop].options;
        let userCountSearch =   userOpt.find( options => options.option_id === myAgeCount );
        this.setState({age1to4Count : parseInt(userCountSearch.options)});
      }
    }
    else{
      this.setState({age1to4Count : 0});
    }
   }


    // Primary Age age5to10Count
    if(nextProps.educationData[eduLoop]._id === "5bd97f098082461644deaeb3"){

      if(nextProps.educationData[eduLoop].userOptions){
      let myAgeCount = nextProps.educationData[eduLoop].userOptions ;
      if (myAgeCount === "0" || myAgeCount === "Select")
      {
        this.setState({age5to10Count : 0});
      }
      else{
      let userOpt = nextProps.educationData[eduLoop].options;
      let userCountSearch =   userOpt.find( options => options.option_id === myAgeCount );
      this.setState({age5to10Count : parseInt(userCountSearch.options)});
    }
  }
  else{
    this.setState({age5to10Count : 0});
  }
  }

// Primary Age age11to18Count
if(nextProps.educationData[eduLoop]._id === "5bd97cd88082461644deaea3"){
  if(nextProps.educationData[eduLoop].userOptions){
  let myAgeCount = nextProps.educationData[eduLoop].userOptions ;
  if (myAgeCount === "0" || myAgeCount === "Select")
  {
    this.setState({age11to18Count : 0});
  }
  else{
  let userOpt = nextProps.educationData[eduLoop].options;
  let userCountSearch =   userOpt.find( options => options.option_id === myAgeCount );
  this.setState({age11to18Count : parseInt(userCountSearch.options)});
}
  }
  else{
    this.setState({age11to18Count : 0});
  }
}



      }

    //  let per = Math.round((nextProps.educationData.length - countUserAns)*100/nextProps.educationData.length);
      // this.setState({
      //   categoryPercentage : per
      // });
    }

    if(nextProps.catPercentage !== null && nextProps.catPercentage !== ""&& nextProps.catPercentage !== undefined){
      this.setState({
        categoryPercentage : nextProps.catPercentage
      });
    }
  }
  ShowNextPage(){
    
    let nextcat = parseInt(this.state.currentOrderID) + 1;
    let data = this.state.TotalCategoryData.find(item => item.order_no == nextcat);
    //this.props.categoryView(data.cat_id, data.catName);
    this.props.history.push("/education" +"/cat"+data.cat_id + "/catName" +data.catName);
     window.location.reload();
  }

  ShowPreviousPage(){
    if(this.state.currentOrderID > 1){
      let nextcat = parseInt(this.state.currentOrderID) - 1;
    let data = this.state.TotalCategoryData.find(item => item.order_no == nextcat);
    //this.props.categoryView(data.cat_id, data.catName);
    this.props.history.push("/education" +"/cat"+data.cat_id + "/catName" +data.catName);
     window.location.reload();
    }
  }

  ShowFineshPage(){
    this.props.history.push('/profileDashboard');
  }

  onChange(qData, e) {
    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");
    if (userId && catID) {

      if(this.state.userPrimaryAgeCount == 5)
      {
        if (qData.sec_sub_ques_id) {

          let quesData = {};
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = e.target.value;
          this.setState({ selectoption: e.target.value });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
          //this.props.dispatch(fetchEduQuestions(userId, catID));
  
        } else if (qData.sub_ques_id) {
  
          let quesData = {};
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = e.target.value;
          this.setState({ selectoption: e.target.value });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
          //this.props.dispatch(fetchEduQuestions(userId, catID));
  
        } else {
          let quesData = {};
          quesData.ques_id = qData.ques_id;
          quesData.option_id = e.target.value;
          this.setState({ selectoption: e.target.value });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
          //this.props.dispatch(fetchEduQuestions(userId, catID));
        }
      } else {

     
      if(qData._id === "5bd97e7c8082461644deaeac")
      {
        // age1to4
        if(e.target.value === "Select"){
          let quesData = {};
          quesData.q_id = qData._id;
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = e.target.value;
          this.setState({ selectoption: e.target.value });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        } 
        else {
          let userCountSearch =   qData.options.find( options => options.option_id === e.target.value );
         // alert(userCountSearch.options); 

         let totalChildsCount = parseInt(userCountSearch.options) + parseInt(this.state.age5to10Count) + parseInt(this.state.age11to18Count) ;
        if(parseInt(this.state.userPrimaryAgeCount) >= totalChildsCount){
          let quesData = {};
          quesData.q_id = qData._id;
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = e.target.value;
          this.setState({ selectoption: e.target.value });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        }
        else {
          
          let quesData = {};
          quesData.q_id = qData._id;
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = "1";
          this.setState({ selectoption: 0 });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
          alert("You reached on your total childs count. Please select properly !");

        }
      }
        
      }
      //age5to10
      else if(qData._id === "5bd97f098082461644deaeb3")
      {

        if(e.target.value === "Select"){
          let quesData = {};
          quesData.q_id = qData._id;
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = e.target.value;
          this.setState({ selectoption: e.target.value });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        } 
        else {
          let userCountSearch =   qData.options.find( options => options.option_id === e.target.value );
         // alert(userCountSearch.options); 

         let totalChildsCount = parseInt(userCountSearch.options) + parseInt(this.state.age1to4Count) + parseInt(this.state.age11to18Count) ;
        if(parseInt(this.state.userPrimaryAgeCount) >= totalChildsCount){
          let quesData = {};
          quesData.q_id = qData._id;
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = e.target.value;
          this.setState({ selectoption: e.target.value });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        }
        else {
          
          let quesData = {};
          quesData.q_id = qData._id;
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = "1";
          this.setState({ selectoption: 0 });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
          alert("You reached on your total childs count. Please select properly !");

        }
      }
      }
      // age 11 to 18
      else if(qData._id === "5bd97cd88082461644deaea3")
      {


        if(e.target.value === "Select"){
          let quesData = {};
          quesData.q_id = qData._id;
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = e.target.value;
          this.setState({ selectoption: e.target.value });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        } 
        else {
          let userCountSearch =   qData.options.find( options => options.option_id === e.target.value );
         // alert(userCountSearch.options); 

         let totalChildsCount = parseInt(userCountSearch.options) + parseInt(this.state.age1to4Count) + parseInt(this.state.age5to10Count) ;
        if(parseInt(this.state.userPrimaryAgeCount) >= totalChildsCount){
          let quesData = {};
          quesData.q_id = qData._id;
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = e.target.value;
          this.setState({ selectoption: e.target.value });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        }
        else {
          
         
          let quesData = {};
          quesData.q_id = qData._id;
          quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
          quesData.sub_ques_id = qData.sub_ques_id;
          quesData.ques_id = qData.ques_id;
          quesData.option_id = "1";
          this.setState({ selectoption: 0 });
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
          alert("You reached on your total childs count. Please select properly !");

        }
      }
      }
      else {
   
      if (qData.sec_sub_ques_id) {

        let quesData = {};
        quesData.q_id = qData._id;
        quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
        quesData.sub_ques_id = qData.sub_ques_id;
        quesData.ques_id = qData.ques_id;
        quesData.option_id = e.target.value;
        this.setState({ selectoption: e.target.value });
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        //this.props.dispatch(fetchEduQuestions(userId, catID));

      } else if (qData.sub_ques_id) {

        let quesData = {};
        quesData.q_id = qData._id;
        quesData.sub_ques_id = qData.sub_ques_id;
        quesData.ques_id = qData.ques_id;
        quesData.option_id = e.target.value;
        this.setState({ selectoption: e.target.value });
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        //this.props.dispatch(fetchEduQuestions(userId, catID));

      } else {
        let quesData = {};
        quesData.q_id = qData._id;
        quesData.ques_id = qData.ques_id;
        quesData.option_id = e.target.value;
        this.setState({ selectoption: e.target.value });
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        //this.props.dispatch(fetchEduQuestions(userId, catID));
      }
    }
  }
    }
  }

  //..........................Handle change........................//

  handleChangeq12s2 = (selectedOption) => { 

    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");

    this.setState({ selectedOption });
    let ansArray = [];
    if (selectedOption.length <= parseInt(this.state.houseAns)) {
      if(selectedOption.length > 0){
      for (var i = 0; i < selectedOption.length; i++) {
        ansArray.push(selectedOption[i].value);
      }

      let quesData = {};
      quesData.sub_ques_id = selectedOption[0].ques.sub_ques_id;
      quesData.ques_id = selectedOption[0].ques.ques_id;
      quesData.optionArr = ansArray;
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));

    } else {
      let quesData = {};
      quesData.sub_ques_id = "2";
      quesData.ques_id = "12";
      quesData.optionArr = [];
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));
    }
    //console.log(`Option selected:`, selectedOption);
  }
}
  //car questions handle
  handleChangeq15s2 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");

    if (selectedOption.length <= parseInt(this.state.carAns)) {
      this.setState({ selectedOption });
      let ansArray = [];
      if (selectedOption.length > 0) {
        for (var i = 0; i < selectedOption.length; i++) {
          ansArray.push(selectedOption[i].value);
        }

        let quesData = {};
        quesData.sub_ques_id = selectedOption[0].ques.sub_ques_id;
        quesData.ques_id = selectedOption[0].ques.ques_id;
        quesData.optionArr = ansArray;
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));

      } else {
        let quesData = {};
        quesData.sub_ques_id = "2";
        quesData.ques_id = "15";
        quesData.optionArr = [];
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      }
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq18s2 = (selectedOption) => {   //mobile

    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");

    if (selectedOption.length <= parseInt(this.state.mobAns)) {
      this.setState({ selectedOption });
      let ansArray = [];
      if (selectedOption.length > 0) {
        for (var i = 0; i < selectedOption.length; i++) {
          ansArray.push(selectedOption[i].value);
        }

        let quesData = {};
        quesData.sub_ques_id = selectedOption[0].ques.sub_ques_id;
        quesData.ques_id = selectedOption[0].ques.ques_id;
        quesData.optionArr = ansArray;
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));

      } else {
        let quesData = {};
        quesData.sub_ques_id = "2";
        quesData.ques_id = "18";
        quesData.optionArr = [];
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      }
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq16s2 = (selectedOption) => { //bike 

    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");

    if (selectedOption.length <= parseInt(this.state.bikeAns)) {
      this.setState({ selectedOption });
      let ansArray = [];
      if (selectedOption.length > 0) {
        for (var i = 0; i < selectedOption.length; i++) {
          ansArray.push(selectedOption[i].value);
        }

        let quesData = {};
        quesData.sub_ques_id = selectedOption[0].ques.sub_ques_id;
        quesData.ques_id = selectedOption[0].ques.ques_id;
        quesData.optionArr = ansArray;
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));

      } else {
        let quesData = {};
        quesData.sub_ques_id = "2";
        quesData.ques_id = "16";
        quesData.optionArr = [];
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      }
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq17s2 = (selectedOption) => { //laptop

    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");

    if (selectedOption.length <= parseInt(this.state.lapAns)) {
      this.setState({ selectedOption });
      let ansArray = [];
      if (selectedOption.length > 0) {
        for (var i = 0; i < selectedOption.length; i++) {
          ansArray.push(selectedOption[i].value);
        }

        let quesData = {};
        quesData.sub_ques_id = selectedOption[0].ques.sub_ques_id;
        quesData.ques_id = selectedOption[0].ques.ques_id;
        quesData.optionArr = ansArray;
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));

      } else {
        let quesData = {};
        quesData.sub_ques_id = "2";
        quesData.ques_id = "17";
        quesData.optionArr = [];
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      }
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq42s1 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");


    this.setState({ selectedOption });
    let ansArray = [];
    if (selectedOption.length > 0) {
      for (var i = 0; i < selectedOption.length; i++) {
        ansArray.push(selectedOption[i].value);
      }

      let quesData = {};
      quesData.sub_ques_id = selectedOption[0].ques.sub_ques_id;
      quesData.ques_id = selectedOption[0].ques.ques_id;
      quesData.optionArr = ansArray;
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));

    } else {
      let quesData = {};
      quesData.sub_ques_id = "1";
      quesData.ques_id = "42";
      quesData.optionArr = [];
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq41s1 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");


    this.setState({ selectedOption });
    let ansArray = [];
    if (selectedOption.length > 0) {
      for (var i = 0; i < selectedOption.length; i++) {
        ansArray.push(selectedOption[i].value);
      }

      let quesData = {};
      quesData.sub_ques_id = selectedOption[0].ques.sub_ques_id;
      quesData.ques_id = selectedOption[0].ques.ques_id;
      quesData.optionArr = ansArray;
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));

    } else {
      let quesData = {};
      quesData.sub_ques_id = "1";
      quesData.ques_id = "41";
      quesData.optionArr = [];
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));
    }
    //console.log(`Option selected:`, selectedOption);
  }


  handleChangeq28 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");


    this.setState({ selectedOption });
    let ansArray = [];
    if(selectedOption.length <= parseInt(this.state.creditCmpnyAns)){
    if (selectedOption.length > 0) {
      for (var i = 0; i < selectedOption.length; i++) {
        ansArray.push(selectedOption[i].value);
      }

      let quesData = {};
      
      quesData.sec_sub_ques_id = selectedOption[0].ques.sec_sub_ques_id;
      quesData.sub_ques_id = selectedOption[0].ques.sub_ques_id;
      quesData.ques_id = selectedOption[0].ques.ques_id;
      quesData.optionArr = ansArray;
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));

    } else {
      let quesData = {};
      quesData.sec_sub_ques_id = "1";
      quesData.sub_ques_id = "2";
      quesData.ques_id = "43";
      quesData.optionArr = [];
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));
    }
    //console.log(`Opti
  }
  }


  handleChangeq27 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat")[1]);
    let userId = sessionStorage.getItem("user_id");


    this.setState({ selectedOption });
    let ansArray = [];
    if (selectedOption.length > 0) {
      for (var i = 0; i < selectedOption.length; i++) {
        ansArray.push(selectedOption[i].value);
      }

      let quesData = {};

      quesData.ques_id = selectedOption[0].ques.ques_id;
      quesData.optionArr = ansArray;
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));

    } else {
      let quesData = {};

      quesData.ques_id = "27";
      quesData.optionArr = [];
      quesData.ansType = "checkBox";
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));
    }
    //console.log(`Opti
  }

  render() {
    const { selectedOption } = this.state;
    let eduList = "";
    let eduItem = "";
  
    let note = "";
    if (this.props.success) {

      if (this.props.educationData && this.props.educationData.length) {
        
        let questions = this.props.educationData;
        
        eduList = Object.keys(questions).map((item, index) => {
          eduItem = questions[item];

          if (eduItem.selectionType === "select") {

            return <div id="startquestion">
              <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
              <hr/>
              <div id="quesfield">
              <Input type="select" name={index} value={this.state.option[index]} onChange={this.onChange.bind(this, eduItem)}>
                <option>Select</option>
                {
                  eduItem.options.map((optionItem, i) => {

                    //eduItem
                    if (eduItem.userOptions == 0) {
                      return <option key={i} value={optionItem.option_id}   > {optionItem.options} </option>

                    }
                    else {
                      if (eduItem.userOptions == optionItem.option_id) {
                        return <option selected key={i} value={optionItem.option_id}   > {optionItem.options} </option>
                      }
                      else {
                        return <option key={i} value={optionItem.option_id}   > {optionItem.options} </option>
                      }
                    }
                  })
                }
              </Input>
              </div>
            </div>
          } else if (eduItem.selectionType === "checkBox") {

            var employees = {
              accounting: []
            };
            var employees1 = {
              accounting1: []
            };



            if (eduItem.userOptions) {


              if (eduItem.userOptions.length > 0) {

                for (var userOpt = 0; userOpt < eduItem.options.length; userOpt++) {
                  var item = eduItem.options[userOpt];

                  if (!eduItem.userOptions.includes(item.option_id)) {
                    employees.accounting.push({
                      "value": item.option_id,
                      "label": item.options,
                      "ques": eduItem
                    });
                  }
                  else {
                    employees1.accounting1.push({
                      "value": item.option_id,
                      "label": item.options,
                      "ques": eduItem
                    });

                  }
                }
              }
              else {
                for (var j in eduItem.options) {
                  var item = eduItem.options[j];
                  employees.accounting.push({
                    "value": item.option_id,
                    "label": item.options,
                    "ques": eduItem
                  });
                }
              }
            } else {
              for (var j in eduItem.options) {
                var item = eduItem.options[j];
                employees.accounting.push({
                  "value": item.option_id,
                  "label": item.options,
                  "ques": eduItem
                });
              }
            }

            //--------------------------checkbox options----------------------------------//

            if (eduItem._id == "5bd1968d9a7ee00f7064d15e") {
              return (
                <div id="startquestion">
                <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
                <hr/>
                 <div id="quesfield"> 
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq12s2}
                    isMulti={true}
                    options={employees.accounting}
                  />
                </div>
               </div>
              )
            }
            else if (eduItem._id == "5bd19af09a7ee00f7064d182") {

              return (
                
                <div id="startquestion">
                <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
                <hr/>
                <div id="quesfield">
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq15s2}
                    isMulti={true}
                    options={employees.accounting}
                  /></div>
                {/* </div> */}
                </div>
              )
            }
            //mobile
            else if (eduItem._id == "5bd19fc89a7ee00f7064d1c3") {

              return (
                <div id="startquestion">
                <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
                <hr/>
                <div id="quesfield">
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq18s2}
                    isMulti={true}
                    options={employees.accounting}
                  />
                </div>
                </div>
              )
            }
            //bike
            else if (eduItem._id == "5bd19cc89a7ee00f7064d199") {

              return (
                <div id="startquestion">
                <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
                <hr/>
                <div id="quesfield">
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq16s2}
                    isMulti={true}
                    options={employees.accounting}
                  />
                </div>
                </div>
              )
            }
            else if (eduItem._id == "5bd19e4f9a7ee00f7064d1ae") { //laptop

              return (
                <div id="startquestion">
                <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
                <hr/>
                <div id="quesfield">
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq17s2}
                    isMulti={true}
                    options={employees.accounting}
                  />
                </div>
                </div>
              )
            }
            else if (eduItem._id == "5bd17d049a7ee00f7064d080") {

              return (
                <div id="startquestion">
                <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
                <hr/>
                <div id="quesfield">
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq42s1}
                    isMulti={true}
                    options={employees.accounting}
                  />
                </div>
                </div>
              )
            }
            else if (eduItem._id == "5bd17be29a7ee00f7064d062") {

              return (
                <div id="startquestion">
                <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
                <hr/>
                <div id="quesfield">
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq41s1}
                    isMulti={true}
                    options={employees.accounting}
                  />
                </div>
                </div>
              )
            }
            else if (eduItem._id == "5bd192c29a7ee00f7064d14b") {

              return (
                <div id="startquestion">
                <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
                <hr/>
                <div id="quesfield">
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq27}
                    isMulti={true}
                    options={employees.accounting}
                  />
                </div>
                </div>                
              )
            }else if (eduItem._id == "5c5e7a0ad394800a08ac3532") {

              //5c5e61498421c0144824aed9   test
              //5c5e7a0ad394800a08ac3532 live

              return (
                <div id="startquestion">
                <div id="questxt">{index + 1 + ". "}{eduItem.questions}</div>
                <hr/>
                <div id="quesfield">
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq28}
                    isMulti={true}
                    options={employees.accounting}
                  />
                </div>
                </div>                
              )
            }

          }
        });
      }

    }

    let showNextButton = '';
    let showPrevButton = '';
    if(this.props.educationData){
      if(this.state.currentOrderID < this.state.TotalCategory){
        showNextButton = <div><h4><span  onClick={this.ShowNextPage.bind(this)}>Next</span></h4></div>
      }else{
        showNextButton = <div><span   onClick={this.ShowFineshPage.bind(this)}><h4>Finish</h4></span></div>
      }

      if(this.props.educationData[0].cat_id !== "7" ){
        showPrevButton = <span  onClick={this.ShowPreviousPage.bind(this)}><h4>Prev</h4></span> 
      }
      
    }

    //ShowPreviousPage


    return (

      <div id="studycountmain">
        <div id="questionheader">
            <div id="previous">{showPrevButton}</div>
            <div id="next">{showNextButton}</div>
          </div>
          <div id="questions">
          
          <div id="questionprogressbar">
          <div style={{width:""}}><CircularProgressbar className = "totalQuestionsProgressbar" percentage={this.state.categoryPercentage} text={`${this.state.categoryPercentage}%`} /><span >{this.state.categoryName.toUpperCase()+" QUESTION"} </span></div>
          </div>
          {eduList}
          </div>
      </div>       
    );
  }
}

const mapStateToProps = (state) => {
  return {
    educationData: state.profileDashboard.educationData,
    subQuestions: state.profileDashboard.subQuestions,
    success: state.profileDashboard.success,
    catPercentage : state.profileDashboard.catPercentage,
    requestStarted: state.common.requestStarted
  }
}

export default connect(mapStateToProps)(Education);
