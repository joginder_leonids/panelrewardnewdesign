import React, { Component } from 'react';
import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { Card, Button, CardTitle, CardText, CardHeader, CardBody, Row, Col } from 'reactstrap';
import './style.css';
import { Link } from 'react-router-dom';
import onlinesurvey from '../../images/Online-Surveys.jpg';
import editprofile from '../../icons/edit.png';
class ProfileDashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
    };
  }
  
  componentDidMount(){
    document.title = "Panel Reward-DemoGraphics";
    sessionStorage.removeItem('categoriesData');
  }

  catView(data) {
    this.props.catView(data);
  }

  componentWillReceiveProps(nextProps){
    
  }

  render() {

    //store category data in season
    if(this.props.profileCategory.categoryData && this.props.profileCategory.categoryData.length){
      let category = this.props.profileCategory.categoryData;
      localStorage.setItem("categoriesData", JSON.stringify(category));
    }

    let categoryList = "";
    let categoryItem = "";
    if (this.props.profileCategory.categoryData && this.props.profileCategory.categoryData.length>0) {
       let categoriesData = this.props.profileCategory.categoryData;

      let primaryPer = categoriesData[0].percentage;
      let housePer = categoriesData[1].percentage;
      let shopPer = categoriesData[2].percentage;
      let employePer = categoriesData[3].percentage;
      let InsuBankPer = categoriesData[4].percentage;
      let oragnizationPer = categoriesData[5].percentage;
      let eduPer = categoriesData[6].percentage;

      if(categoriesData[0].percentage > 100){
        primaryPer = 100;
      }
      if(categoriesData[1].percentage > 100){
        housePer = 100;
      }
      if(categoriesData[2].percentage > 100){
        shopPer = 100;
      }
      if(categoriesData[3].percentage > 100){
        employePer = 100;
      }
      if(categoriesData[4].percentage > 100){
        InsuBankPer = 100;
      }
      if(categoriesData[5].percentage > 100){
        oragnizationPer = 100;
      }
      if(categoriesData[6].percentage > 100){
        eduPer = 100;
      }


       categoryList = <div>
          <div className="firstlayer">
            <div id="firstbutton" className="firstlayercommon" ><h2>{categoriesData[0].catName}</h2>
            <CircularProgressbar className="profileDashboardProgressbar" percentage={primaryPer} text={`${primaryPer}%`} /><br/><br/>
            <h6><button className="catBox"  onClick={this.catView.bind(this, categoriesData[0])}> <img src={editprofile} height="18px" width="18px" /> &nbsp;Update {categoriesData[0].catName}</button></h6>
            </div>
            <div id="secondbutton" className="firstlayercommon"><h2>{categoriesData[1].catName}</h2>
            <CircularProgressbar className="profileDashboardProgressbar" percentage={housePer} text={`${housePer}%`} /><br/><br/>
            <h6><button className="catBox"  onClick={this.catView.bind(this, categoriesData[1])}><img src={editprofile} height="18px" width="18px" /> &nbsp;Update {categoriesData[1].catName}</button></h6>
            </div>
            <div id="thirdbutton" className="firstlayercommon"><h2>{categoriesData[2].catName}</h2>
            <CircularProgressbar className="profileDashboardProgressbar" percentage={shopPer} text={`${shopPer}%`} /><br/><br/>
            <h6><button className="catBox"  onClick={this.catView.bind(this, categoriesData[2])}><img src={editprofile} height="18px" width="18px" /> &nbsp;Update {categoriesData[2].catName}</button></h6>
            </div>
          </div>

          <div className="secondlayer">
            <div id="fourthbutton" className="firstlayercommon"><h2>{categoriesData[3].catName}</h2>
            <CircularProgressbar className="profileDashboardProgressbar" percentage={employePer} text={`${employePer}%`} /><br/><br/>
            <h6><button className="catBox"  onClick={this.catView.bind(this, categoriesData[3])}><img src={editprofile} height="18px" width="18px" /> &nbsp;Update {categoriesData[3].catName}</button></h6>
            </div>
            <div id="fifthbutton" className="firstlayercommon"><h2>{categoriesData[4].catName}</h2>
            <CircularProgressbar className="profileDashboardProgressbar" percentage={InsuBankPer} text={`${InsuBankPer}%`} /><br/><br/>
            <h6><button className="catBox"  onClick={this.catView.bind(this, categoriesData[4])}><img src={editprofile} height="18px" width="18px" /> &nbsp;Update {categoriesData[4].catName}</button></h6>
            </div>
            <div id="sixthbutton" className="firstlayercommon"><h2>{categoriesData[5].catName}</h2>
            <CircularProgressbar className="profileDashboardProgressbar" percentage={oragnizationPer} text={`${oragnizationPer}%`} /><br/><br/>
            <h6><button className="catBox"  onClick={this.catView.bind(this, categoriesData[5])}><img src={editprofile} height="18px" width="18px" /> &nbsp;Update {categoriesData[5].catName}</button></h6>
            </div>
          </div>

          <div className="thirdlayer">
            <div id="seventhbutton" className="firstlayercommon"><h2>{categoriesData[6].catName}</h2>
            <CircularProgressbar className="profileDashboardProgressbar" percentage={eduPer} text={`${eduPer}%`} /><br/><br/>
            <h6><button className="catBox"  onClick={this.catView.bind(this, categoriesData[6])}><img src={editprofile} height="18px" width="18px" /> &nbsp;Update {categoriesData[6].catName}</button></h6>
            </div>
          </div>
        </div>

      // categoryList = Object.keys(categories).map((item, index) => {
      //   categoryItem = categories[item];
      //   return <div key={index}>
      //     <button className="catBox"  onClick={this.catView.bind(this, categoryItem)}>
      //       <h3>{categoryItem.catName}</h3><br/>
      //       <CircularProgressbar className="progressbar" percentage={categoryItem.percentage} text={`${categoryItem.percentage}%`} /><br/><br/>
      //       <h6>Update {categoryItem.catName} .</h6>
      //         </button>
      //   </div>
      // });


    }else{
      categoryList= <div className="loading">{
        this.props.requestStarted ?
          <div className="preloader" ><LoadingDots interval={100} dots={5} /></div>
          :
          false
      }</div>
    }

    return (
      <div id="studycountmain">
      <div id="headerstudy">
          <h3>Manage Profile</h3>
          <ol id="breadcrumb">
            <li>Home</li> &nbsp;/&nbsp;
            <li style={{ opacity: '0.8' }}>Manage Profile</li>
          </ol>
        </div>
        <div id="profiledashboard">

          {categoryList}
        </div>

        </div>
       


      // <div className="profile" style={{marginTop: "40px"}}> 
      //   <div className="dashboardback">
      //   <Card style={{marginLeft:'10%',marginRight:'10%',marginTop:'1.5%'}}>
      //     <CardHeader><h3 className="text-center">Demo Graphic Dashboard</h3></CardHeader>
         
      //     <div id="cardback">
      //       <CardBody>
      //       {categoryList}
            
      //       </CardBody>
      //       </div>
      //   </Card>
      //    </div>
      //    </div>
      
   
    );
  }
}
export default ProfileDashboard;
