import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './style.css';

import Modal from 'react-awesome-modal';
import profilecompleteimg from '../../images/prolilecomplete.jpg';


class ProfileView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      editText: true,
      email: "",
      contact: "",
      password: "",
      firstName: "",
      lastName: "",
      dob: "",
      gender: "",
      addressLineOne: "",
      addressLineTwo: "",
      state: "",
      country: "",
      pinCode: "",
      marriageStatus: "",
      visible: false,
      notification: "",
      countryData: [],
      countryCode:"",
    };
    this.edit = this.edit.bind(this);
    // this.onChange = this.onChange.bind(this);
    this.firstNameChange=this.firstNameChange.bind(this);
    this.lastNameChange=this.lastNameChange.bind(this);
    this.regmailChange=this.regmailChange.bind(this);
    this.contactChange=this.contactChange.bind(this);
    this.dobChange=this.dobChange.bind(this);
    this.genderChange=this.genderChange.bind(this);
    this.marriageChange=this.marriageChange.bind(this);
    this.addoneChange = this.addoneChange.bind(this);
    this.addtwoChange=this.addtwoChange.bind(this);
    this.countryChange=this.countryChange.bind(this);
    this.stateChange=this.stateChange.bind(this);
    this.pinChange=this.pinChange.bind(this);
    this.passChange=this.passChange.bind(this);
    this.updateProfile = this.updateProfile.bind(this);
    this.test = this.test.bind(this);
  }

  componentDidMount() {
    var yyyy;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var maxyyyy = today.getFullYear();
     if(dd<10){
            dd='0'+dd;
        } 
        if(mm<10){
            mm='0'+mm;
        } 
    yyyy=maxyyyy-15;
    today = yyyy+'-'+12+'-'+31;
    document.getElementById("prodateofbirth").setAttribute("max", today);
    var mmm='0'+1;
    var ddd='0'+1;
      var minyyyy=yyyy-80;
      var mindate=minyyyy+'-'+mmm+'-'+ddd;
      document.getElementById("prodateofbirth").setAttribute("min", mindate);
    




    this.setState({ countryData : 
      [
        {"name": "Afghanistan", "iso2": "AF", "code": "+93"},
        {"name": "Albania", "iso2": "AL", "code": "+355"},
        {"name": "Algeria", "iso2": "DZ", "code": "+213"},
        {"name": "American Samoa", "iso2": "AS", "code": "+1-684"},
        {"name": "Andorra", "iso2": "AD", "code": "+376"},
        {"name": "Angola", "iso2": "AO", "code": "+244"},
        {"name": "Anguilla", "iso2": "AI", "code": "+1-264"} ,
        {"name": "Antarctica","iso2": "AQ","code": "+672"},
        {"name": "Antigua And Barbuda", "iso2": "AG", "code": "+1-268" },
        {"name": "Argentina", "iso2": "AR", "code": "+54" },
        {"name": "Armenia", "iso2": "AM", "code": "+374" },
        {"name": "Aruba", "iso2": "AW","code": "+297"},
        {"name": "Ascension Island", "iso2": "AC","code": "+247"},
        {"name": "Australia", "iso2": "AU", "code": "+61"},
        {"name": "Austria", "iso2": "AT", "code": "+43"},
        {"name": "Azerbaijan", "iso2": "AZ", "code": "+994"},
        {"name": "Bahamas", "iso2": "BS", "code": "+1-242"},
        {"name": "Bahrain", "iso2": "BH", "code": "+973"},
        {"name": "Bangladesh", "iso2": "BD", "code": "+880"},
        {"name": "Barbados", "iso2": "BB", "code": "+1-246"},
        {"name": "Belarus", "iso2": "BY", "code": "+375" },
        {"name": "Belgium", "iso2": "BE", "code": "+32"},
        {"name": "Belize", "iso2": "BZ", "code": "+501"},
        {"name": "Benin", "iso2": "BJ", "code": "+229"},
        {"name": "Bermuda", "iso2": "BM", "code": "+1-441"},
        {"name": "Bhutan", "iso2": "BT", "code": "+975"},
        {"name": "Bolivia, Plurinational State Of", "iso2": "BO", "code": "+591"},
        {"name": "Bonaire, Saint Eustatius And Saba", "iso2": "BQ", "code": "+599"},
        {"name": "Bosnia & Herzegovina", "iso2": "BA", "code": "+387" },
        {"name": "Botswana", "iso2": "BW", "code": "+267"},
        {"name": "Brazil", "iso2": "BR", "code": "+55"},
        {"name": "British Indian Ocean Territory", "iso2": "IO", "code": "+246" },
        {"name": "Brunei Darussalam", "iso2": "BN", "code": "+673" },
        {"name": "Bulgaria", "iso2": "BG", "code": "+359"},
        {"name": "Burkina Faso", "iso2": "BF", "code": "+226" },
        {"name": "Burundi", "iso2": "BI", "code": "+257" },
        {"name": "Cambodia", "iso2": "KH", "code": "+855" },
        {"name": "Cameroon", "iso2": "CM", "code": "+237" },
        {"name": "Canada", "iso2": "CA", "code": "+1" },
        {"name": "Cape Verde", "iso2": "CV", "code": "+238" },
        {"name": "Cayman Islands", "iso2": "KY", "code": "+1-345" },
        {"name": "Central African Republic", "iso2": "CF", "code": "+236" },
        {"name": "Chad","iso2": "TD","code": "+235"},
        {"name": "Chile","iso2": "CL","code": "+56"},
        {"name": "China","iso2": "CN","code": "+86"},
        {"name": "Christmas Island","iso2": "CX","code": "+61"},
        {"name": "Cocos (Keeling) Islands","iso2": "CC","code": "+61"},
        {"name": "Colombia","iso2": "CO","code": "+57"},
        {"name": "Comoros","iso2": "KM","code": "+269"},
        {"name": "Cook Islands","iso2": "CK","code": "+682"},
        {"name": "Costa Rica","iso2": "CR","code": "+506"},
        {"name": "Cote d'Ivoire","iso2": "CI","code": "+225"},
        {"name": "Croatia","iso2": "HR","code": "+385"},
        {"name": "Cuba","iso2": "CU","code": "+53"},
        {"name": "Curacao","iso2": "CW","code": "+599"},
        {"name": "Cyprus","iso2": "CY","code": "+357"},
        {"name": "Czech Republic","iso2": "CZ","code": "+420"},
        {"name": "Democratic Republic Of Congo","iso2": "CD","code": "+243"},
        {"name": "Denmark","iso2": "DK","code": "+45"},
        {"name": "Djibouti","iso2": "DJ","code": "+253"},
        {"name": "Dominica","iso2": "DM","code": "+1-767"},
        {"name": "Dominican Republic","iso2": "DO","code": "+1-809"},
        {"name": "East Timor","iso2": "TL","code": "+670"},
        {"name": "Ecuador","iso2": "EC","code": "+593"},
        {"name": "Egypt","iso2": "EG","code": "+20"},
        {"name": "El Salvador","iso2": "SV","code": "+503"},
        {"name": "Equatorial Guinea","iso2": "GQ","code": "+240"},
        {"name": "Eritrea","iso2": "ER","code": "+291"},
        {"name": "Estonia","iso2": "EE","code": "+372"},
        {"name": "Ethiopia","iso2": "ET","code": "+251"},
        {"name": "European Union","iso2": "EU","code": "+388"},
        {"name": "Falkland Islands","iso2": "FK","code": "+500"},
        {"name": "Faroe Islands","iso2": "FO","code": "+298"},
        {"name": "Fiji","iso2": "FJ","code": "+679"},
        {"name": "Finland","iso2": "FI","code": "+358"},
        {"name": "France","iso2": "FR","code": "+33"},
        {"name": "France, Metropolitan","iso2": "FX","code": "+241"},
        {"name": "French Guiana","iso2": "GF","code": "+44"},
        {"name": "French Polynesia","iso2": "PF","code": "+689"},
        {"name": "Gabon","iso2": "GA","code": "+44"},
        {"name": "Gambia","iso2": "GM","code": "+220"},
        {"name": "Georgia","iso2": "GE","code": "+594"},
        {"name": "Germany","iso2": "DE","code": "+49"},
        {"name": "Ghana","iso2": "GH","code": "+233"},
        {"name": "Gibraltar","iso2": "GI","code": "+350"},
        {"name": "Greece","iso2": "GR","code": "+30"},
        {"name": "Greenland","iso2": "GL","code": "+299"},
        {"name": "Grenada","iso2": "GD","code": "+995"},
        {"name": "Guadeloupe","iso2": "GP","code": "+590"},
        {"name": "Guam","iso2": "GU","code": "+1 671"},
        {"name": "Guatemala","iso2": "GT","code": "+502"},
        {"name": "Guinea","iso2": "GN","code": "+224"},
        {"name": "Guinea-bissau","iso2": "GW","code": "+245"},
        {"name": "Guyana","iso2": "GY","code": "+592"},
        {"name": "Haiti","iso2": "HT","code": "+509"},
        {"name": "Honduras","iso2": "HN","code": "+504"},
        {"name": "Hong Kong","iso2": "HK","code": "+852"},
        {"name": "Hungary","iso2": "HU","code": "+36"},
        {"name": "Iceland","iso2": "IS","code": "+354"},
        {"name": "India","iso2": "IN","code": "+91"},
        {"name": "Indonesia","iso2": "ID","code": "+62"},
        {"name": "Iran, Islamic Republic Of","iso2": "IR","code": "+98"},
        {"name": "Iraq","iso2": "IQ","code": "+964"},
        {"name": "Ireland","iso2": "IE","code": "+353"},
        {"name": "Isle Of Man","iso2": "IM","code": "+44"},
        {"name": "Israel","iso2": "IL","code": "+972"},
        {"name": "Italy","iso2": "IT","code": "+39"},
        {"name": "Jamaica","iso2": "JM","code": "+1-876"},
        {"name": "Japan","iso2": "JP","code": "+81"},
        {"name": "Jersey","iso2": "JE","code": "+44"},
        {"name": "Jordan","iso2": "JO","code": "+962"},
        {"name": "Kazakhstan","iso2": "KZ","code": "+7"},
        {"name": "Kenya","iso2": "KE","code": "+254"},
        {"name": "Kiribati","iso2": "KI","code": "+686"},
        {"name": "Korea, Democratic People's Republic Of","iso2": "KP","code": "+850"},
        {"name": "Korea, Republic Of","iso2": "KR","code": "+82"},
        {"name": "Kuwait","iso2": "KW","code": "+965"},
        {"name": "Kyrgyzstan","iso2": "KG","code": "+996"},
        {"name": "Lao People's Democratic Republic","iso2": "LA","code": "+856"},
        {"name": "Latvia","iso2": "LV","code": "+371"},
        {"name": "Lebanon","iso2": "LB","code": "+961"},
        {"name": "Lesotho","iso2": "LS","code": "+266"},
        {"name": "Liberia","iso2": "LR","code": "+231"},
        {"name": "Libya","iso2": "LY","code": "+218"},
        {"name": "Liechtenstein","iso2": "LI","code": "+423"},
        {"name": "Lithuania","iso2": "LT","code": "+370"},
        {"name": "Luxembourg","iso2": "LU","code": "+352"},
        {"name": "Macao","iso2": "MO","code": "+853"},
        {"name": "Macedonia, The Former Yugoslav Republic Of","iso2": "MK","code": "+389"},
        {"name": "Madagascar","iso2": "MG","code": "+261"},
        {"name": "Malawi","iso2": "MW","code": "+265"},
        {"name": "Malaysia","iso2": "MY","code": "+60"},
        {"name": "Maldives","iso2": "MV","code": "+960"},
        {"name": "Mali","iso2": "ML","code": "+223"},
        {"name": "Malta","iso2": "MT","code": "+356"},
        {"name": "Marshall Islands","iso2": "MH","code": "+692"},
        {"name": "Martinique","iso2": "MQ","code": "+596"},
        {"name": "Mauritania","iso2": "MR","code": "+222"},
        {"name": "Mauritius","iso2": "MU","code": "+230"},
        {"name": "Mayotte","iso2": "YT","code": "+262"},
        {"name": "Mexico","iso2": "MX","code": "+52"},
        {"name": "Micronesia, Federated States Of","iso2": "FM","code": "+691"},
        {"name": "Moldova","iso2": "MD","code": "+373"},
        {"name": "Monaco","iso2": "MC","code": "+377"},
        {"name": "Mongolia","iso2": "MN","code": "+976"},
        {"name": "Montenegro","iso2": "ME","code": "+382"},
        {"name": "Montserrat","iso2": "MS","code": "+1-664"},
        {"name": "Morocco","iso2": "MA","code": "+212"},
        {"name": "Mozambique","iso2": "MZ","code": "+258"},
        {"name": "Myanmar","iso2": "MM","code": "+95"},
        {"name": "Namibia","iso2": "NA","code": "+264"},
        {"name": "Nauru","iso2": "NR","code": "+674"},
        {"name": "Nepal","iso2": "NP","code": "+977"},
        {"name": "Netherlands","iso2": "NL","code": "+31"},
        {"name": "New Caledonia","iso2": "NC","code": "+687"},
        {"name": "New Zealand","iso2": "NZ","code": "+64"},
        {"name": "Nicaragua","iso2": "NI","code": "+505"},
        {"name": "Niger","iso2": "NE","code": "+227"},
        {"name": "Nigeria","iso2": "NG","code": "+234"},
        {"name": "Niue","iso2": "NU","code": "+683"},
        {"name": "Norfolk Island","iso2": "NF","code": "+672"},
        {"name": "Northern Mariana Islands","iso2": "MP","code": "+1-670"},
        {"name": "Norway","iso2": "NO","code": "+47"},
        {"name": "Oman","iso2": "OM","code": "+968"},
        {"name": "Pakistan","iso2": "PK","code": "+92"},
        {"name": "Palau","iso2": "PW","code": "+680"},
        {"name": "Palestinian Territory, Occupied","iso2": "PS","code": "+970"},
        {"name": "Panama","iso2": "PA","code": "+507"},
        {"name": "Papua New Guinea","iso2": "PG","code": "+675"},
        {"name": "Paraguay","iso2": "PY","code": "+595"},
        {"name": "Peru","iso2": "PE","code": "+51"},
        {"name": "Philippines","iso2": "PH","code": "+63"},    
        {"name": "Poland","iso2": "PL","code": "+48"},
        {"name": "Portugal","iso2": "PT","code": "+351"},
        {"name": "Puerto Rico","iso2": "PR","code": "+1-787"},
        {"name": "Qatar","iso2": "QA","code": "+974"},
        {"name": "Republic Of Congo","iso2": "CG","code": "+242"},
        {"name": "Reunion","iso2": "RE","code": "+262"},
        {"name": "Romania","iso2": "RO","code": "+40"},
        {"name": "Russian Federation","iso2": "RU","code": "+7"},
        {"name": "Rwanda","iso2": "RW","code": "+250"},
        {"name": "Saint Barthélemy","iso2": "BL","code": "+590"},
        {"name": "Saint Helena, Ascension And Tristan Da Cunha","iso2": "SH","code": "+290"},
        {"name": "Saint Kitts And Nevis","iso2": "KN","code": "+1-869"},
        {"name": "Saint Lucia","iso2": "LC","code": "+1-758"},
        {"name": "Saint Martin","iso2": "MF","code": "+590"},
        {"name": "Saint Pierre And Miquelon","iso2": "PM","code": "+508"},
        {"name": "Saint Vincent And The Grenadines","iso2": "VC","code": "+1-784"},
        {"name": "Samoa","iso2": "WS","code": "+685"},
        {"name": "San Marino","iso2": "SM","code": "+378"},
        {"name": "Sao Tome And Principe","iso2": "ST","code": "+239"},
        {"name": "Saudi Arabia","iso2": "SA","code": "+966"},
        {"name": "Senegal","iso2": "SN","code": "+221"},
        {"name": "Serbia","iso2": "RS","code": "+381"},
        {"name": "Seychelles","iso2": "SC","code": "+248"},
        {"name": "Sierra Leone","iso2": "SL","code": "+232"},
        {"name": "Singapore","iso2": "SG","code": "+65"},
        {"name": "Sint Maarten","iso2": "SX","code": "+1-721"},
        {"name": "Slovakia","iso2": "SK","code": "+421"},
        {"name": "Slovenia","iso2": "SI","code": "+386"},
        {"name": "Solomon Islands","iso2": "SB","code": "+677"},
        {"name": "Somalia","iso2": "SO","code": "+252"},
        {"name": "South Africa","iso2": "ZA","code": "+27"},
        {"name": "South Georgia And The South Sandwich Islands","iso2": "GS","code": "+500"},
        {"name": "Spain","iso2": "ES","code": "+34"},
        {"name": "Sri Lanka","iso2": "LK","code": "+94"},
        {"name": "Sudan","iso2": "SD","code": "+249"},
        {"name": "Suriname","iso2": "SR","code": "+597"},
        {"name": "Svalbard And Jan Mayen","iso2": "SJ","code": "+47"},
        {"name": "Swaziland","iso2": "SZ","code": "+268"},
        {"name": "Sweden","iso2": "SE","code": "+46"},
        {"name": "Switzerland","iso2": "CH","code": "+41"},
        {"name": "Syrian Arab Republic","iso2": "SY","code": "+963"},
        {"name": "Taiwan, Province Of China","iso2": "TW","code": "+886"},
        {"name": "Tajikistan","iso2": "TJ","code": "+992"},
        {"name": "Tanzania, United Republic Of","iso2": "TZ","code": "+255"},
        {"name": "Thailand","iso2": "TH","code": "+66"},
        {"name": "Togo","iso2": "TG","code": "+228"},
        {"name": "Tokelau","iso2": "TK","code": "+690"},
        {"name": "Tonga","iso2": "TO","code": "+676"},
        {"name": "Trinidad And Tobago","iso2": "TT","code": "+1-868"},
        {"name": "Tristan de Cunha","iso2": "TA","code": "+290"},
        {"name": "Tunisia","iso2": "TN","code": "+216"},
        {"name": "Turkey","iso2": "TR","code": "+90"},
        {"name": "Turkmenistan","iso2": "TM","code": "+993"},
        {"name": "Turks And Caicos Islands","iso2": "TC","code": "+1-649"},
        {"name": "Tuvalu","iso2": "TV","code": "+688"},
        {"name": "USSR","iso2": "SU","code": "+7"},
        {"name": "Uganda","iso2": "UG","code": "+256"},
        {"name": "Ukraine","iso2": "UA","code": "+380"},
        {"name": "United Arab Emirates","iso2": "AE","code": "+971"},
        {"name": "United Kingdom","iso2": "GB","code": "+1-473"},
        {"name": "United Kingdom","iso2": "UK","code": "+44"},
        {"name": "United States","iso2": "US","code": "+1"},
        {"name": "United States Minor Outlying Islands","iso2": "UM","code": "+1"},
        {"name": "Uruguay","iso2": "UY","code": "+598"},
        {"name": "Uzbekistan","iso2": "UZ","code": "+998"},
        {"name": "Vanuatu","iso2": "VU","code": "+678"},
        {"name": "Vatican City State","iso2": "VA","code": "+379"},
        {"name": "Venezuela, Bolivarian Republic Of","iso2": "VE","code": "+58"},
        {"name": "Viet Nam","iso2": "VN","code": "+84"},
        {"name": "Virgin Islands (British)","iso2": "VG","code": "+1-284"},
        {"name": "Virgin Islands (US)","iso2": "VI","code": "+1-340"},
        {"name": "Wallis And Futuna","iso2": "WF","code": "+681"},
        {"name": "Western Sahara","iso2": "EH","code": "+212"},
        {"name": "Yemen","iso2": "YE","code": "+967"},
        {"name": "Zambia","iso2": "ZM","code": "+260"},
        {"name": "Zimbabwe","iso2": "ZW","code": "+263"}
      ]
    });

    if (this.props.userProfile === undefined || this.props.userProfile === null) {
      this.setState({

      });
    }
    else {
      // let countryCodeNo = this.state.countryData.find(item => item.name == this.props.userProfile[0].Country).code;
      // this.setState({ countryCode: countryCodeNo, contact: '' });

      this.setState({
        email: this.props.userProfile[0].Email,
        //contact: this.props.userProfile[0].Contact,
        password: this.props.userProfile[0].Password,
        firstName: this.props.userProfile[0].First_name,
        lastName: this.props.userProfile[0].Last_name,
        dob: this.props.userProfile[0].D_O_B,
        gender: this.props.userProfile[0].Gender,
        addressLineOne: this.props.userProfile[0].Address_line_one,
        addressLineTwo: this.props.userProfile[0].Address_line_two,
        state: this.props.userProfile[0].State,
        country: this.props.userProfile[0].Country,
        pinCode: this.props.userProfile[0].Pincode,
        marriageStatus: this.props.userProfile[0].Marriage_status,
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.userProfileUpdate.success) {
      this.setState({
        visible: true,
        notification: nextProps.userProfileUpdate.message
      });
      // alert(nextProps.userProfileUpdate.message);
    }
    if(nextProps.profileImageUploadRes){
      if(nextProps.profileImageUploadRes.success === true){
        alert(nextProps.profileImageUploadRes.message);
        //sessionStorage.setItem('userImagePath', appConfig.imageUrl + nextProps.profileImageUploadRes.imagePath);
       // window.location.reload();
      }else{
        alert(nextProps.profileImageUploadRes.error);
      }
    }
  }
  

  closeModal() {
    this.setState({
      visible: false
    });
    this.props.setProfileResNull();
  }



  // onChange(e) {
  //   this.setState({ [e.target.name]: e.target.value });
  // }
  firstNameChange(e) {
  
    if (e.target.value === "") {
        document.getElementById("profirstnamealphaerror").style.display = "none";
        document.getElementById("profirstnameerror").style.display = "block";
    } else if (!e.target.value.match(/^[a-zA-Z ]*$/)) {
        document.getElementById("profirstnamealphaerror").style.display = "block";
        document.getElementById("profirstnameerror").style.display = "none";
    } else {
        document.getElementById("profirstnamealphaerror").style.display = "none";
        document.getElementById("profirstnameerror").style.display = "none";
    }
    this.setState({ firstName: e.target.value });
}
lastNameChange(e) {
  if (e.target.value === "") {
      document.getElementById("prolastnamealphaerror").style.display = "none";
      document.getElementById("prolastnameerror").style.display = "block";
  } else if (!e.target.value.match(/^[a-zA-Z ]*$/)) {
      document.getElementById("prolastnamealphaerror").style.display = "block";
      document.getElementById("prolastnameerror").style.display = "none";
  } else {
      document.getElementById("prolastnamealphaerror").style.display = "none";
      document.getElementById("prolastnameerror").style.display = "none";
  }
  this.setState({ lastName: e.target.value });
}

  regmailChange(e) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    let tempemail = e.target.value;
    if (e.target.value === "") {
        document.getElementById("proregmailvaliderror").style.display = "none";
        document.getElementById("proregmailerror").style.display = "block";
    } else if (!pattern.test(tempemail)) {
        document.getElementById("proregmailvaliderror").style.display = "block";
        document.getElementById("proregmailerror").style.display = "none";
    } else {
        document.getElementById("proregmailvaliderror").style.display = "none";
        document.getElementById("proregmailerror").style.display = "none";
    }
    this.setState({ email: e.target.value });
  }

    contactChange(e) {
      if (e.target.value === "") {
          document.getElementById("procontacterror").style.display = "block";
          document.getElementById("procontactnumbererror").style.display = "none";
      } else if(e.target.value==="+"){
          e.target.value="";
      } else if(isNaN(e.target.value)){
        document.getElementById("procontactnumbererror").style.display = "block";
        document.getElementById("procontacterror").style.display = "none";
      }else {
          document.getElementById("procontacterror").style.display = "none";
        document.getElementById("procontactnumbererror").style.display = "none";

      }
      this.setState({ contact: e.target.value });
  }

    dobChange(e) {
      if (e.target.value === "") {
          document.getElementById("prodoberror").style.display = "block";
      } else {
          document.getElementById("prodoberror").style.display = "none";

      }
      this.setState({ dob: e.target.value });
  }

    genderChange(e) {
      if (e.target.value === "0") {
          document.getElementById("progendererror").style.display = "block";
      } else {
          document.getElementById("progendererror").style.display = "none";

      }
      this.setState({ gender: e.target.value });
  }

    marriageChange(e) {
      if (e.target.value === "0") {
          document.getElementById("promarriageerror").style.display = "block";
      } else {
          document.getElementById("promarriageerror").style.display = "none";

      }
      this.setState({ marriageStatus: e.target.value });
  }
      addoneChange(e) {
        if (e.target.value === "") {
            document.getElementById("prolineoneerror").style.display = "block";
        } else {
            document.getElementById("prolineoneerror").style.display = "none";

        }
        this.setState({ addressLineOne: e.target.value });
    }

    addtwoChange(e) {
      if (e.target.value === "") {
          document.getElementById("prolinetwoerror").style.display = "block";
      } else {
          document.getElementById("prolinetwoerror").style.display = "none";

      }
      this.setState({ addressLineTwo: e.target.value });
  }

  countryChange(e) {
    if (e.target.value === "0") {
        document.getElementById("procountryerror").style.display = "block";
    } else {
        document.getElementById("procountryerror").style.display = "none";

    }
    this.setState({ country: e.target.value });

    if (e.target.value !== '' && e.target.value !== null) {
      let countryCodeNo = this.state.countryData.find(item => item.name == e.target.value).code;
      this.setState({ countryCode: countryCodeNo, contact: '' });
    } else if (e.target.value === '' || e.target.value == 0) {
        this.setState({ countryCode: '' });
    }
  }

  stateChange(e) {
    if (e.target.value === "") {
        document.getElementById("prostateerror").style.display = "block";
    } else {
        document.getElementById("prostateerror").style.display = "none";
    }
    this.setState({ state: e.target.value });
    
  }

  pinChange(e) {
    if (e.target.value === "") {
        document.getElementById("propinerror").style.display = "block";
    } else {
        document.getElementById("propinerror").style.display = "none";
    }
    if (isNaN(e.target.value)) {
        document.getElementById("propinnumbererror").style.display = "block";
    } else {
        document.getElementById("propinnumbererror").style.display = "none";
    }
      this.setState({ pinCode: e.target.value });
  }
    passChange(e) {
      if (e.target.value === "") {
          document.getElementById("propassworderror").style.display = "block";
      } else {
          document.getElementById("propassworderror").style.display = "none";
      }
      this.setState({ password: e.target.value });
    }

  edit() {
    this.setState({
      editText: false
    });
  }

  handlevalidation() {
    let valid = true;
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    let tempemail = this.state.email;
    if (this.state.firstName === "") {
      valid = false;
      document.getElementById("profirstnameerror").style.display = "block";
      return valid;
    } else if (!this.state.firstName.match(/^[a-zA-Z ]*$/)) {
      valid = false;
      document.getElementById("profirstnamealphaerror").style.display = "block";      
      return valid;
    } else if (this.state.lastName === "") {
      valid = false;
      document.getElementById("prolastnameerror").style.display = "block";      
      return valid;
    } else if (!this.state.lastName.match(/^[a-zA-Z ]*$/)) {
      valid = false;
      document.getElementById("prolastnamealphaerror").style.display = "block";      
      return valid;
    } else if (this.state.email === "") {
      valid = false;
      document.getElementById("proregmailerror").style.display = "block";      
      return valid;
    } else if (!pattern.test(tempemail)) {
      valid = false;
      document.getElementById("proregmailvaliderror").style.display = "block";
      return valid;
    } else if (this.state.contact === "") {
      valid = false;
      document.getElementById("procontacterror").style.display = "block";    
      return valid;
    } else if (isNaN(this.state.contact)) {
      valid = false;
      document.getElementById("procontactnumbererror").style.display = "block";      
      return valid;
    } else if (this.state.gender === "0") {
      valid = false;
      document.getElementById("progendererror").style.display = "block";
      return valid;
    } if (this.state.dob === "") {
      valid = false;
      document.getElementById("prodoberror").style.display = "block";     
      return valid;
    } else if (this.state.addressLineOne === "") {
      valid = false;
      document.getElementById("prolineoneerror").style.display = "block";      
      return valid;
    } else if (this.state.addressLineTwo === "") {
      valid = false;
      document.getElementById("prolinetwoerror").style.display = "block";  
      return valid;
    } else if (this.state.country === "") {
      valid = false;
      document.getElementById("procountryerror").style.display = "block";
      return valid;
    } else if (this.state.state === "") {
      valid = false;
      document.getElementById("prostateerror").style.display = "block";     
      return valid;
    } else if (this.state.pinCode === "") {
      valid = false;
      document.getElementById("propinerror").style.display = "block";
      return valid;
    } else if (isNaN(this.state.pinCode)) {
      valid = false;
      document.getElementById("propinnumbererror").style.display = "block";  
      return valid;
    } else if (this.state.marriageStatus === "0") {
      valid = false;
      document.getElementById("promarriageerror").style.display = "block";    
      return valid;
    } if (this.state.password === "") {
      valid = false;
      document.getElementById("propassworderror").style.display = "block";      
      return valid;
    }

    return valid;
  }


  updateProfile() {
    let profileData = {};
    if (this.handlevalidation()) {
      profileData.profileId = this.state.profileId,
        profileData.email = this.state.email,
        profileData.contact = this.state.countryCode + this.state.contact,
        profileData.password = this.state.password,
        profileData.firstName = this.state.firstName,
        profileData.lastName = this.state.lastName,
        profileData.dob = this.state.dob,
        profileData.gender = this.state.gender,
        profileData.Address_line_one = this.state.addressLineOne,
        profileData.Address_line_two = this.state.addressLineTwo,
        profileData.State = this.state.state,
        profileData.Country = this.state.country,
        profileData.Pincode = this.state.pinCode,
        profileData.marriageStatus = this.state.marriageStatus
      this.props.updateProfile(profileData);
      this.setState({
        editText: true
      });
    }

  }

  test(){
    let countryCodeNo = this.state.countryData.find(item => item.name == this.props.userProfile[0].Country).code;
    this.setState({
      countryCode: countryCodeNo,
      contact: this.props.userProfile[0].Contact.substring(countryCodeNo.length),
    });
  }

  render() {

    let countries = this.state.countryData;
      let countriesItem = countries.map((country) =>
          <option key={country.name}>{country.name}</option>
      );

    let setProfile = "";
    if (this.state.editText) {
      setProfile = <div >
        {/* <button id="updateprofilebtn">Update Profile</button> */}

        <button id="editprofilebtn" onClick={this.edit}>Edit Profile</button>
      </div>
      //  <div className="centereditbtn">
      //   <Button className="editbtn bg-danger" onClick={this.edit} >Edit</Button>
      // </div>
    } else {
      setProfile = <div >
        <button id="updateprofilebtn" onClick={this.updateProfile}>Update Profile</button>
      </div>
      // <div className="centereditbtn">
      //   <Button className="editbtn bg-success" onClick={this.updateProfile} >Update</Button>
      // </div>
    }

    let viewCountryCode="";

    if(this.state.countryCode===""){
      if(this.state.countryData.length>0){
        let countryCodeNo = this.state.countryData.find(item => item.name == this.props.userProfile[0].Country).code;
       this.test();
        viewCountryCode = <div>{countryCodeNo}</div>
      }
    }else{
      viewCountryCode = <div>{this.state.countryCode}</div>
    }

    return (
      <div id="studycountmain">
        <Modal
              visible={this.state.visible}
              width="250"
              height="200"
              effect="fadeInDown"
              // onClickAway={() => this.closeModal()}
            >
              <div className="text-center" style={{ paddingTop: '20%' }}>
                <h4>Alert!</h4>
                <p>{this.state.notification}</p>
                <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
              </div>
            </Modal>
            {/* <div id="customdialog">
              <div id="customdialogcontent"><img src={profilecompleteimg} height="100%" width="100%" />
              
              </div>
            </div> */}
        <div id="headerstudy">
          <h3>Profile</h3>
          <ol id="breadcrumb">
            <li>Home</li> &nbsp; / &nbsp;
            <li style={{ opacity: '0.8' }}>Profile</li>
          </ol>
        </div>
        <div id="profileform">
        
          <hr />
          <div id="namediv">
            <div id="firstname">
              <label >First Name</label>
              <input type="text" style={{ width: '100%' }} className="input" value={this.state.firstName} placeholder="First Name"  onChange={this.firstNameChange} disabled={this.state.editText}/>
              <div id="profirstnameerror">
                                <div class="proquote">
                                    <problockquote>
                                        Required
                                            </problockquote>
                                </div>
                            </div>
                            <div id="profirstnamealphaerror">
                                <div id="profirstnameaplhaquote">
                                    <blockquote>
                                        Only Alphabets
                                            </blockquote>
                                </div>
                            </div>
            </div>
            <div id="lastname">
              <label >Last Name</label>
              <input type="text" style={{ width: '100%' }} className="input"  value={this.state.lastName} placeholder="Last Name" onChange={this.lastNameChange} disabled={this.state.editText}/>
              <div id="prolastnameerror">
                  <div class="proquote">
                      <problockquote>
                          Required
                              </problockquote>
                  </div>
              </div>
              <div id="prolastnamealphaerror">
                  <div id="prolastnameaplhaquote">
                      <blockquote>
                          Only Alphabets
                              </blockquote>
                  </div>
              </div>
            </div>
          </div>
          <div id="emailnumber">
            <div id="email">
              <label >Email Address</label>
              <input type="text" style={{ width: '100%' }} className="input" name="email"  value={this.state.email} placeholder="Email"  onChange={this.regmailChange} disabled={this.state.editText}/>
              <div id="proregmailerror">
                  <div class="proquote">
                      <blockquote>
                        Required
                      </blockquote>
                  </div>
              </div>
              <div id="proregmailvaliderror">
                  <div id="proregmailvalidquote">
                      <blockquote>
                        Incorrect Mail
                      </blockquote>
                  </div>
              </div>
            </div>
            <div id="number">
              <label >Mobile Number</label>
              <input type="text" style={{ width: '100%' }} id="numberinput" placeholder="Mobile Number"  value={this.state.contact} onChange={this.contactChange} disabled={this.state.editText}/>
              <div id="procntrycode">{viewCountryCode}</div>
              <div id="procontacterror">
                  <div class="proquote">
                        <blockquote>
                            Required
                                </blockquote>
                    </div>
                </div>
                <div id="procontactnumbererror">
                    <div id="procontactquote">
                        <blockquote>
                            Only Number
                                        </blockquote>
                    </div>
                </div>
               
            </div>
          </div>
          <div id="agegendermarriage">
            <div id="age">
              <label >Date of Birth</label>
              <input type="date" style={{ width: '100%' }} className="input" id="prodateofbirth" placeholder="date"  value={this.state.dob} onChange={this.dobChange} disabled={this.state.editText}/>
              <div id="prodoberror">
                                <div class="proquote">
                                    <blockquote>
                                        Required
                                        </blockquote>
                                </div>
                            </div>
            </div>
            <div id="gender">
              <label >Gender</label>
              <select style={{ width: '100%' }} className="input" placeholder="gender" onChange={this.genderChange}  value={this.state.gender} disabled={this.state.editText}>
                <option value="0">Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Shemale">Shemale</option>
                <option value="Transgender">Transgender</option>
                <option value="Others">Others</option>
              </select>
              <div id="progendererror">
                                <div class="proquote">
                                    <blockquote>
                                        Required
                                        </blockquote>
                                </div>
                            </div>
            </div>
            <div id="marriage">
              <label >Marriage Status</label>
              <select type="select" style={{ width: '100%' }} className="input" placeholder="marriage"  value={this.state.marriageStatus} onChange={this.marriageChange} disabled={this.state.editText}>
                <option value="0">Marriage Status</option>
                <option value="Married">Married</option>
                <option value="UnMarried">UnMarried</option>
                <option value="Lesbian">Lesbian</option>
                <option value="Gay">Gay</option>
                <option value="Divorced">Divorced</option>
                <option value="Register Partner">Register Partner</option>
                <option value="Other">Other</option>
              </select>
              <div id="promarriageerror">
                                <div class="proquote">
                                    <blockquote>
                                        Required
                                                </blockquote>
                                </div>
                            </div>
            </div>
          </div>

          <div id="addone">
            <div id="addressone">
              <label >Address Line One</label>
              <textarea type="text-area" style={{ width: '100%' }} className="input" placeholder="Address Line One"  value={this.state.addressLineOne} onChange={this.addoneChange} disabled={this.state.editText}/>
              <div id="prolineoneerror">
                                <div class="proquote">
                                    <blockquote>
                                        Required
                                                </blockquote>
                                </div>
                            </div>
            </div>
          </div>

          <div id="addtwo">
            <div id="addresstwo">
              <label >Address Line Two</label>
              <textarea type="text-area" style={{ width: '100%' }} className="input" placeholder="Address Line Two"  value={this.state.addressLineTwo} onChange={this.addtwoChange} disabled={this.state.editText}/>
              <div id="prolinetwoerror">
                                <div class="proquote">
                                    <blockquote>
                                        Required
                                                    </blockquote>
                                </div>
                            </div>
            </div>
          </div>

          <div id="countrystatepin">
            <div id="country">
              <label >Country</label>
              <select style={{ width: '100%' }} className="input" placeholder="gender"  value={this.state.country} onChange={this.countryChange} disabled={this.state.editText}>
              <option Value="" >Select Country</option>
                {countriesItem}
              </select>
              <div id="procountryerror">
                                <div class="proquote">
                                    <blockquote>
                                        Required
                                                    </blockquote>
                                </div>
                            </div>
            </div>
            <div id="state">
              <label >State</label>
              <input type="text" style={{ width: '100%' }} className="input" placeholder="State"  value={this.state.state} onChange={this.stateChange} disabled={this.state.editText}/>
              <div id="prostateerror">
                                <div class="proquote">
                                    <blockquote>
                                        Required
                                          </blockquote>
                                </div>
                            </div>
            </div>
            <div id="pincode">
              <label >Pin Code</label>
              <input type="text" style={{ width: '100%' }} className="input" placeholder="Pin Code"  value={this.state.pinCode} onChange={this.pinChange} disabled={this.state.editText}/>
              <div id="propinerror">
                                <div class="proquote">
                                    <blockquote>
                                        Required
                                                    </blockquote>
                                </div>
                            </div>
                            <div id="propinnumbererror">
                                <div id="propinquote">
                                    <blockquote>
                                        Only Number
                                                    </blockquote>
                                </div>
                            </div>
            </div>
          </div>

          <div id="secure">
            <div id="pass">
              <label>Password</label>
              <input type="password" style={{ width: '100%' }} className="input" placeholder="Password"  value={this.state.password} onChange={this.passChange} disabled={this.state.editText}/>
              <div id="propassworderror">
                                <div class="proquote">
                                    <blockquote>
                                        Required
                                                    </blockquote>
                                </div>
                            </div>
            </div>
          </div>
          <div id="profilebtn">
            {setProfile}
          </div>
        </div>
      </div>






      // <div className="main" style={{marginTop: "40px"}}>
      //   <Modal
      //     visible={this.state.visible}
      //     width="400"
      //     height="300"
      //     effect="fadeInDown"
      //     onClickAway={() => this.closeModal()}
      //   >
      //     <div className="text-center" style={{ paddingTop: '20%' }}>
      //       <h1>Alert!</h1>
      //       <p>{this.state.notification}</p>
      //       <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
      //     </div>
      //   </Modal>
      //   <div className="app flex-row align-items-center">
      //     <Container>
      //       <Row className="justify-content-center">
      //         <Col>
      //           <Card style={{ marginTop: '2%' }}>
      //             <CardHeader><h3 className="text-center">Profile</h3></CardHeader>
      //             <CardBody>
      //               <Form>
      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={user} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="text" placeholder=" First name" name="firstName" value={this.state.firstName} onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>
      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={user} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="text" placeholder=" Last name" name="lastName" value={this.state.lastName} onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>

      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={email} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="text" placeholder=" Email" name="email" value={this.state.email} onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>

      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={cont} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="text" placeholder=" Contact Number with country code" name="contact" value={this.state.contact} onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>
      //                 <Label for="exampleSelect">Date of birth</Label>
      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={dateicon} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="date" name="dob" value={this.state.dob} placeholder="date" onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>

      //                 <FormGroup>
      //                   <Label for="exampleSelect">Gender

      //               </Label>
      //                   <Input type="select" name="gender" value={this.state.gender} onChange={this.onChange} disabled={this.state.editText}>
      //                     <option>Select</option>
      //                     <option>Male</option>
      //                     <option>Female</option>
      //                     <option>Transgender</option>
      //                     <option>Shemale</option>
      //                     <option>Others</option>
      //                   </Input>
      //                 </FormGroup>
      //                 <Label for="exampleSelect">Address</Label>
      //                 <InputGroup className="mb-3">

      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={add} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="text" placeholder="Address Line -1" name="addressLineOne" value={this.state.addressLineOne} onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>

      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={add} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="text" placeholder="Address Line -2" name="addressLineTwo" value={this.state.addressLineTwo} onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>

      //                 <FormGroup>
      //                   <Label for="exampleSelect">Country

      //               </Label>
      //                   <Input type="select" name="country" value={this.state.country} onChange={this.onChange} disabled={this.state.editText}>
      //                     <option>Select</option>
      //                     <option Value="" >-- Please Select --</option>
      //                     <option Value="Afghanistan" >Afghanistan</option>
      //                     <option Value="Albania" >Albania</option>
      //                     <option Value="Algeria" >Algeria</option>
      //                     <option Value="American Samoa" >American Samoa</option>
      //                     <option Value="Andorra" >Andorra</option>
      //                     <option Value="Angola" >Angola</option>
      //                     <option Value="Anguilla" >Anguilla</option>
      //                     <option Value="Antarctica" >Antarctica</option>
      //                     <option Value="Antigua and Barbuda" >Antigua and Barbuda</option>
      //                     <option Value="Argentina" >Argentina</option>
      //                     <option Value="Armenia" >Armenia</option>
      //                     <option Value="Australia" >Australia </option>
      //                     <option Value="Austria" >Austria </option>
      //                     <option Value="Azerbaijan" >Azerbaijan </option>
      //                     <option Value="Bahrain" >Bahrain </option>
      //                     <option Value="Belarus" >Belarus </option>
      //                     <option Value="Belgium" >Belgium </option>
      //                     <option Value="Bermuda" >Bermuda </option>
      //                     <option Value="Bolivia" >Bolivia </option>
      //                     <option Value="Brazil" >Brazil </option>
      //                     <option Value="Brunei Darussalam" >Brunei Darussalam  </option>
      //                     <option Value="Bulgaria" >Bulgaria</option>
      //                     <option Value="Canada" >Canada</option>
      //                     <option Value="Chile" >Chile</option>
      //                     <option Value="China" >China</option>
      //                     <option Value="Columbia" >Columbia </option>
      //                     <option Value="Costa Rica" >Costa Rica</option>
      //                     <option Value="Croatia" >Croatia</option>
      //                     <option Value="Czech Republic" >Czech Republic</option>
      //                     <option Value="Denmark" >Denmark </option>
      //                     <option Value="Dominican Republic" >Dominican Republic </option>
      //                     <option Value="Ecuador" >Ecuador </option>
      //                     <option Value="Egypt" >Egypt </option>
      //                     <option Value="Estonia" >Estonia </option>
      //                     <option Value="Finland" >Finland </option>
      //                     <option Value="France" >France </option>
      //                     <option Value="Germany" >Germany </option>
      //                     <option Value="Greece" >Greece  </option>
      //                     <option Value="Guatemala" >Guatemala </option>
      //                     <option Value="Hong Kong" >Hong Kong </option>
      //                     <option Value="Hungary" >Hungary </option>
      //                     <option Value="Iceland" >Iceland </option>
      //                     <option Value="India" >India  </option>
      //                     <option Value="Indonesia" >Indonesia </option>
      //                     <option Value="Ireland" >Ireland </option>
      //                     <option Value="Israel" >Israel </option>
      //                     <option Value="Italy" >Italy  </option>
      //                     <option Value="Jamaica" >Jamaica </option>
      //                     <option Value="Japan" >Japan  </option>
      //                     <option Value="Jordan" >Jordan </option>
      //                     <option Value="Kenya" >Kenya  </option>
      //                     <option Value="Korea" >Korea  </option>
      //                     <option Value="Kuwait" >Kuwait </option>
      //                     <option Value="Latvia" >Latvia </option>
      //                     <option Value="Liechtenstein" >Liechtenstein  </option>
      //                     <option Value="Lithuania" >Lithuania  </option>
      //                     <option Value="Luxembourg" >Luxembourg </option>
      //                     <option Value="Macau" >Macau </option>
      //                     <option Value="Malaysia" >Malaysia </option>
      //                     <option Value="Mexico" >Mexico </option>
      //                     <option Value="Monaco" >Monaco </option>
      //                     <option Value="Morocco" >Morocco </option>
      //                     <option Value="Netherlands" >Netherlands </option>
      //                     <option Value="New Zealand" >New Zealand </option>
      //                     <option Value="Nicaragua" >Nicaragua </option>
      //                     <option Value="Norway" >Norway </option>
      //                     <option Value="Oman" >Oman </option>
      //                     <option Value="Pakistan" >Pakistan </option>
      //                     <option Value="Panama" >Panama </option>
      //                     <option Value="Paraguay" >Paraguay </option>
      //                     <option Value="Peru" >Peru </option>
      //                     <option Value="Philippines" >Philippines </option>
      //                     <option Value="Poland" >Poland </option>
      //                     <option Value="Portugal" >Portugal </option>
      //                     <option Value="Romania" >Romania </option>
      //                     <option Value="Russia" >Russia </option>
      //                     <option Value="Saudi Arabia" >Saudi Arabia </option>
      //                     <option Value="Singapore" >Singapore </option>
      //                     <option Value="Slovak Republic" >Slovak Republic </option>
      //                     <option Value="Slovenia" >Slovenia </option>
      //                     <option Value="South Africa" >South Africa </option>
      //                     <option Value="Spain" >Spain </option>
      //                     <option Value="Sweden" >Sweden </option>
      //                     <option Value="Switzerland" >Switzerland </option>
      //                     <option Value="Taiwan" >Taiwan </option>
      //                     <option Value="Thailand" >Thailand </option>
      //                     <option Value="Turkey" >Turkey </option>
      //                     <option Value="Uganda" >Uganda </option>
      //                     <option Value="Ukraine" >Ukraine </option>
      //                     <option Value="United Arab Emirates" >United Arab Emirates </option>
      //                     <option Value="United Kingdom" >United Kingdom </option>
      //                     <option Value="Uruguay" >Uruguay </option>
      //                     <option Value="Venezuela" >Venezuela </option>
      //                     <option Value="Vietnam" >Vietnam </option>
      //                   </Input>
      //                 </FormGroup>

      //                 <Label id="ms">State/City</Label>
      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={add} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="text" placeholder="State" name="state" value={this.state.state} onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>


      //                 <Label id="ms">Pin Code</Label>
      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={add} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="text" placeholder="pincode" name="pinCode" value={this.state.pinCode} onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>



      //                 <FormGroup>
      //                   <Label id="ms">Marrital Status</Label>
      //                   <Input type="select" name="marriageStatus" value={this.state.marriageStatus} onChange={this.onChange} disabled={this.state.editText} >
      //                     <option>Select</option>
      //                     <option>Married</option>
      //                     <option>UnMarried</option>
      //                     <option>Lesbian</option>
      //                     <option>Gay</option>
      //                     <option>Divorced</option>
      //                     <option>Register Partener</option>
      //                     <option>Others</option>
      //                   </Input>
      //                 </FormGroup>


      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={lock} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="password" placeholder="Password" autoComplete="new-password" name="password" value={this.state.password} onChange={this.onChange} disabled={this.state.editText} />
      //                 </InputGroup>

      //                 {setProfile}
      //               </Form>



      //             </CardBody>
      //             <CardFooter className="p-4">
      //             </CardFooter>
      //           </Card>
      //         </Col>
      //       </Row>
      //     </Container>
      //   </div >

      // </div>


    );
  }
}
export default ProfileView;
