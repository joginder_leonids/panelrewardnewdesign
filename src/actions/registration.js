import {RegistrationApi, varifyTokenApi}  from "../api/registrationApi";
import {startSpinner, endSpinner} from "../loadingSpinner/Action";

export const setRegistrationSuccess=(response) =>{
  return {
    type: "SHOW_REGISTER_USER",
    response
  }
}

export const setRegistrationFail=(response) =>{
  return {
    type: "SHOW_CREATE_USER_FAIL_EXCEPTION",
    response
  }
}

export const setRegistrationMessage=() =>{
  return {
    type: "SET_REGISTRATION_MESSAGE"
  }
}

export const varifyTokenResponse=(response) =>{
  return {
    type: "SHOW_TOKEN_VARIFY",
    response
  }
}

export const varifyTokenResponseFail=(responseFail) =>{
  return {
    type: "SHOW_TOKEN_VARIFY_FAIL",
    responseFail
  }
}


export const tokenMsgFail=() =>{
  return {
    type: "SHOW_TOKEN_MSG_FAIL",
  }
}

export const EmptyRegisterProps=() =>{  // new change
  return {
    type: "SET_REGISTER_NULL",
  }
}



export const registerUser = (userData)=>{
  return dispatch =>{
    dispatch(startSpinner());
    dispatch(EmptyRegisterProps());   // new change
     RegistrationApi(userData).then((response) =>{
       if(response.success){
         dispatch(setRegistrationSuccess(response));
         dispatch(endSpinner());
         }else{
          dispatch(setRegistrationFail(response));
          dispatch(endSpinner());
        }
    });
  }
}

export const varifyToken = (userData)=>{
  return dispatch =>{
    dispatch(startSpinner());
     varifyTokenApi(userData).then((response) =>{
       if(response.success){
          dispatch(varifyTokenResponse(response));
          dispatch(endSpinner());
         }else{
          dispatch(varifyTokenResponseFail(response));
          dispatch(endSpinner());
        }
    });
  }
}
