import { transactionApi, fetchUserTractionApi, getGiftcouponByUserApi} from "../api/rewardApi";
import {studyCountApi} from "../api/studyApi"
import {startSpinner, endSpinner} from "../loadingSpinner/Action";

export const setTransactionResponse=(response) =>{
  return {
    type: "SET_TRANSACTION_REQUEST_RESPONSE",
    response
  }
}

export const setStudyCountDataReward =(countResponse) => {
  return {
    type : "SET_STUDY_REWARD_COUNT_DATA",
    countResponse
  }
}

export const setUserTransaction = (transactionData) =>{
  return {
    type : "SET_USER_TRANSACTION_DATA",    
    transactionData
  }
}

export const setCouponDetails = (couponData) =>{
  return {
    type : "SET_GIFTCOUPON_ONLINE_DATA",    
    couponData
  }
}

export const transaction = (data) => {
    return dispatch => {
      dispatch(startSpinner());
        transactionApi(data).then((response) => {
          dispatch(setTransactionResponse(response));
          dispatch(endSpinner());
      });
    }
  }

  

  export const studyCountData=(user) =>{
    return dispatch =>{
      dispatch(startSpinner());
      studyCountApi(user).then((result) =>{
        dispatch(setStudyCountDataReward(result));
        dispatch(endSpinner());
      });
    }
  }

  export const fetchUserTraction=(user) =>{
    return dispatch =>{
      dispatch(startSpinner());
      fetchUserTractionApi(user).then((result) =>{
        dispatch(setUserTransaction(result));
        dispatch(endSpinner());
      });
    }
  }

  export const getGiftcouponByUser = (assignId) =>{
    return dispatch =>{
      dispatch(startSpinner());
      getGiftcouponByUserApi(assignId).then((result) =>{
        dispatch(setCouponDetails(result));
        dispatch(endSpinner());
      });
    }
  }