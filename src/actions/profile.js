import { fetchProfileApi , updateProfileApi, addProfileImageApi} from '../api/profileApi';
import {startSpinner, endSpinner} from "../loadingSpinner/Action";
import Modal from 'react-awesome-modal';

export const setProfile =(profileData) => {
  return {
    type : "SET_PROFILE",
    profileData
  }
}

export const setprofileUpdateResponse =(updateResponse) =>{
  return {
    type : "SET_PROFILE_UPLOAD_IMAGE_RESPONSE",
    updateResponse
  }
}
//setProfileResNull
export const setProfileResNull =() =>{
  return {
    type : "SET_PROFILE_RESPONSE_NULL",
    
  }
}

export const fetchProfile=(userId , access_token) =>{
  return dispatch =>{
      dispatch(startSpinner());
    fetchProfileApi(userId, access_token).then((result) =>{ 
      dispatch(setProfile(result));
      dispatch(endSpinner());
    });
  }
}


export const updateProfile=(userId , access_token, data) =>{
  return dispatch =>{
      dispatch(startSpinner());
    updateProfileApi(userId, access_token, data).then((result) =>{
       if(result.success){
         //alert(result.message);
         dispatch(setProfile(result));
         dispatch(endSpinner());
       }

    });
  }
}


export const addProfileImage=(data, profileID)=>{
  return dispatch =>{
    dispatch(startSpinner());
    addProfileImageApi(data, profileID).then((result) =>{
        dispatch(setprofileUpdateResponse(result));
        let access_token=sessionStorage.getItem("token");
        let userId=sessionStorage.getItem("user_id");
        dispatch(fetchProfile(userId, access_token));
        dispatch(endSpinner());
    });
  }
}