import { fetchCategoryApi, fetchEduQuestionsApi, saveQuestionAnsApi } from '../api/profileDashboardApi';
import {startSpinner, endSpinner} from "../loadingSpinner/Action";

export const setProfileCategoryDashboard =(categoryData) => {
  return {
    type : "SET_CATEGORY",
    categoryData
  }
}

export const setEduQuestions =(educationData) => {
  return {
    type : "SET_EDUCATION",
    educationData
  }
}

export const emptySetStateQuestions = () =>{
  return{
    type : "SET_EMPTY_QUESTIONS"
  }
}

export const fetchCategory=(userId) =>{
  return dispatch =>{
      dispatch(startSpinner());
    fetchCategoryApi(userId).then((result) =>{
     dispatch(setProfileCategoryDashboard(result));
     dispatch(endSpinner());
    });
  }
}


 export const fetchEduQuestions=(userId, catID) =>{
   return dispatch =>{
       dispatch(startSpinner());
     fetchEduQuestionsApi(userId, catID).then((result) =>{
      dispatch(setEduQuestions(result));
      dispatch(endSpinner());
     });
   }
 }

export const saveQuestionAns=(userId, catID, quesData) =>{
  return dispatch =>{
      dispatch(startSpinner());
      dispatch(emptySetStateQuestions());
    saveQuestionAnsApi(userId, catID, quesData).then((result) =>{
       if(result){
        dispatch(setEduQuestions(result));
        dispatch(endSpinner());
       }
       
     //dispatch();
    });
  }
}
