import { fetchStudyApi , saveStudyParticipateApi, studyParticipateStatusApi, fetchStudyCountDataApi, studyCountApi} from '../api/studyApi';
import {startSpinner, endSpinner} from "../loadingSpinner/Action";


export const setStudy =(studyData) => {
  return {
    type : "SET_STUDY",
    studyData
  }
}

//setStudyCountData
export const setStudyTotalCountData =(studyCountData) => {
  return {
    type : "SET_Total_STUDY_COUNT_DATA",
    studyCountData
  }
}

export const setStatusParticipateStudySuccess =(response) => {
  return {
    type : "SET_STUDY_PARTICIPATE_STATUS",
    response
  }
}

export const setStatusParticipateStudyFail =(response) => {
  return {
    type : "FAIL_STUDY_PARTICIPATE_STATUS",
    response
  }
}

export const setStudyCountData =(countResponse) => {
  return {
    type : "SET_STUDY_COUNT_DATA",
    countResponse
  }
}

export const setRewardNull =() =>{
  return{
    type : "SET_REWARD_NULL"
  }
}

export const setGetStudiesAgain=() =>{
  return{
    type : "SET_GET_STUDIES_AGAIN"
  }
}

export const fetchStudy=(userId , access_token, pageNo) =>{
  return dispatch =>{
    dispatch(startSpinner());
    fetchStudyApi(userId, access_token, pageNo).then((result) =>{
     dispatch(setStudy(result));
     dispatch(endSpinner());
    });
  }
}

//fetchStudyCountData
export const fetchStudyCountData=(userId ) =>{
  return dispatch =>{
    dispatch(startSpinner());
    fetchStudyCountDataApi(userId).then((result) =>{
     dispatch(setStudyTotalCountData(result));
     dispatch(endSpinner());
    });
  }
}

export const saveStudyParticipate=(data, userId) =>{
  return dispatch =>{
    dispatch(startSpinner());
    saveStudyParticipateApi(data, userId).then((result) =>{
      dispatch(endSpinner());
       dispatch(setGetStudiesAgain(result));
    });
  }
}

export const studyParticipate=(data) =>{
  return dispatch =>{
    dispatch(startSpinner());
    studyParticipateStatusApi(data).then((result) =>{
      if(result.success){
        dispatch(setStatusParticipateStudySuccess(result));
        dispatch(endSpinner());
      }else{
        dispatch(setStatusParticipateStudyFail(result));
        dispatch(endSpinner());
      }
    });
  }
}



export const studyCountData=(user) =>{
  return dispatch =>{
    dispatch(startSpinner());
    studyCountApi(user).then((result) =>{
      dispatch(setRewardNull());
      dispatch(setStudyCountData(result));
      dispatch(endSpinner());
    });
  }
}