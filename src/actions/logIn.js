import {submitLoginApi, resetPasswordApi, forgetPasswordMailApi} from "../api/loginApi";
import {startSpinner, endSpinner} from "../loadingSpinner/Action";

export const setLogOut = () => {
  return{
    type : "LOGOUT"
  }
}

export const setLogin =(authClient) => {
  return {
    type : "SET_AUTH_CLIENT",
    authClient
  }
}
export const setLoginFail =(authFailResponse) => {
  return {
    type : "SET_LOGIN_FAIL",
    authFailResponse
  }
}

//setLoginFailExistUser
export const setLoginFailExistUser =(authFailResponse) => {
  return {
    type : "SET_LOGIN_FAIL_USER_EXIST",
    authFailResponse
  }
}

export const setMessageFail =() => {
  return {
    type : "SET_MESSAGE_FAIL",
  }
}

export const setResetPassResponse =(response) => {
  return {
    type : "SET_RESET_PASS_MESSAGE",
    response
  }
}

export const resetPasswordMsgFail =(response) => {
  return {
    type : "SET_RESET_PASS_MESSAGE_FAIL",
  }
}

export const setForgetPassMailResponse =(responseMail) => {
  return {
    type : "SET_FORGET_PASS_MAIL",
    responseMail
  }
}

export const setForgetPassFailMailResponse =(responseMail) => {
  return {
    type : "SET_FORGET_PASS_FAIL_MAIL",
    responseMail
  }
}

export const setProfileData =() => {
  return {
    type : "SET_PROFILE_LOGOUT",
    
  }
}

export const removeForgetPassProps=()=>{
  return {
    type: "REMOVE_FORGETPASS_PROPS"
  }
}


export const logIn = (loginData) => {
  return dispatch => {
    dispatch(startSpinner());
    submitLoginApi(loginData).then((response) => {
      
      if(response.success){
        dispatch(setLogin(response));
        dispatch(endSpinner());
      }
      else if(response.userExist === true){
        dispatch(setLoginFailExistUser(response));
        dispatch(endSpinner());
      }else{
        dispatch(setLoginFail(response));
        dispatch(endSpinner());
      }
      
    });
  }
}

export const submitLogOut = () => {
  return dispatch => {
    dispatch(startSpinner());
    submitLoginApi().then((response) => {
    //  if(response.success){
      dispatch(setLogOut(response));
      dispatch(endSpinner());
      //}
    });
  }
}


export const resetPassword = (Data) => {
  return dispatch =>{
    dispatch(startSpinner());
    resetPasswordApi(Data).then((response) => {
      dispatch(setResetPassResponse(response));
      dispatch(endSpinner());
    });
  }
}

export const forgetPasswordMail = (Data) => {
  return dispatch =>{
    dispatch(startSpinner());
    forgetPasswordMailApi(Data).then((responseMail) => {
      if(responseMail.success){
        dispatch(setForgetPassMailResponse(responseMail));
        dispatch(endSpinner());
      }else{
        dispatch(setForgetPassFailMailResponse(responseMail));
        dispatch(endSpinner());
      }
    });
  }
}
