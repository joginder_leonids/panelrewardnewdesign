//containers -> Loout -> index.js
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {submitLogOut, setProfileData} from '../../actions/logIn';


class Logout extends Component {

  componentWillMount(){
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user_id');
    sessionStorage.removeItem('user_name');
    sessionStorage.removeItem('userImagePath');
    this.props.dispatch(setProfileData());
    this.props.history.push("/Login");
    this.props.dispatch(submitLogOut());
  }

  render(){
    return(<div></div>)
  }
}

const mapStateToProps=(state) =>{
  return {
    authenticated : state.login.authenticated
  };
}

export default connect(mapStateToProps)(Logout);
