import React, { Component } from 'react';
import './leftcolumn.css';
import desktop from '../../icons/desktop.png';
import buffer from '../../icons/study.png';
import bank from '../../icons/bank.png';
import card from '../../icons/card.png';
import arrow from '../../icons/arrowangle.png';
import logout from '../../icons/logout.png';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import avtar from '../../images/avtar.jpg';
import { connect } from 'react-redux';
class LeftColumn extends Component{
    constructor(props) {
        super(props)
        this.state = {
         showSliderMenu: true,
        dashboardColor:''
        };
     
      }
    
        // componentDidMount(){
        //         let i;
        //         for(i=0;i<document.links.length;i++){
        //                 document.links[i].onclick=function(){
        //                         if(this.title=='dahsboardmenu'){
        //                          document.getElementById("dahsboardmenu").style.backgroundColor="blue";
        //                          document.getElementById("studymenu").style.backgroundColor="";
        //                          document.getElementById("transmenu").style.backgroundColor="";
        //                        // this.style.color='yellow';
        //                         //here window.open...
        //                         return false;
        //                         }else if(this.title=='studymenu'){
        //                                 document.getElementById("studymenu").style.backgroundColor="blue";
        //                                 document.getElementById("dahsboardmenu").style.backgroundColor="";
        //                                 document.getElementById("transmenu").style.backgroundColor="";
        //                         }
        //                        else if(this.title=='transmenu'){
        //                         document.getElementById("transmenu").style.backgroundColor="blue";
        //                         document.getElementById("dahsboardmenu").style.backgroundColor="";
        //                         document.getElementById("studymenu").style.backgroundColor="";
        //                        }
        //                         else{
        //                          document.getElementById("dahsboardmenu").style.backgroundColor="";
        //                          document.getElementById("studymenu").style.backgroundColor="";
        //                          document.getElementById("transmenu").style.backgroundColor="";

        //                         }
        //                         };
        //         }
        // }

      openNav() {
          this.setState({showSliderMenu:true});
        document.getElementById("leftcol").style.width = "230px";
        document.getElementById("openslider").style.display="none";
        document.getElementById("closeslider").style.display="block";
        document.getElementById("rightcol").style.marginLeft= "230px";
        document.getElementById("footerlogin").style.marginLeft= "230px";
        

        // document.getElementById("leftcolcontent").style.display="block";
        // // document.getElementById("iconspro").style.display="none";
        // document.getElementById("imagepro").style.display="block";
        // //
        // document.getElementById("menuitems").style.display="block";
       

      }
      closeNav(){
        document.getElementById("leftcol").style.width = "40px";
        document.getElementById("closeslider").style.display="none";
        document.getElementById("openslider").style.display="block";
        this.setState({showSliderMenu:false});
        document.getElementById("rightcol").style.marginLeft= "40px";
        document.getElementById("footerlogin").style.marginLeft= "40px";

      }
      

   
     
      
      render(){
        let showUserName;
           let getusername=sessionStorage.getItem('user_name');
           if(getusername !==null){
                showUserName=getusername.toUpperCase();
           }
          
         
        let sliderMenu ="";
        let userImageView = "";
        if(sessionStorage.getItem('userImagePath')){
                userImageView =<div id="userimage"><Link to="/imageupload"><img src={sessionStorage.getItem('userImagePath')} alt=""/></Link></div>
                //  <div id="profileimgleftcol"><img src={sessionStorage.getItem('userImagePath')} alt="" class="avatar" /></div>
        }else{
                userImageView =<div id="userimage"><Link to="/imageupload"><img src={avtar} alt=""/></Link></div>
        }

        //nav close
if(this.state.showSliderMenu){
            sliderMenu = <div>
                        <div style={{textAlign:'center'}}>
                                {userImageView}
                                 
                                <sapn style={{color:'#898989'}}>{showUserName}</sapn><br/>
                                {/* <span style={{color:'#6b3e9e'}}>GNN Research Group</span> */}
                                <hr/>
                        </div>  
                     
                        <Link className="mm"  to="/studyCount" >
                        <div className="commonmenus" id="dahsboardmenu" >
                                <div id="iconmenu">
                                
                                <img src={desktop} height="18px" width="18px" /><span> Dashboard </span>
                                </div>
                                <div id="rightarrow"><img src={arrow} height="10px" width="10px" /></div>
                        </div>
                        </Link>
                
                        <Link className="mm"  to="/Study">
                        <div className="commonmenus" id="studymenu">
                                <div id="iconmenu">
                                <img src={buffer} height="18px" width="18px" /><span> Study </span>
                                </div>
                                <div id="rightarrow"><img src={arrow} height="10px" width="10px" /></div>
                        </div>
                        </Link>
                        <Link className="mm" to="/transactionHistory" >
                        <div className="commonmenus" id="transmenu">
                                <div id="iconmenu">
                                <img src={bank} height="18px" width="18px" /><span> Transaction History </span>
                                </div>
                                <div id="rightarrow"><img src={arrow} height="10px" width="10px" /></div>
                        </div>
                        </Link>
                         <Link className="mm" to="/profileDashboard" >
                        <div className="commonmenus">
                                <div id="iconmenu">
                                <img src={card} height="18px" width="18px" /><span>Manage Profile </span>
                                </div>
                                <div id="rightarrow"><img src={arrow} height="10px" width="10px" /></div>
                        </div>
                        </Link>
                        <Link className="mm" to="/logout" >
                        <div className="commonmenus">
                                <div id="iconmenu">
                                <img src={logout} height="18px" width="18px" /><span>Logout </span>
                                </div>
                                <div id="rightarrow"><img src={arrow} height="10px" width="10px" /></div>
                        </div>
                        </Link>
                       

{/*                         
                    <Link  className = "mm" to="/studyCount" > <div  className="leftmenuscss" >
                    <img src={desktop} height="18px" width="18px" /><span> Dashboard </span>
                    </div></Link>
                   

                    <Link  className = "mm" to="/Study" >
                    <div  className="leftmenuscss">
                    <img src={buffer} height="18px" width="18px" /><span>Study</span>
                    </div></Link>
                   

                     <Link  className = "mm" to="/transactionHistory" >
                    <div  className="leftmenuscss">
                    <img src={bank} height="18px" width="18px" /><span>Transaction History</span>
                    </div></Link>
                   

                     <Link  className = "mm" to="/profileDashboard" >
                    <div  className="leftmenuscss">
                    <img src={card} height="18px" width="18px" /><span>Demo Graphic</span>
                    </div></Link>
                    
                     <Link  className = "mm" to="/logout" >
                    <div  className="leftmenuscss">
                    <img src={logout} height="18px" width="18px" /><span>Logout </span>
                    </div></Link> */}
                    
            </div>
           
        }else{
            sliderMenu = 
            <div style={{textAlign:'center'}} >
                <Link  className = "mm" to="/studyCount" >
                <div className="iconcss">
                <img src={desktop} height="18px" width="18px" />
                </div></Link>
                <Link  className = "mm" to="/Study" >
                <div  className="iconcss">
                <img src={buffer} height="18px" width="18px" />
                </div></Link>
                <Link  className = "mm" to="/transactionHistory" >
                <div className="iconcss">
                <img src={bank} height="18px" width="18px" />
                </div></Link>
                <Link  className = "mm" to="/profileDashboard" >
                <div className="iconcss">
                <img src={card} height="18px" width="18px" />
                </div></Link>
                <Link  className = "mm" to="/logout" >
                <div className="iconcss">
                <img src={logout} height="18px" width="18px" />
                </div></Link>
            </div>
        }


          return(
                <div >
                <div style={{textAlign:'center',display:'flex',textAlign:'center',justifyContent:'center'}}>
                    <span style={{fontSize:'30px', cursor:'pointer'}} id="closeslider" onClick={() => this.closeNav()}>&#x2039;</span>
                    <span style={{fontSize:'30px', cursor:'pointer'}} id="openslider"   onClick={() => this.openNav()}>&#9776;</span>
                 </div>
               
               
                    {sliderMenu}                  

                </div>            
          );
      }

}

// export default LeftColumn;

const mapStateToProps = (state) => {
        return {
                authenticated: state.login.authenticated,
                setUser: state.profile.setUser,
                userProfile : state.profile.userProfile
        }
}
      
export default connect(mapStateToProps)(LeftColumn);