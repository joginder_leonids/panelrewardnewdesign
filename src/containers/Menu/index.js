import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { setRegistrationMessage } from '../../actions/registration';
import { connect } from 'react-redux';
import {fetchProfile} from '../../actions/profile';
import "./style.css";
import  pic from './6.jpg';
import appConfig from '../../api/apiConfig';
import profileimg from '../../icons/profile.png';
import logoutimg from '../../icons/logout.png';


import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Badge,

  DropdownItem,
  DropdownToggle,
  DropdownMenu
} from 'reactstrap';
import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
class Menu extends Component {
  constructor(props) {
    super(props)
    this.state = {
     
    };

    this.setMessage = this.setMessage.bind(this);
  
  }

  setMessage() {
    this.props.dispatch(setRegistrationMessage());
  }


  componentWillReceiveProps(nextProps){
    if(nextProps.userProfile && nextProps.userProfile.length>0){
      let userData = nextProps.userProfile;
      if(userData[0].ImageStatus === true){
        
        sessionStorage.setItem('userImagePath', appConfig.imageUrl + userData[0].ImageURL);

      }else{

      }

    }else if(nextProps.authenticated === undefined || nextProps.authenticated ===false){
      
    }else{
      let access_token=sessionStorage.getItem("token");
      let userId=sessionStorage.getItem("user_id");
      if(userId){
        this.props.dispatch(fetchProfile(userId, access_token));
      }
    }
  }


  render() {

    let userview = "";

  if(this.props.setUser===undefined || this.props.setUser===false){
    if(sessionStorage.getItem('userImagePath')){
      userview = <div id="headerprofile"><img src={sessionStorage.getItem('userImagePath')} alt="" class="avatar" /></div>
    }else if(sessionStorage.getItem('user_name')){
      let user =sessionStorage.getItem('user_name').split(" ")[0].slice(0,1).toUpperCase() + sessionStorage.getItem('user_name').split(" ")[1].slice(0,1).toUpperCase();
      userview = <div id="headerprofilename">{user}</div>
    }
  }else{
    if(sessionStorage.getItem('userImagePath')){
      userview = <div ><img src={sessionStorage.getItem('userImagePath')} alt="" class="avatar" /></div>
    }else if(sessionStorage.getItem('user_name')){
      let user = sessionStorage.getItem('user_name').split(" ")[0].slice(0,1).toUpperCase() + sessionStorage.getItem('user_name').split(" ")[1].slice(0,1).toUpperCase();
      userview = <div id="headerprofilename">{user}</div>
    }
  }



     let menuItem = "";
    // let user = "";
    // if (this.props.setUser === undefined || this.props.setUser === false) {
    //   if (sessionStorage.getItem('user_name')) {
    //     user = sessionStorage.getItem('user_name').split(" ")[0].slice(0, 1).toUpperCase() + sessionStorage.getItem('user_name').split(" ")[1].slice(0, 1).toUpperCase();
    //   }

    // } else {
    //   user = (this.props.setUser[0].First_name.slice(0, 1).toUpperCase() + this.props.setUser[0].Last_name.slice(0, 1).toUpperCase());
    // }

    

    if (this.props.authenticated === undefined || this.props.authenticated === false) {
      let userid = sessionStorage.getItem('user_id');
      let access_token = sessionStorage.getItem('token');
      if (userid && access_token) {
        menuItem = <div><Navbar style={{backgroundImage:'linear-gradient(to left,#9853af,#623AA2)'}} fixed={`top`}  light expand="md">
             <NavbarBrand >
             <img src={require("./LOGO.png")}  className = "logoimage"/>
             </NavbarBrand>              
               <Nav className="ml-auto" navbar>
                <div id="usershowonmenu">{sessionStorage.getItem('user_name')}</div>
                 <AppHeaderDropdown direction="down">
                   <DropdownToggle nav>
                   {userview}
                   {/* <img src={pic}  alt="admin@bootstrapmaster.com"  style={{borderRadius:"40px", cursor:"pointer"}} height="30" width="30"/> */}
                   </DropdownToggle>
                   <DropdownMenu right style={{ right: 'auto',  cursor: 'pointer'  }}>    
                   <Link className="mm" to="/Profile"><DropdownItem style={{cursor: 'pointer'}}><img src={profileimg} height="10px" width="10px" />  Profile</DropdownItem></Link>     
                   <Link className="mm" to="/logout"> <DropdownItem style={{cursor: 'pointer'}}><img src={logoutimg} height="10px" width="10px" />  Logout </DropdownItem></Link>
                   </DropdownMenu>
                 </AppHeaderDropdown>
               </Nav>
           </Navbar>
          
           </div>
     
        
      } else {
        menuItem = <div>
      </div>
      }
    } else {
//background-image: linear-gradient(to left,#9853af,#623AA2);
      menuItem = 
      <div><Navbar style={{backgroundImage:'linear-gradient(to left,#9853af,#623AA2)'}} fixed={`top`}  light expand="md">
             <NavbarBrand >
             <img src={require("./LOGO.png")}  className = "logoimage"/>
             </NavbarBrand>              
               <Nav className="ml-auto" navbar>
               <div id="usershowonmenu">{sessionStorage.getItem('user_name')}</div>
                 <AppHeaderDropdown direction="down">
                   <DropdownToggle nav>
                   {userview}
                   {/* <img src={pic}  alt="admin@bootstrapmaster.com"  style={{borderRadius:"40px", cursor:"pointer"}} height="30" width="30"/> */}
                   </DropdownToggle>
                   <DropdownMenu right style={{ right: 'auto',  cursor: 'pointer'  }}>    
                   <Link className="mm" to="/Profile"><DropdownItem style={{cursor: 'pointer'}}><img src={profileimg} height="10px" width="10px" />  Profile</DropdownItem></Link>     
                   <Link className="mm" to="/logout"> <DropdownItem style={{cursor: 'pointer'}}><img src={logoutimg} height="10px" width="10px" />  Logout </DropdownItem></Link>
                   </DropdownMenu>
                 </AppHeaderDropdown>
               </Nav>
           </Navbar></div>

    //   menuItem =<div><Navbar color="light" light expand="md">
    //   <NavbarBrand >
    //   <h3>Leonids</h3>
    //   {/* <img src={require("./LOGO.png")} className="logoimage" /> */}
    //   </NavbarBrand>
    //   <NavbarToggler onClick={this.toggle} />
    //   <Collapse isOpen={this.state.isOpen} navbar>
    //     <Nav className="d-md-down-none" navbar>
    //       <NavItem className="px-3" color="primary">
    //       <Link className="mm" to ="/studyCount">Dashboard</Link>
    //       </NavItem>
    //       <NavItem className="px-3">
    //       <Link className="mm" to="/transactionHistory" > Transaction History </Link>
    //       </NavItem>
    //       <NavItem className="px-3">
    //       <Link className="mm" to="/Study" > Study </Link>
    //       </NavItem>
    //       <NavItem className="px-3">
    //       <Link className="mm" to="/profileDashboard"> MY Profile</Link>
    //       </NavItem>
    //     </Nav>
    //     <Nav className="ml-auto" navbar>
    //       <AppHeaderDropdown direction="down">
    //         <DropdownToggle nav>
    //           <img src={pic}  alt="admin@bootstrapmaster.com"  height="30" width="30"/>
    //         </DropdownToggle>
    //         <DropdownMenu right style={{ right: 'auto',  cursor: 'pointer'  }}>    
    //         <Link className="mm" to="/Profile"><DropdownItem style={{cursor: 'pointer'}}><i className="fa fa-user"></i>  Profile</DropdownItem></Link>     
    //         <Link className="mm" to="/logout"> <DropdownItem style={{cursor: 'pointer'}}><i className="fa fa-lock"></i>  Logout </DropdownItem></Link>
    //         </DropdownMenu>
    //       </AppHeaderDropdown>
    //     </Nav>
    //   </Collapse>
    // </Navbar>
    //     </div>
     
    }
    return (<div>
      <div className="menuBar">
        {menuItem}
      </div>
    </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authenticated: state.login.authenticated,
    setUser: state.profile.setUser,
    userProfile : state.profile.userProfile
  }
}

export default connect(mapStateToProps)(Menu);
