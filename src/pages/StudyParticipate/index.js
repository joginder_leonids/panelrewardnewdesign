import React, { Component } from 'react';
import './style.css';
import {Link} from 'react-router-dom';
import { studyParticipate} from '../../actions/study';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import {connect} from 'react-redux';
import {
  Button, Card, CardBody, CardGroup, CardSubtitle, FormGroup, Label, CardImg, CardTitle, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row,
  CardHeader, CardText, CardFooter
} from 'reactstrap';

class StudyParticipate extends Component {
  constructor(props){
    super(props)

    this.state={

    }

  }





  componentDidMount(){
    let studyId = ( window.location.pathname.split("/")[2].split("s:")[1]);
    let userId = ( window.location.pathname.split("/")[3].split("u:")[1]);
    let statusId = ( window.location.pathname.split("/")[4].split("status:")[1]);
    let Data = {}
      Data.studyId = studyId;
      Data.userId = userId;
      Data.status =  statusId;
     this.props.dispatch(studyParticipate(Data));
  }


  render() {

    let msg = "";
    let msgData = "";

    if(this.props.success === true ){
      msg = this.props.message;
        msgData = <div style={{padding:'1.2rem'}}>
                  {msg}
                  </div>
      }else{
        if(this.props.message){
          msg = this.props.message;
            msgData = <div className = "tPassMsgErr" style={{padding:'1.2rem'}}>{msg}</div>
        }
      }




    return (
      <div className = "studyParticipateView" style={{marginTop: "40px"}}>
            <div className="loading">{
                 this.props.requestStarted?
                 <div className="preloader"><LoadingDots interval={100} dots={5}/></div>
                 :
                 false
               }</div><br/>
               <Card className="text-center" style={{marginTop:'10%',marginLeft:'30%',marginRight:'30%'}}>
               <CardHeader><h1>Study Status</h1></CardHeader>
                <CardBody  className="text-center">
                  <h3> {msgData}</h3>
                  <Link to="/Study">
                  <Button style={{ backgroundColor: "#1985AC" }}>Participate New Study</Button>
                  </Link>
                </CardBody>
               </Card>
              


      </div>
    );
  }
}

const  mapStateToProps =(state) =>{
  return {
    message : state.study.messages,
    success : state.study.success
  }
}

export default  connect(mapStateToProps)(StudyParticipate);
