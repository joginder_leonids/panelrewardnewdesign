import React, { Component } from 'react';
import './style.css';
import {Link} from 'react-router-dom';
import { studyParticipate} from '../../actions/study';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import {connect} from 'react-redux';


class StudyParticipateFail extends Component {
  constructor(props){
    super(props)

    this.state={

    }

  }

  componentDidMount(){
    document.title = "Panel Reward-participant"
  }



  componentDidMount(){
    
    let studyId = ( window.location.pathname.split("UID=")[1].split("$SID$")[1].split("@UID@")[0]);
    let userId = ( window.location.pathname.split("UID=")[1].split("$SID$")[1].split("@UID@")[1]);
  
    let Data = {}
      Data.studyId = studyId;
      Data.userId = userId;
      Data.status =  3;
    this.props.dispatch(studyParticipate(Data));
  }


  render() {

    let msg = "";
    let msgData = "";

    if(this.props.success === true ){
      msg = this.props.message;
        msgData = <div style={{color:"#fff"}}>
                <h4>Thank you for participant </h4>
                Failed {msg}

                  </div>
      }else{
        if(this.props.message){
          msg = this.props.message;
            msgData = <div style={{color: "red"}}>Sorry, {msg}</div>
        }
      }




    return (
      <div className = "studyParticipateView">
        <div className="loading">{
          this.props.requestStarted?
          <div className="preloader"><LoadingDots interval={100} dots={5}/></div>
          :
          false
        }</div><br/>
        {msgData}
      </div>
    );
  }
}

const  mapStateToProps =(state) =>{
  return {
    message : state.study.messages,
    success : state.study.success
  }
}

export default  connect(mapStateToProps)(StudyParticipateFail);
