import React, { Component } from 'react';
import './style.css';
import { connect } from 'react-redux';
import appConfig from '../../api/apiConfig';
import {
  Button, Card, CardBody, CardGroup, CardSubtitle, FormGroup, Label, CardImg, CardTitle, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row,
  CardHeader, CardText, CardFooter
} from 'reactstrap';

class CompaneyParticipated extends Component {
  constructor(props) {
    super(props)

    this.state = {
      status: "",
      buttonShow: true
    }
    this.onChange = this.onChange.bind(this);
    this.studyParticipateStatus = this.studyParticipateStatus.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }



  studyParticipateStatus(){
    let url ="";
    let statusId =  this.state.status;
    let studyAndUserId = window.location.search.split("&UID")[1];
    let userId = ( window.location.search.split("&UID")[1].split("$SID$")[1].split("@UID@")[1]);

      

    if(appConfig.url.includes("localhost:")){
      // let url ="http://localhost:4010/studyParticipate/s:"+ studyId + "/u:"+ userId+"/status:" + statusId ;
      // let url = appConfig.url +"studyParticipate/s:"+ studyId + "/u:"+ userId+"/status:" + statusId ;
      // window.open(url,'_self'); 

      if(statusId == 1){   //"Completed"
      url ="http://localhost:4011/studyParticipateCompleted/UID"+studyAndUserId;
     
      window.open(url,'_self'); 

      }else if(statusId == 2){   //"Quota Fail"
      url ="http://localhost:4011/studyParticipateQuotaFail/UID"+studyAndUserId;
     
      window.open(url,'_self'); 
      }else if(statusId == 3){    //Fail
        url ="http://localhost:4011/studyParticipateFail/UID"+studyAndUserId;
     
      window.open(url,'_self'); 
     }
    }else{
      // server
      if(statusId == 1){   //"Completed"
      url = appConfig.completedUrl+"UID"+studyAndUserId;
      window.open(url,'_self'); 

      }else if(statusId == 2){   //"Quota Fail"
      url = appConfig.quotaFailUrl+"UID"+studyAndUserId;
      window.open(url,'_self'); 

      }else if(statusId == 3){    //Fail
      url = appConfig.failUrl+"UID"+studyAndUserId;
      window.open(url,'_self'); 
      }
    }
  }




  render() {


    return (
      <div className="paypal" style={{marginTop: "40px"}}>
        <div className="cardback">
          <Card style={{marginLeft:'35%',marginRight:'35%'}} className="text-center">
            <CardHeader><h1 className="text-center">Select Status</h1></CardHeader>
            <CardTitle>Please select status for participated Study</CardTitle>
            <CardBody>
              <FormGroup check className="text-left" style={{marginLeft:'40%'}}>
                <Label check>
                  <Input type="radio" name="status" value="1" onChange={this.onChange} />Completed</Label>

              </FormGroup>
              <FormGroup check className="text-left" style={{marginLeft:'40%'}}>
                <Label check>
                  <Input type="radio" name="status" value="2" onChange={this.onChange} />QuotaFail</Label>

              </FormGroup>
              <FormGroup check className="text-left" style={{marginLeft:'40%'}}>
                <Label check>
                  <Input type="radio" name="status" value="3" onChange={this.onChange} />Fail</Label>

              </FormGroup><br/><br/>
              <Button onClick={this.studyParticipateStatus} style={{backgroundColor:'#1985AC'}}>Submit</Button>
            </CardBody>
          </Card>
        </div>
      </div>

      // <div className = "studyParticipateView">
      // <p className = "para"> Select status  </p>
      // <label>Please select status for participated Study</label><br/>
      // <input type="radio" name="status" value="1"  onChange={this.onChange} /> Completed<br/>
      // <input type="radio" name="status" value="2"  onChange={this.onChange} /> QuotaFail<br/>
      // <input type="radio" name="status" value="3"  onChange={this.onChange} /> Fail <br/><br/>
      // <button  className="studyParticipatebtn" onClick={this.studyParticipateStatus} >Submit</button><br/>
      // </div>
    );
  }


}

export default CompaneyParticipated;
