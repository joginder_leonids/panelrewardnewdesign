import React, { Component } from 'react';
import { Fragment } from 'react';
import './profileupload.css';
import desktop from '../../icons/desktop.png';
import buffer from '../../icons/buffer.png';
import bank from '../../icons/bank.png';
import card from '../../icons/card.png';
import arrow from '../../icons/arrowangle.png';
import logout from '../../icons/logout.png';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import editpro from '../../icons/edit-draw-pencil.png';
import {connect} from 'react-redux';
import avtar from '../../images/avtar.jpg';
import {addProfileImage} from '../../actions/profile';
class ImageUpload extends Component {

  constructor(props) {
    super(props);

    this.state = {
          file:'',
          showUploadbtn:true
    };
    this.profileUpload=this.profileUpload.bind(this);
  }

  
  onFileLoad(e){

    //e.preventDefault();
    let fileReader = new FileReader();
    let file1 = e.target.files[0];
    fileReader.onload=()=>{
      this.setState({file : file1});
      let dataURL = fileReader.result;
      let output = document.getElementById('profileimg');
      output.src = dataURL;

      //console.log("Image Uploaded :", dataURL);
    }
    if(file1){
      fileReader.readAsDataURL(file1);
      this.setState({showUploadbtn:false});

    }
    

    fileReader.onabort=()=>{
      alert("Reading aborted");
    }
    fileReader.onerror=()=>{
      alert("File reading error");
    }

  }

  profileUpload(){

    const data = new FormData();
    data.append('sampleFile', this.state.file);
    // this.setState({
    //   file: '',
    //   value : ''
    // });

    if(this.state.file){
      let userId = sessionStorage.getItem("user_id");
      this.props.dispatch(addProfileImage(data, userId));
     }else{
       alert("Please choose image");
     }
  }

  render() {
    const previewStyle = {
      display: 'inline',
      width: 100,
      height: 100,
    };

    let userImageView = "";
    if(this.state.file){
      userImageView = <div><img id="profileimg" /></div>
    }else if(sessionStorage.getItem('userImagePath')){
      userImageView = <div id="profileimg"><img src={sessionStorage.getItem('userImagePath')} alt="" /></div>
      // <div><img  src={sessionStorage.getItem('userImagePath')} alt="" height="100%" width="100%" style={{borderRadius:'50%'}}/></div>
    }else{
      userImageView = <div id="profileavtar"><img  src={avtar} /></div>
    }

    let showActionDiv ="";
    if(this.state.showUploadbtn){
      showActionDiv =  <div><span>Change your profile image and upload it.</span></div>
    }else{
      showActionDiv = <div>
         <button disabled={this.state.showUploadbtn} onClick={this.profileUpload}>Upload Image</button>
        </div>
    }

    return (
      <div id="studycountmain">
        <div id="headerstudy">
          <h3>Profile Image</h3>
          <ol id="breadcrumb">
            <li><a href="javascript:void(0)" style={{ color: 'white', textDecoration: 'none' }}></a></li>
            <li style={{ opacity: '0.8' }}></li>
          </ol>
        </div>
        <div id="proflieupload">
         
          <div id="proflepicspace">
            <div id="imageinput">
              {userImageView}
                <div id="editprofile">
                    <img src={editpro} height="20px" width="20px" />
                        <input
                        type="file"
                        id="file-browser-input"
                        name="file-browser-input"
                        accept=".png, .jpg, .jpeg"
                        ref={input => this.fileInput = input}
                        // onDragOver={(e)=>{
                        //   e.preventDefault();
                        //   e.stopPropagation();
                        // }}
                        // onDrop={this.onFileLoad.bind(this)}
                        onChange={this.onFileLoad.bind(this)}
                        />

                  </div>
                </div>
               
            </div> 
            <div id="imageUploadbtn">
            
            {showActionDiv}
            </div>
          
            </div>
            </div>

      //   <div id="proflieupload">
      //     <div id="proflepicspace">
      //       <div id="imgandtext">
      //         <div


      //           className="inner-container"
      //           style={{
      //             display: "flex",
      //             flexDirection: "column"
      //           }}>
      //           <div className="sub-header">Drag an Image</div>
      //           <div className="draggable-container">
      //             <input
      //               type="file"
      //               id="file-browser-input"
      //               name="file-browser-input"
      //               ref={input => this.fileInput = input} />
      //             <div className="files-preview-container"></div>
      //             <div className="helper-text">Drag and Drop Images Here</div>
      //             <div className="file-browser-container">
      //               {/* <AnchorButton
      //                 text="Browse"
      //                 intent={Intent.PRIMARY}
      //                 minimal={true}
      //                 onClick={() => this.fileInput.click()} /> */}
      //                 <button>Browser</button>
      //             </div>
      //           </div>
      //           <button>Upload</button>
      //           {/* <AnchorButton text="Upload" intent={Intent.SUCCESS} /> */}
      //         </div>
      //       </div>
      //     </div>
      //   </div>
      // </div>
    );
  }

}

const  mapStateToProps =(state) =>{
  return {
    authenticated : state.login.authenticated,
    userProfile : state.profile.userProfile,
    profileImageUploadRes : state.profile.profileImageUploadRes
  }
}

export default connect(mapStateToProps)(ImageUpload);