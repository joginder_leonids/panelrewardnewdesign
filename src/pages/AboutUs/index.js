import React, { Component } from 'react';


class AboutUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
 
  }

  render() {
    return(
        <div  > sfsf
    <div id="icons">
      <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
      <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
      <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
      <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
    </div>

      <div className = ""> <h2>About Us</h2></div>
      <div className = "">
        <p className = "">Panel Reward is a diverse community of over 2.7 million consumers from world’s fastest growing economies.</p>
        
        <p className = "">As a member, your opinions on consumer goods, technology, healthcare, travel, and finance help global brands build better products and create better experiences. So, what are you waiting for - share your thoughts with the world’s prominent brands and get rewarded. With the strong support of our community members, we are shaping to become one of the world's largest consumer panel and 
        a premier global provider of consumer research information to major corporations.</p>

        <p className = "">We specialize in providing consumer insights via online panel services in emerging & hard-to-reach markets across the world. We value your data security, privacy and confidentiality.</p>
        
        <h4 className = "">REDEMPTION PROCESS :</h4>
        <p className = "">
        Every survey fetches you reward points ranging from 100 to 5000 depending on the complexity and length of every survey. 
        automatically credits the points earned to your account.
        You need minimum 3000 points in your account to start redeeming.
        </p>
        <h4 className = "">BEST PRACTICES :</h4>
        <ul>
        <li>One Person. One Account</li>
          <p className = "">
          Ensure that you have only one account with The . If you have multiple accounts you will be informed and flagged as a suspect. All your accounts will be deactivated and points in each will be suspended.
          </p>
        <li>It’s all about being genuine</li>
          <p className = "">
          Honest opinion is what we are looking for! While taking surveys, please ensure you do not cheat by straight-lining (selecting same choice for all questions), speeding (finishing a 25 minute survey in 5 minutes) or answering the same survey multiple times. Our system identifies these fraudulent activities and you risk being flagged as a suspect & losing access to your account.
          </p>
        </ul><br/>
        <h4 className = "">HOW IT WORKS</h4>
        <h5>Step-1</h5>
        <p style={{textAlign:"left"}}><b>Sign Up : - </b> Fill a short and simple registration form and you will receive your login credentials on your registered email Id.</p>
        <h5>Step-2</h5>
        <p style={{textAlign:"left"}}><b>Take Surveys : - </b> We will send you few profiler surveys to understand your interests and background. Once you fill this profiling information, you will start receiving surveys with reward points ranging from 100 – 5000 points.</p>
        <h5>Step-3</h5>
        <p style={{textAlign:"left"}}><b>Earn Rewards : - </b> Accumulate 3000 points and redeem them for a wide range of exciting gifts.</p>
      </div>
    </div>
    );
}
}

export default AboutUs;
