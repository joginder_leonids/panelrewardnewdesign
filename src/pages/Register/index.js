import React, { Component } from 'react';
import CreateRegisterUser from '../../components/Register/NewRegister';
import {registerUser, setRegistrationFail, setRegistrationMessage, EmptyRegisterProps} from '../../actions/registration';
import {connect} from 'react-redux';
import imageuploads from '../../components/imageupload/imageuploads';

class Registers extends Component {
  constructor(props){
    super(props)
    this.state = {
      showRegister : true
    }
  this.saveData = this.saveData.bind(this);
  this.loginPush = this.loginPush.bind(this);
  }

saveData(data){
  this.props.dispatch(registerUser(data));
}

loginPush(){
  this.props.dispatch(setRegistrationMessage);
  this.props.history.push("/Login");
}

componentWillMount() {
  this.props.dispatch(setRegistrationMessage);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.success){
      sessionStorage.setItem('registerUserMail', nextProps.registerUserData.Email);
      sessionStorage.setItem('registerUserName', nextProps.registerUserData.First_name);
      this.setState({showRegister:false})
      this.props.history.push("/thankuPage");
      this.props.dispatch(EmptyRegisterProps());
    }

  }


  render() {

   let createRegisterHtml ="";
   if(this.state.showRegister === true){
    createRegisterHtml =   <CreateRegisterUser
    saveData = {this.saveData}
    registered  ={this.props.success}
    failMessage = {this.props.failMessage}
    requestStarted ={this.props.requestStarted}
    loginPush = {this.loginPush}
      />
  }
    
   return (<div>
          {createRegisterHtml}
          <imageuploads/>
           </div>
   );
  }
}

const  mapStateToProps =(state) =>{
  return {
      success : state.registration.success,
      failMessage : state.registration.userFailmessage,
      requestStarted:state.common.requestStarted,
      registerUserData : state.registration.registerUserData
  }
}

export default connect(mapStateToProps)(Registers);
