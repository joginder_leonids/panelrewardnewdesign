import React, { Component } from 'react';
import {connect} from 'react-redux';
import {fetchUserTraction, getGiftcouponByUser}  from '../../actions/reward';
import TransactionHistoryView from '../../components/TransactionHistory';
import GiftcouponView from '../../components/TransactionHistory/giftCouponView';

class TransactionHistory extends Component {
  constructor(props){
    super(props)
    this.state = {
    }
    this.getGiftcouponByUser = this.getGiftcouponByUser.bind(this);
    this.showTranactions = this.showTranactions.bind(this);
    this.showDashboard = this.showDashboard.bind(this);
  }

  componentWillMount() {
    //let access_token=sessionStorage.getItem("token");
       let userId=sessionStorage.getItem("user_id");
        if(userId){
      //   this.props.dispatch(fetchUserTraction(userId));
       }
     }

  showTranactions(){
    let userId=sessionStorage.getItem("user_id");
        if(userId){
         this.props.dispatch(fetchUserTraction(userId));
       }
  }

  getGiftcouponByUser(data){
    this.props.dispatch(getGiftcouponByUser(data.assignGiftcouponId));
  }

  showDashboard(){
    this.props.history.push("/studyCount");
  }


  render() {
  //   if(!this.props.authenticated){
  //       let access_token=sessionStorage.getItem("token");
  //       let userId=sessionStorage.getItem("user_id");
  //       if(userId){
             
  //       }else {
  //       alert("Please login.");
  //       this.props.history.push("login");
  //   }
  // }

    let transactionHistoryView = "";

    if(this.props.showGiftcouponDataView){
      transactionHistoryView = <GiftcouponView
      GiftcouponDataSuccess = {this.props.GiftcouponDataSuccess}
      GiftcouponData = {this.props.GiftcouponData}
      showTranactions = {this.showTranactions}
      showDashboard = {this.showDashboard}
      />
    }else{
      transactionHistoryView = <TransactionHistoryView
                              UserTranactionSuccess = {this.props.UserTranactionSuccess}
                              UserTranactionData = {this.props.UserTranactionData}
                              getGiftcouponByUser = {this.getGiftcouponByUser}
                              showTranactions = {this.showTranactions}
                              />
    }

   return (<div>
            {transactionHistoryView}
           </div>

   );
  }
}

const  mapStateToProps =(state) =>{
  return {
    authenticated : state.login.authenticated,
    UserTranactionSuccess : state.reward.UserTranactionSuccess,
    UserTranactionData : state.reward.UserTranactionData,
    showGiftcouponDataView : state.reward.showGiftcouponDataView,
    GiftcouponDataSuccess : state.reward.GiftcouponDataSuccess,
    GiftcouponData : state.reward.GiftcouponData


  }
}

export default connect(mapStateToProps)(TransactionHistory);
