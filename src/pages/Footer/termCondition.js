import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import facebook from '../../components/Image/facebook-logo.svg';
import google from '../../components/Image/google-logo.svg';
import twitter from '../../components/Image/twitter.svg';
import linked from '../../components/Image/linkedin.svg';
import logo from '../../images/panel reward.png';
import cancel from '../../icons/cancel.png';

class TermAndCondition extends Component {
  constructor(props){
    super(props)
    this.state = {

    }
  }

  componentDidMount(){
    document.title = "Panel Reward-Term&Condition"
  }
  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("aboutbtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("aboutbtn").style.display = "block";
  }

  render() {
    let hearderDiv = "";
    if(!this.props.authenticated){
             let userId=sessionStorage.getItem("user_id");
              if(userId){
               
             }else {
              hearderDiv = <div id="footerheader">
              <div id="footerlogo"><img src={logo} height="50px" width="200px" /></div>
              <div id="footericons">
                  <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <a href="javascript:void(0)" onClick={() => this.openNav()}> <button id="aboutbtn">&#9776;</button></a>
                </div>
            </div>
             }
      }
   return (
    <div id="footercontainer">
       {hearderDiv}
      <div id="termstitile">
          <h1>Terms & Conditions</h1>
          <p>Panel Reward is a diverse community of over 2.7 million consumers from world’s fastest growing economies.</p>
        </div>
        <div id="termscontent">
            <em></em>
                <h1>Terms & Conditions</h1>
                    <p >Our survey scripting services are provided through third parties from their proprietary applications and such third parties shall be collectively responsible for all the liabilities with respect to the functioning of the meter mentioned under these terms and conditions.
                    These Terms & Conditions applies to all participants of  website, the app and/or the TPS surveys app.              
                    We specialize in providing consumer insights via online panel services in emerging & hard-to-reach markets across the world. We value your data security, privacy and confidentiality.</p>
                  
                   <em></em>
                   <h4>About Application</h4>
                   <p>Any and all individuals dealing with , is subject to the following Terms and Conditions herein after also referred as general user conditions. 
                       By participating in  you undertake to abide and agree to be bound by the privacy policy of the company and these general user conditions set , which it shall be free to modify at any time without informing you of such changes. Consequently, it is recommended that you refer to the latest version of the Terms & Conditions regularly, which are available on the site at all times. 
                                Without exception, when joining this Service by clicking on the tick box at registration, you agree to accept these general conditions beforehand, the purpose of which is to define the terms under which you benefit from the Service. </p>

                    <em></em>    
                    <h4>PLEASE NOTE: By downloading ‘Panel Reward’, and on your further agreement for sharing the data for research purpose a metering software gets downloaded and you accept the terms and conditions stated in a legally binding manner.</h4>
                    <ol>
                        <h3> &#8888; Use of this Website and the Contract Between Us  </h3>
                        <li>
                            In this Agreement the terms “you” and “the member” mean the individual completing the registration process to become a member of  program.
                         </li>
                         <li>
                            You agree to only use this website and the App along with the meter, in accordance with this Agreement and all applicable laws. 
                        </li>
                        <li>
                            Once you have submitted your answers to our joining questionnaire you become a member of Panel Reward. 
                        </li>
                        <li>
                           This Agreement is a contract for the provision of services and nothing in this Agreement shall create or be deemed to create a legal partnership or the relationship of agent and principal or the relationship of employer and employee between you and the Company.
                        </li>
                        <li>
                            <b>Purpose – Data & Information collection upon downloading metering software
                            a. Meter records and collects from your mobile phone, data, including but not limited to</b>
                              </li>
                              <ul>
                                <li>Device IMEI</li>
                                <li>Operating system</li>
                                <li>Device model</li>
                                <li>Apps installed</li>
                                <li>Device IMEI</li>
                                <li>Device IMEI</li>
                                <li>Device IMEI</li>
                                <li>Apps used</li>
                                <li>Websites visited</li>
                                <li>Number of calls / messages made or received </li>
                                <li>User agent (name of app making the request)</li>
                                <li>Referring site (site you visited before coming to our site)</li>
                                <li>Date and time of visit</li>
                                <li>All http requests from the device which will include URL of pages visited</li>
                                <li>In this process, we may also collect some information about the content that was accessed including media viewed, images downloaded or viewed, display advertisements etc.</li>
                                <li>Size of data consumed by each request etc</li>
                              </ul>
                    </ol>
            </div>
            <div id="myNav" class="overlay">
              <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn"><img src={cancel} height="18px" width="18px" className="link" /></button></a>
              <div class="overlay-content">
              <Link to='/aboutUs'>About Us</Link>
              <Link to='/register'>New Member Registration</Link>
              <Link to='/Login'>Login</Link>
              </div>
          </div>  
    </div>



    // <div className = "trmAndCondPage" > 
    //   <div className = "tAndCHeading"> <h2> Terms And Conditions </h2></div>
    //   <div className = "abpage">
        // <p className = "para1">Our survey scripting services are provided through third parties from their proprietary applications and such third parties shall be collectively responsible for all the liabilities with respect to the functioning of the meter mentioned under these terms and conditions.</p>
        
    //     <p className = "para1">These Terms & Conditions applies to all participants of  website, the app and/or the TPS surveys app.</p>

    //     <p className = "para1">We specialize in providing consumer insights via online panel services in emerging & hard-to-reach markets across the world. We value your data security, privacy and confidentiality.</p>
        
    //     <h4 className = "aboutHeading1">About Application :</h4>
    //     <p className = "para1">
    //     Any and all individuals dealing with , is subject to the following Terms and Conditions herein after also referred as general user conditions. 
    //     </p>
    //     <p className = "para1">By participating in  you undertake to abide and agree to be bound by the privacy policy of the company and these general user conditions set , which it shall be free to modify at any time without informing you of such changes. Consequently, it is recommended that you refer to the latest version of the Terms & Conditions regularly, which are available on the site at all times. </p>
    //     <p className = "para1">Without exception, when joining this Service by clicking on the tick box at registration, you agree to accept these general conditions beforehand, the purpose of which is to define the terms under which you benefit from the Service. </p>
    //     <br/>
    //     <h4 className = "aboutHeading1"><p>PLEASE NOTE: By downloading ‘Panel Reward’, and on your further agreement for sharing the data for research purpose a metering software gets downloaded and you accept the terms and conditions stated in a legally binding manner.</p></h4>
    //     <ol> <h4 className = "aboutHeading1"> &#8888; Use of this Website and the Contract Between Us  </h4>
    //         <li>
    //             <p style={{textAlign:"left"}}> In this Agreement the terms “you” and “the member” mean the individual completing the registration process to become a member of  program.</p>
    //         </li>
    //         <li>
    //             <p style={{textAlign:"left"}}> You agree to only use this website and the App along with the meter, in accordance with this Agreement and all applicable laws. </p>
    //         </li>
    //         <li>
    //             <p style={{textAlign:"left"}}> Once you have submitted your answers to our joining questionnaire you become a member of Panel Reward </p>
    //         </li>
    //         <li>
    //             <p style={{textAlign:"left"}}> This Agreement is a contract for the provision of services and nothing in this Agreement shall create or be deemed to create a legal partnership or the relationship of agent and principal or the relationship of employer and employee between you and the Company. </p>
    //         </li>
    //         <li>
    //         <h4 className = "aboutHeading1"><p style={{textAlign:"left"}}>Purpose – Data & Information collection upon downloading metering software
    //                 a. Meter records and collects from your mobile phone, data, including but not limited to
    //             </p></h4>
    //         </li>
    //     </ol>
    //     <ul>
    //         <li>Device IMEI</li>
    //         <li>Operating system</li>
    //         <li>Device model</li>
    //         <li>Apps installed</li>
    //         <li>Device IMEI</li>
    //         <li>Device IMEI</li>
    //         <li>Device IMEI</li>
    //         <li>Apps used</li>
    //         <li>Websites visited</li>
    //         <li>Number of calls / messages made or received </li>
    //         <li>User agent (name of app making the request)</li>
    //         <li>Referring site (site you visited before coming to our site)</li>
    //         <li>Date and time of visit</li>
    //         <li>All http requests from the device which will include URL of pages visited</li>
    //         <li>In this process, we may also collect some information about the content that was accessed including media viewed, images downloaded or viewed, display advertisements etc.</li>
    //         <li>Size of data consumed by each request etc</li>
    //     </ul>
    //   </div>
    // </div>
   );
  }
}

const  mapStateToProps =(state) =>{
  return {
    
  }
}

export default connect(mapStateToProps)(TermAndCondition);
