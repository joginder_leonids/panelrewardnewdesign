import React, { Component } from 'react';
import {connect} from 'react-redux';
import './aboutus.css';
import {Link} from 'react-router-dom';
import facebook from '../../components/Image/facebook-logo.svg';
import google from '../../components/Image/google-logo.svg';
import twitter from '../../components/Image/twitter.svg';
import linked from '../../components/Image/linkedin.svg';
import logo from '../../images/panel reward.png';
import screendemo from '../../images/devices_2.png';
import cancel from '../../icons/cancel.png';

class AboutUs extends Component {
  constructor(props){
    super(props)
    this.state = {

    }
  }

  componentDidMount(){
    document.title = "Panel Reward-About us"
  }
  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("aboutbtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("aboutbtn").style.display = "block";
  }
  
  render() {
    let hearderDiv = "";
    if(!this.props.authenticated){
             let userId=sessionStorage.getItem("user_id");
              if(userId){
               
             }else {
              hearderDiv = <div id="footerheader">
              <div id="footerlogo"><img src={logo} height="50px" width="200px" /></div>
              <div id="footericons">
                  <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <a href="javascript:void(0)" onClick={() => this.openNav()}> <button id="aboutbtn">&#9776;</button></a>
                </div>
            </div>
             }
      }

   return ( 
        <div id="footercontainer">
          {hearderDiv}
            
            <div id="abouttitle">
                <h1>About Us</h1>
                <p>Panel Reward is a diverse community of over 2.7 million consumers from world’s fastest growing economies.</p>
              </div>
              {/* <div id="saperator"> <em></em></div> */}
              <div id="aboutdetails">
                  <div id="aboutcontent">
                      <div id="contents">
                          <h1>Panel Reward</h1>
                          <p id="p1">As a member, your opinions on consumer goods, technology, healthcare, travel, and finance help global brands build better products and create better experiences. So, what are you waiting for - share your thoughts with the world’s prominent brands and get rewarded. With the strong support of our community members, we are shaping to become one of the world's largest consumer panel and 
                              a premier global provider of consumer research information to major corporations.</p>
                              
                              <p id="p1">We specialize in providing consumer insights via online panel services in emerging & hard-to-reach markets across the world. We value your data security, privacy and confidentiality.</p>  
                            <h4>REDEMPTION PROCESS :</h4>
                            <p id="p1">Every survey fetches you reward points ranging from 100 to 5000 depending on the complexity and length of every survey. 
                                automatically credits the points earned to your account.
                                You need minimum 3000 points in your account to start redeeming.
                                </p>
                          </div>
                    </div>
                  <div id="aboutss">
                  <img src={screendemo} style={{height:'auto' , maxWidth:'100%'}} />
                    </div>
              </div>
              <div id="myNav" class="overlay">
              <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn"><img src={cancel} height="18px" width="18px" className="link" /></button></a>
              <div class="overlay-content">
                <Link to='/aboutUs'>About Us</Link>
                <Link to='/register'>New Member Registration</Link>
                <Link to='/Login'>Login</Link>
              </div>
          </div>  
        </div> 
   );
  }
}

const  mapStateToProps =(state) =>{
  return {
    authenticated: state.login.authenticated,
  }
}

export default connect(mapStateToProps)(AboutUs);
