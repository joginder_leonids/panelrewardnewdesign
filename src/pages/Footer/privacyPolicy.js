import React, { Component } from 'react';
import {connect} from 'react-redux';
import './style.css';
import {Link} from 'react-router-dom';
import facebook from '../../components/Image/facebook-logo.svg';
import google from '../../components/Image/google-logo.svg';
import twitter from '../../components/Image/twitter.svg';
import linked from '../../components/Image/linkedin.svg';
import logo from '../../images/panel reward.png';
import cancel from '../../icons/cancel.png';

class PrivacyPolicy extends Component {
  constructor(props){
    super(props)
    this.state = {

    }
  }

  componentDidMount(){
    document.title = "Panel Reward-PrivacyPolicy"
  }
  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("aboutbtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("aboutbtn").style.display = "block";
  }
  
  
  render() {
    let hearderDiv = "";
    if(!this.props.authenticated){
             let userId=sessionStorage.getItem("user_id");
              if(userId){
               
             }else {
              hearderDiv = <div id="footerheader">
              <div id="footerlogo"><img src={logo} height="50px" width="200px" /></div>
              <div id="footericons">
                  <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <a href="javascript:void(0)" onClick={() => this.openNav()}> <button id="aboutbtn">&#9776;</button></a>
                </div>
            </div>
             }
      }

   return (
    <div id="footercontainer">
       {hearderDiv}
      <div id="privacytitle">
          <h1>Privacy Policy</h1>
          <p>Panel Reward is a diverse community of over 2.7 million consumers from world’s fastest growing economies.</p>
        </div>
        <div id="privacycontent">
        <em></em>
          <h1>Privacy Policies</h1>
             
        <p>The impending privacy statement discloses the Company’s information gathering and dissemination practices. It is important for you (for the purpose of this privacy statement, the term “you”, “your” shall construe to mean the Data subject who is voluntarily registering himself or herself on the Panel Reward website) to understand what information we collect about you during your visit to Panel Reward  website and while participating in a survey. This privacy statement would also give you a detailed understanding about the way your information is processed after we receive a consent from you to opt in for passive measurement and installing of a particular “meter” on your device by our third party vendor (please refer the “security of personal information”. 
          This Privacy Statement applies to information collected on  Panel Reward website and its partner in case of opt in, either through online, on device surveys or passive measurement enabled through meter in case opted in and any other information that is collected by Panel Reward. To access the services that we offer, you are generally required to register in this Panel Reward website and you may later on choose to establish which form of services you intend to access. In these situations, if you choose to withhold any personal data requested by us, it may not be possible for you to gain access to certain parts of the site and for us to respond to your queries in a detailed manner. 
          However, you would be eligible to access other parts of our services apart from those that you have consented to not go ahead with. </p>

          <p>Panel Reward mobile application  allows panellists to take surveys they are eligible for and also provides information regarding points earned and their redemption. In addition, the app, after obtaining user opt in, in partnership with our third party, also has the capability of passively capturing mobile behavioural data through its meter installed on your device and sending it across to cloud based servers located on the Microsoft Azure platform. The installation and collection of behavioural data through the third party meter is restricted in various jurisdictions and is applicable for information that you choose to disclose over our android platform in a constricted manner. 
            The capturing of behavioural data is not applicable to any data subject located outside the Indian Territory. </p>

            <p>There may be certain aspects of this policy which may be applicable to you only if you are residing/a part of or a citizen of the European Union and hence If you are an individual who is planning to take part in our surveys from the European Union, then you are required to keep us informed about the same since you would be subject to additional data protection rights if you are operating from the European Union as per the GDPR. You may write to us to inform the specifics of your association with the EU by reaching out to us at support@Panel Reward.com </p>

            <p>
              Following paragraphs detail out the nature of the data captured and passive measurement techniques and our corresponding privacy statment related to the same. 
              For the purpose of this Privacy Statement the term “Personal Information” shall mean any information that relates to a natural person, or related to the metered device which either directly or indirectly, in combination with other information available or is likely to be available , is capable of identifying such person or such device.”
              </p>
              <br/>
              <em></em>
              <h4 >Panel Reward processes your personal data for the following purposes, however the purposes mentioned below are not exhaustive in nature and any additional way through which your information is being processed would be specifically informed to you</h4>
              <ul>
                  <li>The creation of a database of individuals willing to take part in research and surveys.</li>
                  <li>Contacting you by email to invite you to take part in surveys and when permitted by you, to send you electronic marketing communications keeping you informed about the activities of Panel Reward.</li>
                  <li>Evaluating which individuals registered with Panel Reward are best suited for and therefore should be approached for specific surveys</li>
                  <li>Providing you with information about rewards/gifts given to you by Panel Reward as a result of your responding to survey requests</li>
                  <li>Processing the answers to surveys and summarizing the results</li>
                  <li>Undertaking research surveys on behalf of third parties</li>
                  <li>Keeping you informed and obtain your views of Panel Reward’s products, services and activities</li>
                  <li>The administration of your membership of Panel Reward (if you have registered as a member)</li>
                  <li>Statutory and regulatory compliance</li>
                  <li>Processing any enquiries raised by you and other communications initiated by you in relation to your dealings with Panel Reward and any services provided by Panel Reward, or its legal entity</li>
                  <li>We may also use the information you provide in aggregate form for internal business purposes, such as generating statistics and developing marketing plans</li>
                  <li>We may share or transfer such non-personally identifiable information with or to our affiliates, licensees, agents and partners by pseudonymising your data</li>
                  <li>In addition, we may disclose any information, including personally identifiable information, we deem necessary, in our sole discretion, to comply with any applicable law, regulation, legal proceeding or governmental request</li>
                  <li>Please be informed that for any and all Personal Information that is gets collected or captured by the meter, we shall aggregate / collate it in a non-personally identifiable manner so that the same would not remain the Personal Information. We understand the importance of confidentiality of the Personal Information and confirm that the Personal Information will not be shared with any person in a personally identifiable manner without your specific approval.</li>
                  <li>We only retain the personal data collected from you as long as your account is active or otherwise for a limited period of time as long as we need to fulfil the pruposes for which we have initially collected it, unless otherwise required by law. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements</li>
                  <li>We may, if necessary process your information on server located outside your country of residence. Currently we are using Microsoft Azure to secure and safely store your information</li>
                  <li>We may transfer all or part of its business or assets to a third party after receiving your consent. Your information or Personal Information may be a part of such business or assets. Such transfer of information may be outside your country of residence also.</li>
                  </ul>
          </div>
           <div id="myNav" class="overlay">
              <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn"><img src={cancel} height="18px" width="18px" className="link" /></button></a>
              <div class="overlay-content">
              <Link to='/aboutUs'>About Us</Link>
              <Link to='/register'>New Member Registration</Link>
              <Link to='/Login'>Login</Link>
              </div>
          </div>  
        </div>


    // <div className = "policyPage" >  
    //   <div className = "privacyPolicy"> <h2>Privacy Policy</h2></div>
    //   <div className = "abpage">
    //     <p className = "para1">The impending privacy statement discloses the Company’s information gathering and dissemination practices. It is important for you (for the purpose of this privacy statement, the term “you”, “your” shall construe to mean the Data subject who is voluntarily registering himself or herself on the Panel Reward website) to understand what information we collect about you during your visit to Panel Reward  website and while participating in a survey. This privacy statement would also give you a detailed understanding about the way your information is processed after we receive a consent from you to opt in for passive measurement and installing of a particular “meter” on your device by our third party vendor (please refer the “security of personal information”. 
    //       This Privacy Statement applies to information collected on  Panel Reward website and its partner in case of opt in, either through online, on device surveys or passive measurement enabled through meter in case opted in and any other information that is collected by Panel Reward. To access the services that we offer, you are generally required to register in this Panel Reward website and you may later on choose to establish which form of services you intend to access. In these situations, if you choose to withhold any personal data requested by us, it may not be possible for you to gain access to certain parts of the site and for us to respond to your queries in a detailed manner. 
    //       However, you would be eligible to access other parts of our services apart from those that you have consented to not go ahead with. </p>
        
    //     <p className = "para1">Panel Reward mobile application  allows panellists to take surveys they are eligible for and also provides information regarding points earned and their redemption. In addition, the app, after obtaining user opt in, in partnership with our third party, also has the capability of passively capturing mobile behavioural data through its meter installed on your device and sending it across to cloud based servers located on the Microsoft Azure platform. The installation and collection of behavioural data through the third party meter is restricted in various jurisdictions and is applicable for information that you choose to disclose over our android platform in a constricted manner. 
    //     The capturing of behavioural data is not applicable to any data subject located outside the Indian Territory. </p>

      //  <p className = "para1">There may be certain aspects of this policy which may be applicable to you only if you are residing/a part of or a citizen of the European Union and hence If you are an individual who is planning to take part in our surveys from the European Union, then you are required to keep us informed about the same since you would be subject to additional data protection rights if you are operating from the European Union as per the GDPR. You may write to us to inform the specifics of your association with the EU by reaching out to us at support@Panel Reward.com </p>
        
    //     <p className = "para1">
    //     Following paragraphs detail out the nature of the data captured and passive measurement techniques and our corresponding privacy statment related to the same. 
    //     For the purpose of this Privacy Statement the term “Personal Information” shall mean any information that relates to a natural person, or related to the metered device which either directly or indirectly, in combination with other information available or is likely to be available , is capable of identifying such person or such device.”
    //     </p>
    //     <h4 className = "aboutHeading1"><p>Panel Reward processes your personal data for the following purposes, however the purposes mentioned below are not exhaustive in nature and any additional way through which your information is being processed would be specifically informed to you: </p></h4>
    //     <ul>
    //     <li>The creation of a database of individuals willing to take part in research and surveys.</li>
    //     <li>Contacting you by email to invite you to take part in surveys and when permitted by you, to send you electronic marketing communications keeping you informed about the activities of Panel Reward.</li>
    //     <li>Evaluating which individuals registered with Panel Reward are best suited for and therefore should be approached for specific surveys</li>
    //     <li>Providing you with information about rewards/gifts given to you by Panel Reward as a result of your responding to survey requests</li>
    //     <li>Processing the answers to surveys and summarizing the results</li>
    //     <li>Undertaking research surveys on behalf of third parties</li>
    //     <li>Keeping you informed and obtain your views of Panel Reward’s products, services and activities</li>
    //     <li>The administration of your membership of Panel Reward (if you have registered as a member)</li>
    //     <li>Statutory and regulatory compliance</li>
    //     <li>Processing any enquiries raised by you and other communications initiated by you in relation to your dealings with Panel Reward and any services provided by Panel Reward, or its legal entity</li>
    //     <li>We may also use the information you provide in aggregate form for internal business purposes, such as generating statistics and developing marketing plans</li>
    //     <li>We may share or transfer such non-personally identifiable information with or to our affiliates, licensees, agents and partners by pseudonymising your data</li>
    //     <li>In addition, we may disclose any information, including personally identifiable information, we deem necessary, in our sole discretion, to comply with any applicable law, regulation, legal proceeding or governmental request</li>
    //     <li>Please be informed that for any and all Personal Information that is gets collected or captured by the meter, we shall aggregate / collate it in a non-personally identifiable manner so that the same would not remain the Personal Information. We understand the importance of confidentiality of the Personal Information and confirm that the Personal Information will not be shared with any person in a personally identifiable manner without your specific approval.</li>
    //     <li>We only retain the personal data collected from you as long as your account is active or otherwise for a limited period of time as long as we need to fulfil the pruposes for which we have initially collected it, unless otherwise required by law. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements</li>
    //     <li>We may, if necessary process your information on server located outside your country of residence. Currently we are using Microsoft Azure to secure and safely store your information</li>
    //     <li>We may transfer all or part of its business or assets to a third party after receiving your consent. Your information or Personal Information may be a part of such business or assets. Such transfer of information may be outside your country of residence also.</li>
    //     </ul>
    //     <h4 className = "aboutHeading1">Privacy Principles:  </h4>
    //     <p style={{textAlign:"left"}}>Panel Reward complies with the privacy requirements as set forth by the European Union's data protection regulation GDPR (General data protection regulation) regarding the collection, use and retention of personal information </p>
    //   </div>
    // </div>
   );
  }
}

const  mapStateToProps =(state) =>{
  return {
    authenticated: state.login.authenticated,
  }
}

export default connect(mapStateToProps)(PrivacyPolicy);
