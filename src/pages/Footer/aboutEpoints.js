import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import ReactDOM from 'react-dom'
import facebook from '../../components/Image/facebook-logo.svg';
import google from '../../components/Image/google-logo.svg';
import twitter from '../../components/Image/twitter.svg';
import linked from '../../components/Image/linkedin.svg';
import logo from '../../images/panel reward.png';
import paypal from '../../images/PaypalLogo.png';
import cancel from '../../icons/cancel.png';

class AboutEpoints extends Component {
  constructor(props){
    super(props)
    this.state = {

    }
  }

  componentDidMount(){
    document.title = "Panel Reward-AboutEpoints"
  }
  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("aboutbtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("aboutbtn").style.display = "block";
  }
  
  render() {
    let hearderDiv = "";
    if(!this.props.authenticated){
             let userId=sessionStorage.getItem("user_id");
              if(userId){
               
             }else {
              hearderDiv = <div id="footerheader">
              <div id="footerlogo"><img src={logo} height="50px" width="200px" /></div>
              <div id="footericons">
                  <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
                  <a href="javascript:void(0)" onClick={() => this.openNav()}> <button id="aboutbtn">&#9776;</button></a>
                </div>
            </div>
             }
      }

   return (
    <div id="footercontainer">
        {hearderDiv}
      <div id="epointstitle">
      <h1>About E-Points</h1>
          <p>Panel Reward is a diverse community of over 2.7 million consumers from world’s fastest growing economies.</p>
      </div>
      <div id="epointscontent">
            <em></em>
            <h1>E-Points</h1>
            <p>Once registered to PANEL REWARD Surveys, you will be sent surveys for you to respond to. 
              You will accumulate EPoints depending on the contents, number of questions and length of each survey. 
              Accumulated EPoints are redeemable for rewards such as online shopping gift coupons. </p>
            <p style={{fontSize:'1.25rem'}}><b>50 E-Point =  1 USD .</b></p>
            <em></em>
            <h2>About Rewards</h2>
            <h4>Redeem E-Points :</h4>
              <p>Currently gift coupons of various amounts are available for you to redeem. The minimum required for redemption is more then 500 EPoints.</p>
              <p style={{fontSize:'1.25rem'}}><b>Online Gift Coupon (Starting from Rs. 10).</b></p>

            <em></em>
            <h2>About Cash Conversion</h2>
            <h4>Transfer to Paypal Account :</h4>
            <p className = "para1">You can transfer your accumulated EPoints to your Paypal account. Minimum required is more then 500 EPoints.</p>
            <p style={{fontSize:'1.25rem'}}>*Your account must match your registered E-mail address	</p>
             <p> <img src={paypal}/></p>
      </div>
      <div id="myNav" class="overlay">
              <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn"><img src={cancel} height="18px" width="18px" className="link" /></button></a>
              <div class="overlay-content">
              <Link to='/aboutUs'>About Us</Link>
              <Link to='/register'>New Member Registration</Link>
              <Link to='/Login'>Login</Link>
              </div>
          </div>  
    </div>




    // <div className = "aboutEpointsPage" > 
    //   <div className = "aboutEpoints"> <h2>About EPoints</h2></div>
    //   <div className = "aboutEpointsp">
    //     <p className = "para1">Once registered to PANEL REWARD Surveys, you will be sent surveys for you to respond to. 
    //     You will accumulate EPoints depending on the contents, number of questions and length of each survey. 
    //     Accumulated EPoints are redeemable for rewards such as online shopping gift coupons. </p>
    //     <p className = "para1">50 EPoint =  1 USD .	</p><br/>
        
    //     <h3 style={{textAlign:"center"}}>About Rewards</h3>
    //     <h4>Redeem EPoints :</h4>
    //     <p className = "para1">Currently gift coupons of various amounts are available for you to redeem. The minimum required for redemption is more then 500 EPoints.</p>
    //     <h5><b>Online Gift Coupon (Starting from Rs. 10)</b></h5><br/>
        
    //     <h3 style={{textAlign:"center"}}>About Cash Conversion</h3>
    //     <h4>Transfer to Paypal Account :</h4>
    //     <p className = "para1">You can transfer your accumulated EPoints to your Paypal account. Minimum required is more then 500 EPoints.</p>
    //     <p style={{color:"red", textAlign:"left"}}>*Your account must match your registered E-mail address	</p>

    //     <div class="cashRow" style={{borderStyle: "solid", borderWidth: "1px", borderColor: "#57749D" ,height: "80px",padding:"10px"}}>
    //       <div class="cashColumn" style={{  width: "30%"  }}>
    //       <img src={require("./PaypalLogo.png")}  />
    //       </div>
    //       <div class="cashColumn"style={{  width: "50%" ,paddingTop:"5px" }} >
    //       <span>Paytm (Starting from 10 USD)</span><br/>
    //       <span style={{color:"red"}}>Redemption available from more then 500  EPoints</span>
    //       </div>
    //     </div>
    //   </div>
    // </div>
   );
  }
}

const  mapStateToProps =(state) =>{
  return {
    authenticated: state.login.authenticated,
  }
}

export default connect(mapStateToProps)(AboutEpoints);
