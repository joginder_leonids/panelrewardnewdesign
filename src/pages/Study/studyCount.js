import React, { Component } from 'react';
import { connect } from 'react-redux';
import { studyCountData, saveStudyParticipate } from '../../actions/study';
import { getGiftcouponByUser}  from '../../actions/reward';
import { Link } from 'react-router-dom';
import './styleCount.css';
import CircularProgressbar from 'react-circular-progressbar';
import { Bar, Line } from 'react-chartjs-2';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import email from './email.svg';
import pencil from '../../icons/pencil.png';
import thumb from '../../icons/thumb-up.png';
import approved from '../../icons/checkmark.png';
import coupon from '../../icons/gift-card.png';
import process from '../../icons/process.png';
import reject from '../../icons/cross.png';
import failure from '../../icons/failure.png';
import linkbtn from '../../icons/link-symbol.png';
import $ from 'jquery';
import dotspinner from '../../icons/spinner-of-dots.png';
import emptytranhistory from '../../images/empty transaction.png';
import emptystudy from '../../images/empty study.png';
import clickhereimg from '../../images/clickhere.gif';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardText,
  CardBody,
  CardFooter,
  CardHeader,
  CardGroup,
  CardTitle,
  Col, Breadcrumb, BreadcrumbItem,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Container,
  Table,
} from 'reactstrap';



class StudyCount extends Component {
  constructor(props) {
    super(props)

    this.state = {
      totalStudy: "",
      startStudy: "",
      completeStudy: "",
      totalRewards: "",
      totalStudyPer: 0,
      startStudyPer: 0,
      completeStudyPer: 0,
      totalRewardsPer: 0,
      showButton: true,
      profileComplete: 0,
      studiesData : [],
      transactionData : [],
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  componentWillMount() {
    let userId = sessionStorage.getItem("user_id");
    this.props.dispatch(studyCountData(userId));
    //document.getElementById("dahsboardmenu").style.backgroundColor="blue";
  }

  componentWillReceiveProps(Data) {
    if (Data.StudyCountData) {

      let ts = parseInt(Data.StudyCountData.totalStudy);
      let st = parseInt(Data.StudyCountData.startStudy);
      let cs = parseInt(Data.StudyCountData.completeStudy);
      let tr = parseInt(Data.StudyCountData.totalRewards);

      let tsp = Math.round(ts*100/ts);
     
      if(tsp>100){
        tsp = 100;
      }else if(tsp<0){
        tsp = 0;
      }

      let stp = Math.round(st*100/ts);
      if(stp>100){
        stp = 100;
      }else if(stp<0){
        stp = 0;
      }

      let csp = Math.round(cs*100/ts);
      if(csp>100){
        csp = 100;
      }else if(csp<0){
        csp = 0;
      }

      let trp = Math.round(tr*100/500);
      if(trp>100){
        trp = 100;
      }else if(trp<0){
        trp = 0;
      }

      this.setState({
        totalStudy: Data.StudyCountData.totalStudy,
        startStudy: Data.StudyCountData.startStudy,
        completeStudy: Data.StudyCountData.completeStudy,
        totalRewards: Data.StudyCountData.totalRewards,
        totalStudyPer: tsp,
        startStudyPer: stp,
        completeStudyPer: csp,
        totalRewardsPer: trp,
      });
    }

    if(Data.StudyCountRelativeData){
      if(Data.StudyCountRelativeData.totalQuestionByCat != null && Data.StudyCountRelativeData.totalQuestionByCat != undefined ){
        //let profileComp = Math.round(parseInt(Data.StudyCountRelativeData.totalQuestionByCat.totalUserAns)* 100 / parseInt(Data.StudyCountRelativeData.totalQuestionByCat.totalQuestion));
        
        this.setState({
          profileComplete : parseInt(Data.StudyCountRelativeData.totalQuestionByCat),
          studiesData : Data.StudyCountRelativeData.StudiesData
        });
      }

      if(Data.StudyCountRelativeData.userTransactionData){
        this.setState({
          transactionData : Data.StudyCountRelativeData.userTransactionData
        });
      }
    }
  }
  

participate(data) {
  let userId=sessionStorage.getItem("user_id");
  //let url ="http://localhost:4000/CompaneyParticipated/s:"+ data._id + "/u:"+ userId ;

  //let url ="http://192.169.139.127:4000/CompaneyParticipated/s:"+ data._id + "/u:"+ userId ;
  //let url =  data.study_url +"/s:"+ data._id + "#u:"+ userId ;
  let url="";
  if(data.study_url.match("{uid}")){
    url =  data.study_url.split("{uid}")[0] +"$SID$"+ data._id + "@UID@"+ userId ;
  }else{
    url =  data.study_url +"$SID$"+ data._id + "@UID@"+ userId ;
  }

  window.open(url);
  let participateData ={};
  participateData.studyId = data._id;
  participateData.statusId = 0;
    //this.props.saveStudyParticipate(participateData);
    this.props.dispatch(saveStudyParticipate( participateData, userId));
}
getGiftcouponByUser(data) {
  //this.props.getGiftcouponByUser(data);
  
  this.props.dispatch(getGiftcouponByUser(data.assignGiftcouponId));
  this.props.history.push('/transactionHistory');
}

  render() {
    // if (!this.props.authenticated) {
    //   let access_token = sessionStorage.getItem("token");
    //   let userId = sessionStorage.getItem("user_id");
    //   if (userId) {
    //     this.props.dispatch(studyCountData(userId));
    //   } else {
    //     //alert("Please login.");
    //     this.props.history.push("login");
    //   }
    // }
    let studyView = "";
    let transactionView = "";

    let redeemButton = "";

    if (this.state.totalRewards > 500) {
      //redeemButton = <button ><Link to="/collectReward" className="collect">Redeem</Link> </button>
      redeemButton =

      <div>
        <div style={{width:"40px", float:"left"}}><br/><br/></div>
        <div className="studieCountText"><div style={{fontWeight: "600", fontSize: "25px" }} >{this.state.totalRewards} </div>
        <br/>
        <Link to="/collectReward"><button id="reedmbtn">Redeem</button></Link>
        </div>
      </div>
    } else {
      redeemButton =
        <div>
          <div style={{width:"40px", float:"left"}}><CircularProgressbar className = "totalRewardProgressbar" percentage={this.state.totalRewardsPer} text={`${this.state.totalRewardsPer}%`} /><br/><br/></div>
            <div className="studieCountText"><div style={{fontWeight: "600", fontSize: "25px" }} >{this.state.totalRewards} </div>Earned Rewards</div>
        </div>
    }

    let emptyStudyimg="";
    let studyItem="";      

      if (this.state.studiesData && this.state.studiesData.length>0) {
        let studies = this.state.studiesData;
        studyView = Object.keys(studies).map((item, index) => {
          studyItem = studies[item];
          let showParticipateButton = "";
          if (studyItem.is_live == true) {
            studyItem.isLive = "right";
          } else {
            studyItem.isLive = "wrong";
          }
          if (studyItem.completionStatus === "0") {
            studyItem.completionStatuss = <Badge color="info" style={{ width: '50%' }}>Start</Badge>;
            showParticipateButton = <button id="participatebtns" disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participant Participated</span></button>
            this.state.showButton = true;
          } else if (studyItem.completionStatus === "1") {
            studyItem.completionStatuss = <Badge color="success" style={{ width: '50%' }}>Completed</Badge>;
            showParticipateButton = <button id="participatebtns" disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participant Participated</span></button>
            this.state.showButton = true;
          } else if (studyItem.completionStatus === "2") {
            this.state.showButton = true;
            showParticipateButton = <button id="participatebtns" disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participant Participated</span></button>  
            studyItem.completionStatuss = <Badge color="warning" style={{ width: '50%' }}>QuotaFail</Badge>;
          } else if (studyItem.completionStatus === "3") {
            this.state.showButton = true;
            showParticipateButton = <button id="participatebtns" disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participant Participated</span></button>
            studyItem.completionStatuss = <Badge color="danger" style={{ width: '50%' }}>Fail</Badge>;
          } else {
            studyItem.completionStatus = "";
            this.state.showButton = false;
            showParticipateButton = <button id="participatebtn" onClick={this.participate.bind(this, studyItem)} disabled={this.state.showButton}><img src={linkbtn} height="15px" width="15px" /><span>Participate The Survey</span></button>
          }
          let studyd = studyItem.created_at;
          let correctdate = studyd.split('T');
          return <tr className="studyTr" key={index}>
            <td className="studyTd">{index + 1}</td>
            <td className="studyTd">{correctdate[0]}</td>
            <td className="studyTd">
              {/* <Button color="primary" style={{ padding: "0.2rem 0.2rem", backgroundColor: "#1985AC" }} type="button" onClick={this.participate.bind(this, studyItem)} disabled={this.state.showButton} > Participate </Button> */}
              {showParticipateButton}
            </td>
            <td className="studyTd">{studyItem.cpi}</td> 
            
            <td className="studyTd">{studyItem.completionStatuss}</td>
          </tr>
        });
      }else{
        emptyStudyimg= <div id="emptytableimg"><img src={emptystudy} height="100%" width="100%" /></div>
      }
      let emptytransimg="";
      let SNo = 0;
      let transactionItem ="";
      if (this.state.transactionData && this.state.transactionData.length>0) {
        let transactions = this.state.transactionData;
        transactionView = Object.keys(transactions).map((item, index) => {
          transactionItem = transactions[item];
  
          let viewAction = "";
          let ViewPaymentStatus = "";
          if (transactionItem.paymentType === "GiftCouponOnline" && transactionItem.paymentStatus === "success") {
           // viewAction = <Button type="button" style={{ padding: "0.2rem 0.3rem", width: '72%' }} color="success">ViewCoupon</Button>
            viewAction = <button id="coupon"  onClick={this.getGiftcouponByUser.bind(this, transactionItem)}><img src={coupon} height="15px" width="15px" /><span>View Coupon</span></button>
            ViewPaymentStatus = <button id="complete"><img src={thumb} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
          }else if(transactionItem.paymentStatus === "success"){
            viewAction = <button id="complete"><img src={thumb} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
            ViewPaymentStatus = <button id="complete"><img src={thumb} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
          } else if (transactionItem.paymentStatus === "pending") {
            viewAction = <div><button id="pending"><img src={dotspinner} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button></div>
            ViewPaymentStatus = <div><button id="pending"><img src={dotspinner} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button></div>
            //viewAction = <div><Button color="info" style={{ padding: "0.3rem 0.3rem", cursor: 'default', width: '72%' }}>{transactionItem.paymentStatus.toUpperCase()}</Button></div>
          } else if (transactionItem.paymentStatus === "approved") {
            //viewAction = <div><Button color="primary" style={{ padding: "0.2rem 0.3rem", cursor: 'default', width: '72%' }}>{transactionItem.paymentStatus.toUpperCase()}</Button></div>
            viewAction = <button id="approved"><img src={approved} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
            ViewPaymentStatus = <button id="approved"><img src={approved} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
          } else if (transactionItem.paymentStatus === "processing") {
            //viewAction = <div><Button style={{ padding: "0.3rem 0.3rem", backgroundColor: "#021a2f", cursor: 'default', width: '72%' }}>{transactionItem.paymentStatus.toUpperCase()}</Button></div>
            viewAction = <button id="processing"><img src={process} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
            ViewPaymentStatus = <button id="processing"><img src={process} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
          } else {
            //viewAction = <div><Button color='danger' style={{ padding: "0.2rem 0.2rem", cursor: 'default', width: '72%' }}>{transactionItem.paymentStatus.toUpperCase()}</Button></div>
            viewAction = <button id="reject"><img src={reject} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
            ViewPaymentStatus = <button id="reject"><img src={reject} height="15px" width="15px" /><span>{transactionItem.paymentStatus.toUpperCase()}</span></button>
          }
  
          SNo = SNo + 1;
          return <tr  key={index}>
            <td >{SNo}</td>
            <td>{transactionItem.totalAmount + " USD"}</td>
            <td >{transactionItem.totalPoints}</td>
            <td className="txnTD">{transactionItem.paymentType}</td>
            <td className="txnTD">{ViewPaymentStatus}</td>
            <td className="txnTD">{transactionItem.updatedAt.split("T")[0] + " " + transactionItem.updatedAt.split("T")[1].split(".")[0]}</td>
            <td className="txnTD">{viewAction}</td>
          </tr>
        });
      }
      else{
        emptytransimg= <div id="emptytableimg"><img src={emptytranhistory} height="100%" width="100%" /></div>
      }

      if(this.props.getAgainStudiesData){
        let userId = sessionStorage.getItem("user_id");
        this.props.dispatch(studyCountData(userId));
      }

      // let profileCompleteText = "";
      // let profileIncomplete="";
      //   if(this.state.profileComplete >= 100){
      //     profileCompleteText =  <p>Your Demo Graphic is completed.</p>
      //   }else{
      //     profileIncomplete =  <p>Your Demo Graphic is not complete. Please complete this section.
      //       <Link to="/profileDashboard">Click Here</Link>
      //     </p>
      //   }
      let showProfile="";
      if(this.state.profileComplete >= 100){
      showProfile= <div  style={{textAlign:"center"}}>
        <div style={{textAlign:"center",paddingTop:"30px"}}><CircularProgressbar className = "profileCompleteProgressbar" percentage={this.state.profileComplete} text={`${this.state.profileComplete}%`} /><br/><br/></div>
        <p>Your profile is completed.</p>
        </div>
      }else{
        showProfile= <div  style={{textAlign:"center",paddingTop:"10px"}}>
        <p>Your profile is not complete. Please complete this section.</p>
        <div style={{textAlign:"center",paddingTop:"30px"}}><CircularProgressbar className = "profileCompleteProgressbar" percentage={this.state.profileComplete} text={`${this.state.profileComplete}%`} /><br/><br/></div>
        <Link to="/profileDashboard"><img src={clickhereimg} height="80px" width="200px" /></Link>
        </div>
      }

    return (
      
        <div id="studycountmain">
     
        <div id="headerstudy">
          <h3>Dashboard</h3>
          <ol id="breadcrumb">
            <li>Home</li> &nbsp;/&nbsp;
            <li style={{ opacity: '0.8' }}>Dashboard</li>
          </ol>
        </div>
        <div id="studycards">
          <div id="firstlayercards">
            <div id="firstlayercard1">
            <div style={{width:"40px", float:"left"}}><CircularProgressbar className = "totalStudyProgressbar" percentage={this.state.totalStudyPer} text={`${this.state.totalStudyPer}%`} /><br/><br/></div>
          <div className="studieCountText"><div style={{fontWeight: "500", fontSize: "25px" }} >{this.state.totalStudy} </div>Total Study</div>
            </div>
            <div id="firstlayercard2">
            <div style={{width:"40px", float:"left"}}><CircularProgressbar className = "startStudyProgressbar" percentage={this.state.startStudyPer} text={`${this.state.startStudyPer}%`} /><br/><br/></div>
            <div className="studieCountText"><div style={{fontWeight: "600", fontSize: "25px" }} >{this.state.startStudy} </div>Started Study</div>
            </div>
          </div>
          <div id="secondlayercards">
          <div id="secondlayercard1">
          <div style={{width:"40px", float:"left"}}><CircularProgressbar className = "completeStudyProgressbar" percentage={this.state.completeStudyPer} text={`${this.state.completeStudyPer}%`} /><br/><br/></div>
            <div className="studieCountText"><div style={{fontWeight: "600", fontSize: "25px" }} >{this.state.completeStudy} </div>Completed Study</div>
          </div>
            <div id="secondlayercard2">
            {redeemButton}
            </div>
          </div>
          {/* <div className="studies" id="totalstudy"> 
          <div style={{width:"40px", float:"left"}}><CircularProgressbar className = "totalStudyProgressbar" percentage={this.state.totalStudyPer} text={`${this.state.totalStudyPer}%`} /><br/><br/></div>
          <div className="studieCountText"><div style={{fontWeight: "600", fontSize: "25px" }} >{this.state.totalStudy} </div>Total Study</div>
          </div>

          <div className="studies" id="startedstudy">
            <div style={{width:"40px", float:"left"}}><CircularProgressbar className = "startStudyProgressbar" percentage={this.state.startStudyPer} text={`${this.state.startStudyPer}%`} /><br/><br/></div>
            <div className="studieCountText"><div style={{fontWeight: "600", fontSize: "25px" }} >{this.state.startStudy} </div>Started Study</div>
          </div>
          <div className="studies" id="completedstudy">
            <div style={{width:"40px", float:"left"}}><CircularProgressbar className = "completeStudyProgressbar" percentage={this.state.completeStudyPer} text={`${this.state.completeStudyPer}%`} /><br/><br/></div>
            <div className="studieCountText"><div style={{fontWeight: "600", fontSize: "25px" }} >{this.state.completeStudy} </div>Completed Study</div>

          </div>
          <div className="studies" id="earnedstudy">
            {redeemButton}
         </div> */}
        </div>
        <div id="profilestudy">
         <div id="profile">
            <h3>Profile Complete</h3>
            <hr className="profilestudyhr"/>
            {showProfile}
            {/* <div style={{textAlign:"center",paddingTop:"30px"}}><CircularProgressbar className = "profileCompleteProgressbar" percentage={this.state.profileComplete} text={`${this.state.profileComplete}%`} /><br/><br/></div>
            <div className="profilestudyCompleteText">
               <h5>Demo Graphic</h5> 
              {profileCompleteText}
            </div> */}
          </div>
          
          <div id="studytable">
              <h3>Study List</h3>
             <table id="mystudyscroll">
              <tr>
                <th>S.NO</th>
                <th>DATE</th>
                <th>STUDY URL</th>
                <th>REWARD POINTS</th>
                <th>PARTICIPATION STATUS</th>
              </tr>
              <tbody>
                {studyView}
                 </tbody>
            </table>
              {emptyStudyimg}
          </div>
        </div>
        <div id="transhistory">
        <h3>Transaction History</h3>
             <table id="transtable">
              <tr>
                <th>S.NO</th>
                <th>AMOUNT</th>
                <th>POINTS</th>
                <th>PAYMENT TYPE</th>
                <th>PAYMENT STATUS</th>
                <th>DATE & TIME</th>
                <th>ACTION</th>
              </tr>
              <tbody>
                {transactionView}
              </tbody>
            </table>
            {emptytransimg}
            </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    StudyCountData : state.study.StudyCountData,
    StudyCountRelativeData : state.study.StudyCountRelativeData,
    success : state.study.success,
    getAgainStudiesData : state.study.getAgainStudiesData,
  }
}

export default connect(mapStateToProps)(StudyCount);
