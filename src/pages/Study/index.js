import React, { Component } from 'react';
import StudyList from '../../components/Study';
import {connect} from 'react-redux';
import {fetchStudy, saveStudyParticipate, fetchStudyCountData} from '../../actions/study';

class Study extends Component {
  constructor(props){
    super(props)
    this.state = {
    }
    this.saveStudyParticipate = this.saveStudyParticipate.bind(this);
    this.getStudiesByPageNo = this.getStudiesByPageNo.bind(this);
  }

  componentWillMount() {
 let access_token=sessionStorage.getItem("token");
    let userId=sessionStorage.getItem("user_id");
     if(userId){
      this.props.dispatch(fetchStudyCountData(userId));
      this.props.dispatch(fetchStudy(userId, access_token, 1));
    }
  }

  saveStudyParticipate(data){
    let userId=sessionStorage.getItem("user_id");
     if(userId){
      this.props.dispatch(saveStudyParticipate( data,userId));
    }
  }

  getStudiesByPageNo(pageNo){
   
    let access_token=sessionStorage.getItem("token");
    let userId=sessionStorage.getItem("user_id");
     if(userId){
      this.props.dispatch(fetchStudyCountData(userId));
      this.props.dispatch(fetchStudy(userId, access_token, pageNo));
    }
  }

  render() {
  //   if(!this.props.authenticated){
  //     let access_token=sessionStorage.getItem("token");
  //        let userId=sessionStorage.getItem("user_id");
  //         if(userId){
  //          this.props.dispatch(fetchStudy(userId, access_token));
  //        }else {
  //         // alert("Please login.");
  //          this.props.history.push("/login");
  //        }
  // }

  

   let StudyListHtml ="";
   if(this.props.studies){
     StudyListHtml =   <StudyList
                        getStudyData = {this.getStudyData}
                        studies  ={this.props.studies}
                        saveStudyParticipate = {this.saveStudyParticipate}
                        StudyTotalCountData = {this.props.StudyTotalCountData}
                        totalStudies = {this.props.totalStudies}
                        getStudiesByPageNo = {this.getStudiesByPageNo}
                        getAgainStudiesData = {this.props.getAgainStudiesData}
                        />
   }

   return (<div>
          {StudyListHtml}
           </div>

   );
  }
}

const  mapStateToProps =(state) =>{
  return {
        studies : state.study.studies,
        authenticated : state.login.authenticated,
        totalStudies: state.study.totalStudies,
        StudyTotalCountData: state.study.StudyTotalCountData,
        getAgainStudiesData : state.study.getAgainStudiesData
  }
}

export default connect(mapStateToProps)(Study);
