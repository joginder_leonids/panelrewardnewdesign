import React, { Component } from 'react';
import './style.css';
import { Link } from 'react-router-dom';
import { varifyToken, tokenMsgFail } from '../../actions/registration';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { connect } from 'react-redux';
import {
  Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row,
  CardHeader, CardText
} from 'reactstrap';
import panelimg from '../../components/Image/info_graphic_1.svg';
import panelogo from '../../components/Image/panel reward.png';
import facebook from '../../components/Image/facebook-logo.svg';
import google from '../../components/Image/google-logo.svg';
import twitter from '../../components/Image/twitter.svg';
import linked from '../../components/Image/linkedin.svg';
import cancel from '../../icons/cancel.png';
import appleicon from '../../icons/apple.png';
import androidicon from '../../icons/android-logo.png';
import Modal from 'react-awesome-modal';
class RegisterToken extends Component {
  constructor(props) {
    super(props)

    this.state = {
      token: "",
      hideTokenInput: false,
      visible: false,
      notification: ""
    }
    this.tokenChange = this.tokenChange.bind(this);
    this.varifyToken = this.varifyToken.bind(this);
    this.goLoginPage = this.goLoginPage.bind(this);
  }
  closeModal() {
    this.setState({
      visible: false
    });
  }

  tokenChange(e) {
    if (e.target.value === "") {
      document.getElementById("tokenerror").style.display = "block";
    } else {
      document.getElementById("tokenerror").style.display = "none";

    }
    this.setState({ token: e.target.value });
  }

  goLoginPage(e) {
    e.preventDefault();
    //let Url = 'https://testdesign.Panel Reward.com/login';
    //window.open(Url, '_self');
    this.props.history.push("/Login");
  }

  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("myBtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("myBtn").style.display = "block";
  }
  handleValidation(){
    let valid= true;
    if (this.state.token === "") {
      valid=false;
      document.getElementById("tokenerror").style.display = "block";
    return valid;
    }else{
      return valid;
    }
  }
  varifyToken(e) {
    e.preventDefault();
       if (this.handleValidation()) {
      this.props.dispatch(tokenMsgFail());
      let Data = {}
      let userId = window.location.pathname.split("/");
      Data.id = userId[2];
      Data.token = this.state.token;
      this.props.dispatch(varifyToken(Data));
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success === true) {
     
      this.setState({ 
        hideTokenInput: true, 
        visible:true,
        notification: nextProps.resetMessageResponse
      });
    }
    else if(nextProps.resetMessageResponse){
       this.setState({
         visible:true,
         notification:nextProps.resetMessageResponse
       });
    }
  }

  render() {
    let msg = "";
    let msgData = "";
    if (this.props.success === true) {
   
      msg = this.props.resetMessageResponse;
      msgData = <div>
        {msg}
      </div>
    } else {
      if (this.props.resetMessageResponse) {
        msg = this.props.resetMessageResponse;
        msgData = <div className="tPassMsgErr">{msg}</div>
      }
    }



    let showDivBtnToken = "";
    let p = "";
    if (this.props.success === true) {
      //  this.setState({visible:true});
      //alert("fldkjg");
      // p = <p>Your code has varified succesfully. Please Login</p>
      showDivBtnToken = <form onSubmit = {this.goLoginPage} ><div>
        <button id="tokenverified"  type="submit">Login</button>
      </div></form>
    } else {
      p = <p>Check your Registered email id and fill verification code</p>
      showDivBtnToken = <form onSubmit ={this.varifyToken}><div>
        <input type="text" placeholder="Enter code" id="inputoken" onChange={this.tokenChange} />
        <div id="tokenerror">
          <div id="tokentquote">
            <blockquote>
              Required
            </blockquote>
          </div>
        </div>
        <div className="loading">{
          this.props.requestStarted ?
            <div className="preloader"><LoadingDots interval={100} dots={5} /></div>
            :
            false
        }</div><br/>
        {/* {msgData}<br />     */}
          <button id="clickbuttontoken" type="submit" >Submit</button>
      </div> </form>
    }

    let showModelBtn ="";
    if (this.props.success === true) {
      showModelBtn = <div>
      <button id="regmodalclose" onClick={this.goLoginPage}>Login</button>
    </div>
    }else{
      if (this.props.resetMessageResponse) {
      showModelBtn = <button id="regmodalclose" onClick={() => this.closeModal()}> close</button>
      }
    }

    return (
      <div className="wrapper">
             <Modal
          visible={this.state.visible}
          width="400"
          height="300"
          effect="fadeInDown"
          // onClickAway={() => this.closeModal()}
        >
          <div className="text-center" style={{ paddingTop: '20%' }}>
            <h1>Alert!</h1>
            <p>{this.state.notification}</p>
              {showModelBtn}
          </div>
        </Modal>
       
        <a href="javascript:void(0)" onClick={() => this.openNav()}> <button id="myBtn">&#9776;</button></a>
        <div className="column">
          <div id="icons">

            <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
            <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
            <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
            <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
      </div>
          <div className="logo"><img src={panelogo} height="50px" height="50px" /></div>
          <div className="subcolumn">
          <div id="mySidenav" class="sidenav">
              <a href="#" id="ioslink">
                <div id="iosleft"><span>Get it on IOS</span></div>
                <div id="iosright"><img src={appleicon} height="35px" width="35px" /></div>
                </a>
              <a href="https://play.google.com/store/apps/details?id=in.leonids.panel&hl=en" target="_blank" id="androidlink">
                <div id="andrleft"><span>Get it on Android</span></div>
                <div id="andright"><img src={androidicon} height="35px" width="35px" /></div>
                </a>
              </div>
            <div className="image">
              <img src={panelimg} width="400px" />
              <h3 className="heading">Welcome to Panel Reward</h3>
              <p className="title" style={{ fontWeight: '700' }}>Data Collection service</p>
              <p className="paragraph">GNN is a leading service provider in the field of conducting professional market research services be it quantitative & qualitative. We hold expertise in providing both primary and secondary research based services. It helps us to provide improvised marketing research study designs & analytical expertise that help us in successfully meeting client’s customized research needs.</p>
              <p className="title" style={{ fontWeight: '700' }}>Data Analysis service</p>
              <p className="paragraph">We provide advanced analytics services to help companies drill deeper into the data and understand the subterranean data linkages which drive customer behavior. Our Analytics services help companies to take data driven decisions and hone their marketing programs.</p>
              <Link to='/register'> <button className="regbtn">Register Now </button></Link>
            </div>
          </div>


        </div>
        <div class="column alt">
          <div id="menubtn">
          </div>
          <div className="form">
          <div id="clickbuttonhead">Verification code Panel Reward</div>
            {p}
            {/* <p style={{ fontSize: '1.125rem', width: '100%' }}>Enter your email id</p> */}
            <div style={{ position: 'relative' }}>
              {showDivBtnToken}
              {/* <input type="text" placeholder="Enter E-Mail" class="inputtypefrgt"  onChange={this.emailChange}/> */}

            </div>

          </div>
          <div id="myNav" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn"><img src={cancel} height="18px" width="18px" className="link" /></button></a>
            <div class="overlay-content">
              <Link to='/aboutUs'>About Us</Link>
              <Link to='/register'>New Member Registration</Link>
              <Link to='/Login'>Login</Link>
            </div>
          </div>
        </div>
      </div>


      // <div className="app">
      //    <Modal
      //     visible={this.state.visible}
      //     width="400"
      //     height="300"
      //     effect="fadeInDown"
      //     onClickAway={() => this.closeModal()}
      //   >
      //     <div className="text-center" style={{ paddingTop: '20%' }}>
      //       <h1>Alert!</h1>
      //       <p>{this.state.notification}</p>
      //       <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
      //     </div>
      //   </Modal>

      //   <h1 className="text-center" style={{ paddingTop: '6%' }}>Leonids Voice</h1>
      //         <Card style={{ paddingRight:'5%',marginTop:'4%',marginLeft:'35%',marginRight:'35%', paddingBottom: '2%' }}>
      //       <CardBody>
      //         <Form onSubmit={this.login}>
      //           <h3>Token Verification</h3>
      //           {p}<br/><br/>
      //           {showDivBtnToken}

      //           {/* <Row>
      //             <Col>
      //               {showDivBtnToken}
      //             </Col>
      //           </Row> */}
      //         </Form>
      //       </CardBody>
      //     </Card>




    );
  }
}

const mapStateToProps = (state) => {
  return {
    resetMessageResponse: state.registration.message,
    success: state.registration.success,
    requestStarted: state.common.requestStarted
  }
}

export default connect(mapStateToProps)(RegisterToken);
