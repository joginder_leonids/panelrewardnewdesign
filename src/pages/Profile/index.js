import React, { Component } from 'react';
import ProfileView from '../../components/Profile';
import { fetchProfile, updateProfile, setProfileResNull } from '../../actions/profile';
import { connect } from 'react-redux';
import Modal from 'react-awesome-modal';
class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      visible: false,
      notification: ""
    }
    this.updateProfile = this.updateProfile.bind(this);
    this.setProfileResNull = this.setProfileResNull.bind(this);
  }

  closeModal() {
    this.setState({
      visible: false
    });
  }

  openModal() {
    this.setState({
        visible : true
    });
}

setProfileResNull(){
  this.props.dispatch(setProfileResNull());
}

  componentWillMount() {
    let access_token = sessionStorage.getItem("token");
    let userId = sessionStorage.getItem("user_id");
    if (userId) {
      this.props.dispatch(fetchProfile(userId, access_token));
    }
  }

  updateProfile(data) {
    let access_token = sessionStorage.getItem("token");
    let userId = sessionStorage.getItem("user_id");
    if (userId) {
      this.props.dispatch(updateProfile(userId, access_token, data));
    }
  }

  render() {
    if (!this.props.authenticated) {
      let access_token = sessionStorage.getItem("token");
      let userId = sessionStorage.getItem("user_id");
      if (userId) {
        //   this.props.dispatch(fetchStudy(userId, access_token));
      } else {
            //alert("Please login");

        this.props.history.push("/Login");
      }
    }

    let profileView = "";
    if (this.props.userProfile) {
        profileView = <ProfileView
        userProfile={this.props.userProfile}
        updateProfile={this.updateProfile}
        userProfileUpdate = {this.props.userProfileUpdate}
        setProfileResNull = {this.setProfileResNull}
      />
    }

    return (<div>

      <Modal
        visible={this.state.visible}
        width="400"
        height="300"
        effect="fadeInDown"
        onClickAway={() => this.closeModal()}
      >
        <div className="text-center" style={{ paddingTop: '20%' }}>
          <h1>Alert!</h1>
          <p>{this.state.notification}</p>
          <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
        </div>
      </Modal>
      {profileView}
    </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authenticated: state.login.authenticated,
    userProfile: state.profile.userProfile,
    userProfileUpdate : state.profile.userProfileUpdate,
  }
}

export default connect(mapStateToProps)(Profile);
