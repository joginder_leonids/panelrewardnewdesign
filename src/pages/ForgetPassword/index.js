import React, { Component } from 'react';
import './style.css';
import { forgetPasswordMail, removeForgetPassProps } from '../../actions/logIn';
import { connect } from 'react-redux';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { Link } from 'react-router-dom';
import panelimg from '../../components/Image/info_graphic_1.svg';
import panelogo from '../../components/Image/panel reward.png';
import facebook from '../../components/Image/facebook-logo.svg';
import google from '../../components/Image/google-logo.svg';
import twitter from '../../components/Image/twitter.svg';
import linked from '../../components/Image/linkedin.svg';
import cancel from '../../icons/cancel.png';
import appleicon from '../../icons/apple.png';
import androidicon from '../../icons/android-logo.png';

import {
  Card, Container, CardBody, InputGroup,
  CardGroup, CardSubtitle, InputGroupAddon, InputGroupText, Input, Button, CardHeader, CardText,
  Form, Row, Col
} from 'reactstrap';
import Modal from 'react-awesome-modal';

class ForgetPassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      notification: "",
      visible: false
    }
    
    this.forgetPass = this.forgetPass.bind(this);
    this.emailChange=this.emailChange.bind(this);
  }

  emailChange(e){
    let valid=true;
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    let tempemail = e.target.value;
    if(e.target.value===""){
      document.getElementById("frgtvalidemailerror").style.display="none";
      document.getElementById("frgtemailerror").style.display="block";
    }else if(!pattern.test(tempemail)){
      document.getElementById("frgtvalidemailerror").style.display="block";
      document.getElementById("frgtemailerror").style.display="none";
    }else{
      document.getElementById("frgtvalidemailerror").style.display="none";
      document.getElementById("frgtemailerror").style.display="none";
    }
    this.setState({email:e.target.value});
  }
handleValidation(){
  let valid=true;
  var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
  let tempemail = this.state.email;
  if(this.state.email===""){
    valid=false;
    document.getElementById("frgtemailerror").style.display="block";
    return valid;
}else if(!pattern.test(tempemail)){
    valid=false;
    return valid;
}
else{
    document.getElementById("frgtvalidemailerror").style.display="none";
}
  return valid;
}
  
  forgetPass(e) {
    e.preventDefault();
    let Data={}
    if(this.handleValidation()){
      Data.email = this.state.email;
      this.props.dispatch(forgetPasswordMail(Data));
    }
   
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.success===true){
      if(nextProps.resetMessageResponse !== ""){
        this.setState({
          visible:true,
          notification:nextProps.resetMessageResponse,
        });
      }
    }
    else if(nextProps.resetMessageResponse !== ""){
      this.setState({
        visible:true,
        notification:nextProps.resetMessageResponse,
      });
    }
  }


  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("myBtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("myBtn").style.display = "block";
  }
  closeModal(){
    this.setState({
      visible:false,
      notification: ""
    });
    this.props.dispatch(removeForgetPassProps());
  }

  render() {
    
    let msgData = "";
    let msg = "";
    if (this.props.success === true) {
      if (this.props.resetMessageResponse !== "") {
        
         // alert(this.props.resetMessageResponse);
        // msg = this.props.resetMessageResponse;
        // msgData = <div className="showTrue" style={{textAlign:'center'}}>{msg}
        // </div>
      }
    } else {
      // if (this.props.resetMessageResponse !== "") {
      //   msg = this.props.resetMessageResponse;
      //   msgData = <div className="showFalse" style={{textAlign:'center'}}>{msg}
      //   </div>
      // }
    }

   let  showNotification=""
    if(this.state.notification){
      showNotification =   <Modal
      visible={this.state.visible}
      width="400"
      height="300"
      effect="fadeInDown"
      // onClickAway={() => this.closeModal()}
    >
      <div className="text-center" style={{ paddingTop: '20%' }}>
        <h1>Alert!</h1>
        <p>{this.state.notification}</p>
        <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
      </div>
    </Modal>
    }


    return (

      <div className="wrapper">
         {showNotification}
        <a href="javascript:void(0)" onClick={() => this.openNav()}> <button id="myBtn">&#9776;</button></a>
        <div className="column">
          <div id="icons">
            <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
            <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
            <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
            <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
        </div>
          <div className="logo"><img src={panelogo} height="50px" height="50px" /></div>
          <div className="subcolumn">
          <div id="mySidenav" class="sidenav">
              <a href="#" id="ioslink">
                  <div id="iosleft"><span>Get it on IOS</span></div>
                  <div id="iosright"><img src={appleicon} height="35px" width="35px" /></div>
                </a>
              <a href="https://play.google.com/store/apps/details?id=in.leonids.panel&hl=en" target="_blank" id="androidlink">
                  <div id="andrleft"><span>Get it on Android</span></div>
                  <div id="andright"><img src={androidicon} height="35px" width="35px" /></div>
                </a>
              </div>
            <div className="image">
              <img src={panelimg} width="400px" />
              <h3 className="heading">Welcome to Panel Reward</h3>
              <p className="title" style={{ fontWeight: '700' }}>Data Collection service</p>
              <p className="paragraph">GNN is a leading service provider in the field of conducting professional market research services be it quantitative & qualitative. We hold expertise in providing both primary and secondary research based services. It helps us to provide improvised marketing research study designs & analytical expertise that help us in successfully meeting client’s customized research needs.</p>
              <p className="title" style={{ fontWeight: '700' }}>Data Analysis service</p>
              <p className="paragraph">We provide advanced analytics services to help companies drill deeper into the data and understand the subterranean data linkages which drive customer behavior. Our Analytics services help companies to take data driven decisions and hone their marketing programs.</p>
              <Link to='/register'> <button className="regbtn">Register Now </button></Link>
              
            </div>
          </div>


        </div>
        <div class="column alt">
          <div id="menubtn">
          </div>
          <div className="form">
        
          <div id="clickbuttonhead">Forgot Password Panel Reward</div>
            <form onSubmit ={this.forgetPass} >
            <p style={{ fontSize: '1.125rem', width: '100%' }}>Enter your registered email id</p>
            <div style={{ position: 'relative' }}>
            <input type="text" placeholder="Enter E-Mail" class="inputtypefrgt"  onChange={this.emailChange}/>
            <div id="frgtemailerror">
                <div id="frgtquote">
                  <blockquote>
                    Required
                  </blockquote>
                </div>
                </div>
                <div id="frgtvalidemailerror">
                  <div id="frgtvalidquote">
                    <blockquote>
                     Incorrect Mail
                    </blockquote>
                  </div>
                </div>
            </div>
           {msgData}
            <button className="clickbuttonfrgt" type="submit">Forgot</button>
            </form>
           <Link to="/Login"> <button className="clickbuttonfrgt">Login</button></Link>
          </div>
          <div id="myNav" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn"><img src={cancel} height="18px" width="18px" className="link" /></button></a>
            <div class="overlay-content">
              <Link to='/aboutUs'>About Us</Link>
              <Link to='/register'>New Member Registration</Link>
              <Link to='/Login'>Login</Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    resetMessageResponse: state.forgetPassword.message,
    success: state.forgetPassword.success,
    requestStarted: state.common.requestStarted
  }
}

export default connect(mapStateToProps)(ForgetPassword);
