import React, { Component } from 'react';
import ProfileDashboardview from '../../components/ProfileDashboard';
import Education from '../../components/ProfileDashboard';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { fetchCategory } from '../../actions/profileDashboard';
import { connect } from 'react-redux';

class ProfileDashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mydata: "",
    }
    this.catView = this.catView.bind(this);
  }



  componentWillMount() {
    let userId = sessionStorage.getItem("user_id");
    this.props.dispatch(fetchCategory(userId));
    ///////     
    if (!this.props.authenticated) {
      let access_token = sessionStorage.getItem("token");
      let userId = sessionStorage.getItem("user_id");
      if (userId) {
        this.props.dispatch(fetchCategory(userId));
      } else {
        this.setState({
          mydata: "data state changed"
        });
        alert(this.state.mydata);
        // this.props.history.push("login");
      }
    }

  }

  catView(data) {
    this.props.history.push("/education" + "/cat" + data.cat_id + "/catName" + data.catName);
  }

  render() {
    // if (!this.props.authenticated) {
    //   let access_token = sessionStorage.getItem("token");
    //   let userId = sessionStorage.getItem("user_id");
    //   if (userId) {
    //     this.props.dispatch(fetchCategory(userId));
    //   } else {
    //     //alert("Please login.");
    //     this.props.history.push("login");
    //   }
    // }

    <Education
      profileCategory={this.props.profileCategory}

    />



    let ProfileDashboardviewHtml = "";
    ProfileDashboardviewHtml = <ProfileDashboardview
      profileCategory={this.props.profileCategory}
      requestStarted={this.props.requestStarted}
      catView={this.catView}
    />
    return (<div style={{marginTop: "40px"}}>

      {ProfileDashboardviewHtml}
    </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileCategory: state.profileDashboard.category,
    requestStarted: state.common.requestStarted,
    authenticated: state.login.authenticated
  }
}

export default connect(mapStateToProps)(ProfileDashboard);
