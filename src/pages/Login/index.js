import React, { Component } from 'react';
import './style.css';
import { logIn, setMessageFail } from '../../actions/logIn';
import { setRegistrationMessage } from '../../actions/registration';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import FacebookLoginButton from 'react-facebook-login';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import panelimg from './info_graphic_1.svg';
import panelogo from './panel reward.png';
import facebook from './facebook-logo.svg';
import google from './google-logo.svg';
import twitter from './twitter.svg';
import linked from './linkedin.svg';
import cancel from '../../icons/cancel.png';
import androidstore from '../../images/androidplaystore.png';
import appleicon from '../../icons/apple.png';
import androidicon from '../../icons/android-logo.png';
import $ from 'jquery';
import {
  Button, Card, CardBody, CardGroup, CardSubtitle, CardImg, CardTitle, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row,
  CardHeader, CardText
} from 'reactstrap';
import Modal from 'react-awesome-modal';


import email from './email.svg';
import lock from './lock.svg';
class LogIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //isLoggedIn: false,
      userID: '',
      name: '',
      picture: '',
      email: "",
      password: "",
      errorMessage: "",
      visible: false,
      notification: ""
    }
    this.emailChange = this.emailChange.bind(this);
    this.passChange=this.passChange.bind(this);
    this.login = this.login.bind(this);
    this.setMessage = this.setMessage.bind(this);

  }
  emailChange(e){
    let valid=true;
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    let tempemail = e.target.value;
    if(e.target.value===""){
      document.getElementById("mailerror").style.display="none";
      document.getElementById("error").style.display="block";
    }else if(!pattern.test(tempemail)){
      document.getElementById("mailerror").style.display="block";
      document.getElementById("error").style.display="none";
    }else{
      document.getElementById("mailerror").style.display="none";
      document.getElementById("error").style.display="none";
    }
    this.setState({email:e.target.value});
  }
  passChange(e){
    if(e.target.value===""){
      document.getElementById("error1").style.display="block";
    }else{
      document.getElementById("error1").style.display="none";
    }
    this.setState({password:e.target.value});
  }
  componentWillMount() {
     
    if (!this.props.authenticated) {
      let access_token = sessionStorage.getItem("token");
      let userId = sessionStorage.getItem("user_id");
      if (userId) {
        this.props.history.push("/studyCount");
      }
    }
  }

  setMessage() {
    this.props.dispatch(setRegistrationMessage());
  }


  // onChange(e) {
  //   if(e.target.name==="email" & e.target.value===""){
  //       document.getElementById("error").style.display="block";
  //       //alert("enter email");
  //   }
  //   else{
  //     document.getElementById("error").style.display="none";
      
  //   }
  //   if(e.target.name==="password" & e.target.value===""){
  //     document.getElementById("error1").style.display="block";
  //     // alert("enter pass");
  //   }else{
  //     document.getElementById("error1").style.display="none";
      
  //   }
  //   this.setState({ [e.target.name]: e.target.value });
  // }

  handleValidation() {
    let valid=true;
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    let tempemail = this.state.email;
    if(this.state.email===""){
      valid=false;
      document.getElementById("error").style.display="block";
      return valid;
  }else if(!pattern.test(tempemail)){
      valid=false;
      return valid;
  }
  else{
      document.getElementById("mailerror").style.display="none";
  }
    // if(this.state.email===""){
    //   valid=false;
    //   document.getElementById("error").style.display="block";
    //   //alert("Enter email");
    //   return valid;

    // }
    if(this.state.password===""){
      valid=false;
      document.getElementById("error1").style.display="block";
      //alert("Enter pass");
      return valid;
    }
   
    return valid;


    // let valid = true;
    // var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    // let tempemail = this.state.email;
    //  if (this.state.email === "") {
    //   valid = false;
    //   //document.getElementById("error").style.display="block";
    //   alert("Please enter Email Id");
    
    //   return valid;
    // }
    // else if (!pattern.test(tempemail)) {
    //   valid = false;
    //   this.setState({
    //     visible: true,
    //     notification: "Please enter correct Email Id"
    //   });
    //   return valid;
    // }
    // if(this.state.password === ""){
    //   valid=false;
    //   alert("enter password");
    //   return valid;
    // }
    // return valid;
  }

 

  login(e) {
    e.preventDefault();
    let logInData = {}
    if (this.handleValidation()) {
      logInData.email = this.state.email;
      logInData.password = this.state.password;
      logInData.registerType = "email";
      this.props.dispatch(logIn(logInData));
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.authenticated) {
      this.props.history.push('/studyCount');
      } else if(nextProps.userExist ===  true && nextProps.existUserID){
       // alert("hii...."+ nextProps.existUserID);
        this.props.history.push('/registerToken/'+nextProps.existUserID);
      } else if (nextProps.logInFailMessage != null && nextProps.logInFailMessage != "") {
        this.setState({ errorMessage: nextProps.logInFailMessage });
        this.props.dispatch(setMessageFail());
      }
      else {
        //this.setState({ errorMessage: "" });
      }
  }

  responseFacebook = (response) => {
    console.log(response.name + " " + response.email + " " + response.picture.data.url + "  ,id: " + response.id);
    //this.setState({isLoggedIn : false});


    let logInData = {};
    logInData.registerType = "facebook";
    logInData.facebookID = response.id;
    logInData.firstName = response.name;
    logInData.lastName = "";
    logInData.email = response.email;
    logInData.imageURL = response.picture.data.url;
    logInData.Token = response.Token;
    this.props.dispatch(logIn(logInData));
  }


  componentClicked = () => console.log("clicked");
 
  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("myBtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("myBtn").style.display = "block";
  }
 

  render() {
    
    if (this.state.errorMessage !== "") {

      // this.setState({
      //   visible: true,
      //   notification: this.state.errorMessage
      // });
      // alert(this.state.errorMessage);
    }
    // ☰  
    return (
     
        <div className="wrapper">
         
          <a href="javascript:void(0)" onClick={() => this.openNav()}><button id="myBtn">&#9776;</button></a>
          <div className="column">
             <div id="icons">
              <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
              <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
              <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
              <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
          </div>
            <div className="logo"><img src={panelogo} height="50px" height="50px" /></div>
            <div className="subcolumn">
            <div id="mySidenav" className="sidenav">
                <a href="#" id="ioslink">
                <div id="iosleft"><span>Get it on IOS</span></div>
                <div id="iosright"><img src={appleicon} height="35px" width="35px" /></div>
                </a>
                <a href="https://play.google.com/store/apps/details?id=in.leonids.panel&hl=en" target="_blank" id="androidlink">
                <div id="andrleft"><span>Get it on Android</span></div>
                <div id="andright"><img src={androidicon} height="35px" width="35px" /></div>
                </a>
              </div>
              <div className="image">
                <img src={panelimg} width="400px" />
                <h3 className="heading">Welcome to Panel Reward</h3>
                <p className="title" style={{ fontWeight: '700' }}>Data Collection service</p>
                <p className="paragraph">GNN is a leading service provider in the field of conducting professional market research services be it quantitative & qualitative. We hold expertise in providing both primary and secondary research based services. It helps us to provide improvised marketing research study designs & analytical expertise that help us in successfully meeting client’s customized research needs.</p>
                <p className="title" style={{ fontWeight: '700' }}>Data Analysis service</p>
                <p className="paragraph">We provide advanced analytics services to help companies drill deeper into the data and understand the subterranean data linkages which drive customer behavior. Our Analytics services help companies to take data driven decisions and hone their marketing programs.</p>
              <Link to="/register"><button className="regbtn" >Register Now</button></Link>
                
              </div>
            </div>
          </div>
          <div className="column alt">
         
            <div id="menubtn"> </div>
            <div className="form">
            <form onSubmit ={this.login} >
              <div id="clickbuttonhead">Login into Panel Reward</div>
              <p style={{ fontSize: '1.125rem', width: '100%' }}>Login into your account</p>
              <div style={{ position: 'relative' }}>
                <input type="text" placeholder="Enter E-Mail" className="inputtype" name="email" onChange={this.emailChange} />
                <div id="error">
                  <div className="quote">
                    <blockquote>
                        Required
                    </blockquote>
                  </div>
                </div>
                <div id="mailerror">
                  <div id="mailquote">
                  <blockquote>
                      Incorrect Mail
                  </blockquote>
                  </div>
                </div>
              </div>
              <div style={{ position: 'relative' }}>
              <input type="password" placeholder="Enter Password" className="inputtype" name="password" onChange={this.passChange} />
              <div id="error1">
                <div className="quote">
                    <blockquote>
                    Required
                    </blockquote>
                </div>
                </div>
              </div>
              <div id="errormsg">{this.state.errorMessage}</div>
              <button className="clickbutton" type = "submit" >Login</button>
              <Link to='/forgetpassword'> <button className="frgtbtn">Forgot Password</button></Link>
            
            </form>
            </div>
           

            <div id="myNav" class="overlay">
              <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn"><img src={cancel} height="18px" width="18px" className="link" /></button></a>
              <div class="overlay-content">
              <Link to='/aboutUs'>About Us</Link>
              <Link to='/register'>New Member Registration</Link>
              <Link to='/Login'>Login</Link>
              </div>
            </div>
          </div>
        </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    status: state.login.success,
    authenticated: state.login.authenticated,
    logInFailMessage: state.login.message,
    userExist : state.login.userExist,
    existUserID: state.login.existUserID
  }
}

export default connect(mapStateToProps)(LogIn);
