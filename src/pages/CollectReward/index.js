import React, { Component } from 'react';
import CollectTotalReward from '../../components/CollectReward';
import ThankuReward from '../../components/CollectReward/thankuReward'
import {transaction, studyCountData} from '../../actions/reward';
import {connect} from 'react-redux';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
//import {fetchStudy, saveStudyParticipate} from '../../actions/study';

class CollectReward extends Component {
  constructor(props){
    super(props)
    this.state = {
    }
    this.transaction = this.transaction.bind(this);
  }

  componentWillMount(){
    let userId=sessionStorage.getItem("user_id");
    this.props.dispatch(studyCountData(userId));
  }

  transaction(data){
    this.props.dispatch(transaction(data));
  }

  render() {
    if(!this.props.authenticated){
    let access_token=sessionStorage.getItem("token");
    let userId=sessionStorage.getItem("user_id");
        if(userId){
       
        }else {
        //alert("Please login.");
        this.props.history.push("/Login");
        }
    }
    let CollectTotalRewardHtml =""
    if(this.props.requestStarted){
      CollectTotalRewardHtml =
      <div className="loading">{
        this.props.requestStarted?
        <div className="preloader"><LoadingDots interval={100} dots={5}/></div>
        :
        false
      }
      </div>

      //  <div className = "countForm">
      // <div className = "collectRewardBack">
      //  <div className = "headercount"><h1>POINTS REDEEMTION</h1></div><br/><br/><br/>
        // <div className="loading">{
        //         this.props.requestStarted?
        //         <div className="preloader"><LoadingDots interval={100} dots={5}/></div>
        //         :
        //         false
        //       }</div></div><br/><br/><br/></div>
      
    }else if(this.props.transactionSuccess){
      CollectTotalRewardHtml = <ThankuReward
                                />
    }
    else if(this.props.StudyCountData){
     CollectTotalRewardHtml =   <CollectTotalReward
                                  StudyCountData = {this.props.StudyCountData}
                                  transaction = {this.transaction}
                                  transactionSuccess = {this.props.transactionSuccess}
                                  requestStarted ={this.props.requestStarted}
                                />
   }
  
  

   return (<div>
          {CollectTotalRewardHtml}
           </div>

   );
  }
}

const  mapStateToProps =(state) =>{
  return {
    authenticated : state.login.authenticated,
    StudyCountData : state.reward.StudyCountData,
    transactionSuccess : state.reward.TranactionSuccess,
    requestStarted:state.common.requestStarted
  }
}

export default connect(mapStateToProps)(CollectReward);
