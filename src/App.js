
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Menu from './containers/Menu';
import Logout from './containers/Logout';
import Registers from './pages/Register';
import Progressbar from './pages/Spinner/Progressbar';
import Study from './pages/Study';
import LogIn from './pages/Login';
import ResetPassword from './pages/ResetPassword';
import ForgetPassword from './pages/ForgetPassword';
import RegisterToken from './pages/RegisterToken';
import Profile from './pages/Profile';
import ProfileDashboard from './pages/ProfileDashboard';
import Education from './components/ProfileDashboard/education';
import StudyCount from './pages/Study/studyCount';
import StudyParticipate from './pages/StudyParticipate';
import CompaneyParticipated from './pages/StudyParticipate/companeyParticipated';
import ThankuPage from './pages/ThankuPage';
import CollectReward from './pages/CollectReward';
import TransactionHistory from './pages/TransactionHistory';
import LeftColumn from './containers/LeftMenu/LeftColumn';
import Demoquestion from './components/ProfileDashboard/demoquestion';
import ThnxReward from './components/CollectReward/ThnxReward';
import './App.css';
import scrollicon from './icons/scroll.png';
import Home from './pages/Home';
import { connect } from 'react-redux';
import $ from 'jquery';
import ImageUpload from './pages/ProfileImageUpload';
import { Link } from 'react-router-dom';
import StudyParticipateComplete from './pages/StudyParticipate/studyParticipateComplete';
import StudyParticipateFail from './pages/StudyParticipate/studyParticipateFail';
import StudyParticipateQuotaFail from './pages/StudyParticipate/studyParticipateQuotaFail';

import Footer from './pages/Footer';
import AboutUs from './pages/Footer/aboutUs';
import TermAndCondition from './pages/Footer/termCondition';
import PrivacyPolicy from './pages/Footer/privacyPolicy';
import AboutEpoints from './pages/Footer/aboutEpoints';

import { Loader } from 'react-overlay-loader';
import 'react-overlay-loader/styles.css';
class App extends Component {
          constructor(props){
            super(props);
            this.state={
                btnshow:'false',
                refresh:''
            };
            this.backToTop=this.backToTop.bind(this);
           
          }
    
    componentDidMount() {
    
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("scrollbtn").style.display = "block";
      } else {
        document.getElementById("scrollbtn").style.display = "none";
      }

      $(window).scroll(function() {
       
        if ($(this).scrollTop()>20)
         {
            $('#scrollbtn').fadeIn('slow');
         }
        else
         {
          $('#scrollbtn').fadeOut('slow');
         }
     });
     
    
    // if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    //   document.getElementById("scrollbtn").style.display = "block";
    // } else {
    //   document.getElementById("scrollbtn").style.display = "none";
    // }
   
    // $(window).scroll(function (e) {
      
    //   if ($(this).scrollTop() > 50) {
    //     $('#scrollbtn').fadeIn('slow');
    //   } else {
    //     $('#scrollbtn').fadeOut('slow');
    //   }
    // });
    // $('#scrollbtn').click(function () {
    //   $("html, body").animate({ scrollTop: 0 }, 500);
    //   return false;
    // });
    //     $(document).scroll(function() {
    //       if($(window).scrollTop() > 60){

    //        $("#scrollbtn").show();

    //       }else if($(window).scrollTop() < 60){
    //        $("#scrollbtn").hide();
    //        }
    //        $("#scrollbtn").on("click", function() {
    //          $(window).scrollTop(0);

    //     });
    // });
  }
  backToTop(){
        this.setState({btnshow:true});
  }

  render() {
   
      if(this.state.btnshow===true){
   
       $("html, body").animate({ scrollTop: 0 }, 500);
        this.setState({btnshow:false});
      }    
    let viewHtml = "";
    if (this.props.authenticated === undefined || this.props.authenticated === false) {
      let userid = sessionStorage.getItem('user_id');
      let access_token = sessionStorage.getItem('token');
      if (userid && access_token) {
        viewHtml = <Router>
          <div>
            <div>
              <Menu />
            </div>
            <div>
              <div id="leftcol" ><LeftColumn /></div>
              <div id="rightcol">
                <Switch>
                <Route component={LogIn} exact path="/Login" />
                <Route component={StudyCount} exact path="/"/>
                  <Route component={CollectReward} exact path="/collectReward" />
                  <Route component={TransactionHistory} exact path="/transactionHistory" />
                  <Route component={Progressbar} exact path="/spinner" />
                  <Route component={Study} exact path="/Study" />
                  <Route component={Profile} exact path="/Profile" />
                  <Route component={Logout} path="/logout" />
                  <Route component={StudyParticipate} path="/studyParticipate/:key" />
                  <Route component={CompaneyParticipated} path="/companyParticipated/:key?" />
                  <Route component={StudyCount} path="/studyCount" />
                  <Route component={ProfileDashboard} path="/profileDashboard" />
                  <Route component={Demoquestion} path="/demoquestion" />
                  <Route component={ThnxReward} path="/thnxreward" />
                  <Route component={Education} path="/education/:key" />
                  <Route component={ImageUpload} path="/imageupload" />
                  <Route component={StudyParticipateComplete} exact path="/studyParticipateCompleted/:key" />
                  <Route component={StudyParticipateFail} exact path="/studyParticipateFail/:key" />
                  <Route component={StudyParticipateQuotaFail} exact path="/studyParticipateQuotaFail/:key" />

                  <Route component={AboutUs} exact path="/aboutUs" />
                  <Route component={TermAndCondition} exact path="/termAndCondition" />
                  <Route component = {PrivacyPolicy} exact path="/privacyPolicy" />
                  <Route component = {AboutEpoints} exact path = "/aboutEpoints" />

                  {/* <Route component={StudyCount} path="/" /> */}
                </Switch>
                <button id="scrollbtn" onClick={this.backToTop} title="Go to top">
                  <img src={scrollicon} height="18px" width="18px" />
                </button>
              
                {/* <div id="footer">
            <div id="footerlink"><Link to="/aboutUs">About Us</Link>&nbsp;|&nbsp;<Link to="/privacyPolicy">Privacy Policy</Link>
            &nbsp;|&nbsp;<Link to="/aboutEpoints">About E-points</Link>&nbsp;|&nbsp;<Link to="/termAndCondition">Terms & Conditions</Link></div>
            <div id="copyright">Copyright © 2018 Panel Reward All rights reserved.</div>
            </div> */}
              </div>
              <div id="footerlogin">
            <div id="footerlink"><Link to="/aboutUs">About Us</Link>&nbsp;|&nbsp;<Link to="/privacyPolicy">Privacy Policy</Link>
            &nbsp;|&nbsp;<Link to="/aboutEpoints">About E-points</Link>&nbsp;|&nbsp;<Link to="/termAndCondition">Terms & Conditions</Link></div>
            <div id="copyright">Copyright © 2018 Panel Reward All rights reserved.</div>
            </div>
             
            </div>


          </div>
        </Router>
      } else {
        viewHtml = <Router>

          <div>
            <Switch>   
              <Route component={Registers} exact path="/register" />
              <Route component={LogIn} exact path="/Login" />
             

              <Route component={Progressbar} exact path="/spinner" />
              <Route component={ResetPassword} path="/resetPassword" />
              <Route component={ForgetPassword} path="/forgetPassword" />
              <Route component={RegisterToken} path="/registerToken/:key" />
              <Route component={Home} exact path="/home" />
              <Route component={ThankuPage} exact path="/thankuPage" />
              <Route component={AboutUs} exact path="/aboutUs" />
              <Route component={TermAndCondition} exact path="/termAndCondition" />
              <Route component = {PrivacyPolicy} exact path="/privacyPolicy" />
              <Route component = {AboutEpoints} exact path = "/aboutEpoints" />
              <Route component={LogIn} exact path="/" />
            </Switch>
            <button id="scrollbtn"  onClick={this.backToTop} title="Go to top">
              <img src={scrollicon} height="18px" width="18px" />
            </button>
            <div id="footerlogout">
              <div id="footerlink"><Link to="/aboutUs">About Us</Link>&nbsp;|&nbsp;<Link to="/privacyPolicy">Privacy Policy</Link>
              &nbsp;|&nbsp;<Link to="/aboutEpoints">About E-points</Link>&nbsp;|&nbsp;<Link to="/termAndCondition">Terms & Conditions</Link></div>
              <div id="copyright">Copyright © 2018 Panel Reward All rights reserved.</div>
            </div>
            {/* <div id="footer">Copyright © 2018 Panel Reward All rights reserved.</div> */}
          </div>
          </Router>
      }
    } else {
      viewHtml = <Router>
        <div>
          <div>
            <Menu />
          </div>
          <div>
            <div id="leftcol" ><LeftColumn /></div>
            <div id="rightcol">
              <Switch>
              <Route component={LogIn} exact path="/Login" />
              <Route component={StudyCount} exact path="/"/>

                <Route component={Progressbar} exact path="/spinner" />
                <Route component={Study} exact path="/Study" />
                <Route component={Profile} exact path="/Profile" />
                <Route component={Logout} path="/logout" />
                <Route component={StudyParticipate} path="/studyParticipate/:key" />
                <Route component={CompaneyParticipated} path="/companyParticipated/:key?" />
                <Route component={StudyCount} path="/studyCount" />
                <Route component={ProfileDashboard} path="/profileDashboard" />
                <Route component={Education} path="/education/:key" />
                <Route component={CollectReward} exact path="/collectReward" />
                <Route component={TransactionHistory} exact path="/transactionHistory" />
                <Route component={Demoquestion} path="/demoquestion" />
                <Route component={ThnxReward} path="/thnxreward" />
                <Route component={ImageUpload} path="/imageupload" />
                <Route component={StudyParticipateComplete} exact path="/studyParticipateCompleted/:key" />
                <Route component={StudyParticipateFail} exact path="/studyParticipateFail/:key" />
                <Route component={StudyParticipateQuotaFail} exact path="/studyParticipateQuotaFail/:key" />

                <Route component={AboutUs} exact path="/aboutUs" />
                <Route component={TermAndCondition} exact path="/termAndCondition" />
                <Route component = {PrivacyPolicy} exact path="/privacyPolicy" />
                <Route component = {AboutEpoints} exact path = "/aboutEpoints" />

                {/* <Route component={StudyCount} path="/" /> */}
              </Switch>
              <button id="scrollbtn"  onClick={this.backToTop} title="Go to top">
                <img src={scrollicon} height="18px" width="18px" />
              </button>
              {/* <div id="footer">
                <div id="footerlink"><Link to="/aboutUs">About Us</Link>&nbsp;|&nbsp;<Link to="/privacyPolicy">Privacy Policy</Link>
                &nbsp;|&nbsp;<Link to="/aboutEpoints">About E-points</Link>&nbsp;|&nbsp;<Link to="/termAndCondition">Terms & Conditions</Link></div>
                <div id="copyright">Copyright © 2018 Panel Reward All rights reserved.</div> */}
            </div>
            <div id="footerlogin">
                <div id="footerlink"><Link to="/aboutUs">About Us</Link>&nbsp;|&nbsp;<Link to="/privacyPolicy">Privacy Policy</Link>
                &nbsp;|&nbsp;<Link to="/aboutEpoints">About E-points</Link>&nbsp;|&nbsp;<Link to="/termAndCondition">Terms & Conditions</Link></div>
                <div id="copyright">Copyright © 2018 Panel Reward All rights reserved.</div>
              {/* <div id="footer">Copyright © 2018 Panel Reward All rights reserved.</div> */}
            </div>
          </div>
        </div>
      </Router>
    }
    return (
      <div className="App">
        <header className="header">
          {/* <img src={require("./gnnOpinionImg.png")}  className = "appimage"/><br/> */}

        </header>
        {viewHtml}
        <Loader fullPage={true} loading={this.props.loading} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authenticated: state.login.authenticated,
    loading: state.spinner.loading,
  }
}

export default connect(mapStateToProps)(App);