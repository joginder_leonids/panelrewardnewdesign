
import {combineReducers} from 'redux';
import registration from './registration';
import common from './common';
import login from './login';
import study from './study';
import profile from './profile';
import resetPassword from './resetPassword';
import forgetPassword from './forgetPassword';
import profileDashboard  from './profileDashboard';
import reward from './reward';
import spinner from '../loadingSpinner/Reducer';

const rootReducer = combineReducers({
registration,
common,
login,
study,
profile,
resetPassword,
forgetPassword,
profileDashboard,
reward,
spinner
});

export default rootReducer;
