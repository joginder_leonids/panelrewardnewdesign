const initialState = {
  error: [],
  users:{},
  success: false,
  message: ""
  }
  const registration = (state = initialState , action) =>{
    switch (action.type) {

      case 'SET_REGISTRATION_MESSAGE':
          return{
            ...state,
            error: action.error,
            message: false,
            success: false
          }

      case 'SHOW_REGISTER_USER':
          return {
          ...state,
            error: action.error,
            success: action.response.success,
            message: action.response.message,
            registerUserData: action.response.resultData,
          }

      case 'SHOW_CREATE_USER_FAIL_EXCEPTION':
          return{
            ...state,
            error: action.error,
            userFailmessage: action.response.message,
            success: false
          }

          
          case 'SHOW_TOKEN_VARIFY':
          return{
            ...state,
            error: action.error,
            message: action.response.message,
            success: action.response.success
          }

          case 'SHOW_TOKEN_VARIFY_FAIL':
          return{
            ...state,
            error: action.error,
            message: action.responseFail.message,
            success: action.responseFail.success
          }

          case 'SHOW_TOKEN_MSG_FAIL':
          return{
            ...state,
            error: action.error,
            message: false,
            success: false
          }

          case 'SET_REGISTER_NULL' :
          return{
            ...state,
            error: false,
            message: false,
            success: false,
            userFailmessage: false
          }

          default:
          return state;
        }
  };
export default registration;
