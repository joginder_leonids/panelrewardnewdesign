const initialState = {
  success: false,
  message: ""
  }
  const forgetPassword = (state = initialState , action) =>{
    switch (action.type) {

      case 'SET_FORGET_PASS_MAIL':
      return{
        ...state,
        message: action.responseMail.message,
        success: action.responseMail.success
      }

      case 'SET_FORGET_PASS_FAIL_MAIL':
      return{
        ...state,
        message: action.responseMail.message,
        success: action.responseMail.success
      }

      case 'REMOVE_FORGETPASS_PROPS':
      return{
        ...state,
        message : false,
        success : false
      }

      default:
      return state;
    }
  };
export default forgetPassword;
