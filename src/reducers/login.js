import isEmpty from 'lodash/isEmpty';
const initialState = {
  error: [],
  users:{},
  success: false,
  message: ""
  }
  const login = (state = initialState , action) =>{
    switch (action.type) {

      case 'SET_AUTH_CLIENT':
          return {
          ...state,
            authClient: action.authClient,
            authenticated : true,
            error: action.error,
            success: action.success,
            userExist: false,
          }
          

      case 'SET_LOGIN_FAIL':
          return{
            ...state,
            authClient : null,
            authenticated :false,
            success: false,
            message: action.authFailResponse.error,
            userExist: false,
          }
          case 'SET_LOGIN_FAIL_USER_EXIST':
          return{
            ...state,
            authClient : null,
            authenticated :false,
            success: false,
            existUserID: action.authFailResponse.userID,
            userExist : action.authFailResponse.userExist,
            message: action.authFailResponse.error
          }

          case 'LOGOUT' :
          return {
            ...state,
            authClient: null,
            authenticated : false
          }

          case 'SET_MESSAGE_FAIL':
          return{
            ...state,
            authClient : null,
            authenticated :false,
            success: false,
            message: null
          }

          case 'SET_RESET_PASS_MESSAGE_FAIL':
          return{
            ...state,
            authClient : null,
            authenticated :false,
            success: false,
            message: null
          }
         

          default:
          return state;
        }
  };
export default login;
