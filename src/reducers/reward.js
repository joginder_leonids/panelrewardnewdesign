
const initialState = {

}

const reward = (state = initialState, action) => {
  switch (action.type) {

    case 'SET_TRANSACTION_REQUEST_RESPONSE':
      return {
        showGiftcouponDataView: false,
        TranactionSuccess: action.response.success
      }

    case 'SET_STUDY_REWARD_COUNT_DATA':
      return {
        ...state,
        StudyCountData: action.countResponse.StudyCountData,
        success: action.countResponse.success.reward,
        TranactionSuccess: false
      }

    case 'SET_USER_TRANSACTION_DATA':
      return {
        ...state,
        showGiftcouponDataView: false,
        UserTranactionSuccess: action.transactionData.success,
        UserTranactionData: action.transactionData.transactionData
      }

    case 'SET_REWARD_NULL':
      return {
        ...state,
        StudyCountData: false,
        success: false
      }

    case 'SET_GIFTCOUPON_ONLINE_DATA':
      return {
        ...state,
        showGiftcouponDataView: true,
        GiftcouponDataSuccess: action.couponData.success,
        GiftcouponData: action.couponData
      }

    default:
      return state;
  }
};
export default reward;
