
const initialState = {
  studies: {},
  stash: {},

  message: String,
  states: {},
  success: false

}

const study = (state = initialState, action) => {
  switch (action.type) {

      case 'SET_STUDY':
        return {
          ...state,
          //messages : false,
          totalStudies : action.studyData.totalStudy,
          studies: action.studyData.studyData,
          StudyCountData : false,
          getAgainStudiesData: false
        }

      case 'SET_STUDY_PARTICIPATE_STATUS':
        return {
          ...state,
          messages : action.response.message,
          success : action.response.success,
          StudyCountData : false,
          getAgainStudiesData: false
        }

      case 'FAIL_STUDY_PARTICIPATE_STATUS':
        return{
          ...state,
          messages : action.response.error,
          success : action.response.success,
          StudyCountData : false,
          getAgainStudiesData: false
        } 
        
        
        case 'SET_STUDY_COUNT_DATA':
        return {   
          ...state,
          StudyCountData : action.countResponse.StudyCountData,
          StudyCountRelativeData : action.countResponse.relativeData,
          success : action.countResponse.success,
          getAgainStudiesData: false
        }

        case 'SET_Total_STUDY_COUNT_DATA':
        return {
          ...state,
          StudyTotalCountData : action.studyCountData,
          getAgainStudiesData: false
        }

        case 'SET_GET_STUDIES_AGAIN':
         return{
          ...state,
          getAgainStudiesData: true,
         // studies: false,
         }

    default:
      return state;
  }
};
export default study;
