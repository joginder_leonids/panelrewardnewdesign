
const initialState = {
  studies: {},
  stash: {},

  message: String,
  states: {}
}

const profile = (state = initialState, action) => {
  switch (action.type) {

    case 'SET_PROFILE_LOGOUT':
      return {
        userProfile: false,
        setUser:false,
        //userProfileUpdate:false
      }

    case 'SET_PROFILE':
      return {
        ...state,
        userProfile: action.profileData.profile,
        setUser:action.profileData.profile,
        userProfileUpdate : action.profileData,
      }      

    case 'SET_PROFILE_UPLOAD_IMAGE_RESPONSE':
      return {
        ...state,
        profileImageUploadRes : action.updateResponse
      }
    
    case 'SET_PROFILE_RESPONSE_NULL':
      return{
        ...state,
        userProfileUpdate:false
      }

    default:
      return state;
  }
};
export default profile;
